﻿using System;
using System.Collections.Generic;
using System.Linq;
using DroneLegends.DataAccess.Common;
using DroneLegends.DataAccess.Common.Enum;
using DroneLegends.DataAccess.Common.Interfaces;
using DroneLegends.DataAccess.EFBase.Repository;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace DroneLegends.DataAccess.EFBase.UnitOfWork
{
    public class UnitOfWorkBase : IDisposable, IUnitOfWork
    {
        private bool disposed;

        public UnitOfWorkBase(DbContext context)
        {
            this.Context = context;
            this.RepositoriesInternal = new Dictionary<Type, RepositoryBase>();
        }

        protected DbContext Context { get; private set; }

        private Dictionary<Type, RepositoryBase> RepositoriesInternal { get; set; }

        public void SaveChanges(bool setTimestamps = true)
        {
            if (setTimestamps)
            {
                this.SetTimestamps();
            }

            this.Context.SaveChanges();
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        public IReadOnlyDictionary<EntityBase, AffectedEntityState> GetAffectedEntities()
        {
            Dictionary<EntityBase, AffectedEntityState> result = new Dictionary<EntityBase, AffectedEntityState>();

            foreach (EntityEntry entity in this.Context.ChangeTracker.Entries().Where(x => x.State == EntityState.Added || x.State == EntityState.Modified || x.State == EntityState.Deleted))
            {
                AffectedEntityState state;
                if (entity.State == EntityState.Added)
                {
                    state = AffectedEntityState.Added;
                }
                else if (entity.State == EntityState.Modified)
                {
                    state = AffectedEntityState.Updated;
                }
                else if (entity.State == EntityState.Deleted)
                {
                    state = AffectedEntityState.Deleted;
                }
                else
                {
                    throw new InvalidOperationException("Invalid entity state");
                }

                result.Add((EntityBase)entity.Entity, state);
            }

            return result;
        }

        protected TRepository GetRepository<TRepository>()
            where TRepository : RepositoryBase
        {
            Type type = typeof(TRepository);
            if (!this.RepositoriesInternal.TryGetValue(type, out RepositoryBase value))
            {
                this.RepositoriesInternal.Add(type, (TRepository)Activator.CreateInstance(type, new object[] { this.Context }));
            }

            return (TRepository)this.RepositoriesInternal[type];
        }

        protected virtual void SetTimestamps()
        {
            DateTimeOffset now = DateTimeOffset.Now;

            IReadOnlyDictionary<EntityBase, AffectedEntityState> affectedEntities = this.GetAffectedEntities();
            foreach (EntityBase affectedEntity in affectedEntities.Keys)
            {
                affectedEntity.ModificationDate = now;
                AffectedEntityState state = affectedEntities[affectedEntity];

                if (state == AffectedEntityState.Added)
                {
                    affectedEntity.CreationDate = now;
                }
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }

            if (disposing)
            {
                this.Context.Dispose();
            }

            this.disposed = true;
        }
    }
}
