﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using DroneLegends.DataAccess.Common;
using DroneLegends.DataAccess.Common.Interfaces.Generic;

namespace DroneLegends.DataAccess.EFBase.Repository
{
    /// <summary>Repository base</summary>
    /// <typeparam name="TContext">The type of the context.</typeparam>
    /// <typeparam name="TMyEntity">The type of the entity.</typeparam>
    public class RepositoryBaseT<TContext, TMyEntity> : RepositoryBase, IRepository<TMyEntity>
        where TContext : DbContext
        where TMyEntity : EntityBase
    {
        public RepositoryBaseT(TContext context)
            : base(context)
        {
        }

        /// <summary>
        /// Gets the entity by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Entity that matches identifier. In the case if nothing found, returns null</returns>
        public TMyEntity GetById(Guid id)
        {
            return this.DbSetForGettingSingle()
                .SingleOrDefault(e => e.Id == id);
        }

        /// <summary>
        /// Gets all entities.
        /// </summary>
        /// <returns>All entities</returns>
        public IEnumerable<TMyEntity> GetAll()
        {
            return this.DbSetForGettingMany()
                .ToList();
        }

        void IRepository<TMyEntity>.AddEntity(TMyEntity entity)
        {
            this.AddEntity(entity);
        }

        void IRepository<TMyEntity>.DeleteEntity(TMyEntity entity)
        {
            this.DeleteEntity(entity);
        }

        void IRepository<TMyEntity>.UpdateEntity(TMyEntity entity)
        {
            this.UpdateEntity(entity);
        }

        /// <summary>
        /// Retrieves basic DbSet (IQuerable) for getting single entity (by id)
        /// Should be overrieden for adding includes, addtional filters, etc...
        /// </summary>
        /// <returns>IQuerable to be used for fetching single entity</returns>
        protected virtual IQueryable<TMyEntity> DbSetForGettingSingle()
        {
            return this.BaseDbSet();
        }

        /// <summary>
        /// Retrieves basic DbSet (IQuerable) for getting multiple entities
        /// Should be overrieden for adding general includes, additional filters, etc...
        /// </summary>
        /// <returns>IQuerable to be used for fetching multiple entities</returns>
        protected virtual IQueryable<TMyEntity> DbSetForGettingMany()
        {
            return this.BaseDbSet();
        }

        /// <summary>
        /// Returns base DbSet and sets it to be AsNoTracking
        /// </summary>
        /// <returns>Base db set</returns>
        protected IQueryable<TMyEntity> BaseDbSet()
        {
            return this.ContextBase.Set<TMyEntity>().AsNoTracking();
        }
    }
}
