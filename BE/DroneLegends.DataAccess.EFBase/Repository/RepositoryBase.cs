﻿using System;
using Microsoft.EntityFrameworkCore;
using DroneLegends.DataAccess.Common;
using DroneLegends.DataAccess.Common.Interfaces;

namespace DroneLegends.DataAccess.EFBase.Repository
{
    public class RepositoryBase : IRepository
    {
        public RepositoryBase(DbContext context)
        {
            this.ContextBase = context;
        }

        protected DbContext ContextBase { get; private set; }

        public virtual void AddEntity(EntityBase entity)
        {
            this.ContextBase.Entry(entity).State = EntityState.Added;

            if (entity.Id == null || entity.Id == Guid.Empty)
            {
                entity.Id = Guid.NewGuid();
            }
        }

        public virtual void UpdateEntity(EntityBase entity)
        {
            this.ContextBase.Entry(entity).State = EntityState.Modified;
        }

        public virtual void DeleteEntity(EntityBase entity)
        {
            this.ContextBase.Entry(entity).State = EntityState.Deleted;
        }
    }
}
