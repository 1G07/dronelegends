﻿using DroneLegends.BL.RequiredInterfaces;
using DroneLegends.DataAccess.Common.Interfaces.Generic;
using DroneLegends.DataAccess.EF.Context;
using DroneLegends.DataAccess.EF.Repository;
using DroneLegends.DataAccess.EFBase.Repository;
using DroneLegends.DataAccess.EFBase.UnitOfWork;
using DroneLegends.DataModels;

namespace DroneLegends.DataAccess.EF.UnitOfWork
{
    public class UnitOfWork : UnitOfWorkBase, IUnitOfWork
    {
        public UnitOfWork(SourceContext context)
           : base(context)
        {
        }

        /// <summary>
        /// Gets account repository.
        /// </summary>
        public IAccountRepository AccountRepository => this.GetRepository<AccountRepository>();

        /// <summary>
        /// Gets institution repository.
        /// </summary>
        public IInstitutionRepository InstitutionRepository => this.GetRepository<InstitutionRepository>();

        /// <summary>
        /// Gets contact repository.
        /// </summary>
        public IRepository<Contact> ContactRepository => this.GetRepository<RepositoryBaseT<SourceContext, Contact>>();

        /// <summary>
        /// Gets address repository.
        /// </summary>
        public IRepository<Address> AddressRepository => this.GetRepository<RepositoryBaseT<SourceContext, Address>>();

        /// <summary>
        /// Gets license repository.
        /// </summary>
        public IRepository<License> LicenseRepository => this.GetRepository<LicenseRepository>();
        
        /// <summary>
        /// Gets teacher repository.
        /// </summary>
        public ITeacherRepository TeacherRepository => this.GetRepository<TeacherRepository>();
        
        /// <summary>
        /// Gets module repository.
        /// </summary>
        public IRepository<Module> ModuleRepository => this.GetRepository<RepositoryBaseT<SourceContext, Module>>();

        /// <summary>
        /// Gets Lesson repository.
        /// </summary>
        public ILessonRepository LessonRepository => this.GetRepository<LessonRepository>();

        /// <summary>
        /// Gets Subscription repository.
        /// </summary>
        public ISubscriptionRepository SubscriptionRepository => this.GetRepository<SubscriptionRepository>();

        /// <summary>
        /// Gets DownloadableResource repository.
        /// </summary>
        public IDownloadableResourceRepository DownloadableResourceRepository => this.GetRepository<DownloadableResourceRepository>();
        
        /// <summary>
        /// Gets EmbededResource repository.
        /// </summary>
        public IEmbededResourceRepository EmbededResourceRepository => this.GetRepository<EmbededResourceRepository>();

        /// <summary>
        /// Gets ticket repository.
        /// </summary>
        public IRepository<Ticket> TicketRepository => this.GetRepository<RepositoryBaseT<SourceContext, Ticket>>();

        /// <summary>
        /// Gets Super admin repository.
        /// </summary>
        public ISuperAdminRepository SuperAdminRepository => this.GetRepository<SuperAdminRepository>();
    }
}
