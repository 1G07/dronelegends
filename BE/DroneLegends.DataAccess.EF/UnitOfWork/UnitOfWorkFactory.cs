﻿using Microsoft.EntityFrameworkCore;
using DroneLegends.BL.RequiredInterfaces;
using DroneLegends.DataAccess.EF.Context;

namespace DroneLegends.DataAccess.EF.UnitOfWork
{
    public class UnitOfWorkFactory : IUnitOfWorkFactory
    {
        public UnitOfWorkFactory(DbContextOptions<SourceContext> contextOptions)
        {
            this.ContextOptions = contextOptions;
        }

        public DbContextOptions<SourceContext> ContextOptions { get; }

        public IUnitOfWork CreateUnitOfWork()
        {
            return new UnitOfWork(new SourceContext(this.ContextOptions));
        }
    }
}
