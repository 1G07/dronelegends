﻿using System;
using System.Collections.Generic;
using System.Linq;
using DroneLegends.BL.RequiredInterfaces;
using DroneLegends.DataAccess.EF.Context;
using DroneLegends.DataAccess.EFBase.Repository;
using DroneLegends.DataModels;
using DroneLegends.DataModels.Enums;
using Microsoft.EntityFrameworkCore;

namespace DroneLegends.DataAccess.EF.Repository
{
    public class InstitutionRepository : RepositoryBaseT<SourceContext, Institution>, IInstitutionRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InstitutionRepository"/> class.
        /// </summary>
        /// <param name="context">Source context.</param>
        public InstitutionRepository(SourceContext context)
          : base(context)
        {
        }

        public List<Institution> Get(InstitutionFilter filter)
        {
            return this.GetFilterQuery(filter).Skip((filter.Offset - 1) * filter.Limit)
                .Take(filter.Limit)
                .OrderBy(i => i.CreationDate)
                .ToList();
        }
        
        public int GetTotalCount(InstitutionFilter filter)
        {
            IQueryable<Institution> query = this.GetFilterQuery(filter);

            return query.Count();
        }

        public Institution GetByAccountId(Guid accountId)
        {
            return this.BaseDbSet().Include(i => i.ParentInstitution).FirstOrDefault(i => i.AccountId == accountId);
        }

        /// <summary>
        /// Gets institution by account id.
        /// </summary>
        /// <param name="accountId">id.</param>
        /// <param name="parentAccountId">Parent account id.</param>
        /// <returns>Institution</returns>
        public Institution GetByIdAndParentAccountId(Guid id, Guid? parentAccountId)
        {
            return this.BaseDbSet()
                .Include(i => i.ParentInstitution)
                .Include(i => i.Account)
                .Include(i => i.Address)
                .Include(i => i.Contact)
                .FirstOrDefault(i => i.Id == id && i.ParentInstitution.AccountId == parentAccountId);
        }

        public int GetNumberOfSavedInstitutions(Guid parentId, Role role)
        {
            return this.BaseDbSet()
               .Include(i => i.Account)
               .Where(i => i.ParentId == parentId && i.Account.Role == role)
               .Count();
        }

        protected override IQueryable<Institution> DbSetForGettingSingle()
        {
            return this.BaseDbSet()
                .Include(i => i.Account)
                 .ThenInclude(a => a.License)
                     .ThenInclude(l => l.Owner)
                        .ThenInclude(l => l.Account)
               .Include(i => i.Contact)
               .Include(i => i.Address);
        }

        private IQueryable<Institution> GetFilterQuery(InstitutionFilter filter)
        {
            IQueryable<Institution> query = this.DbSetForGettingMany()
               .Include(i => i.Account)
                .ThenInclude(a => a.License)
                  .ThenInclude(l => l.Owner)
               .Include(i => i.Contact)
               .Include(i => i.Address);

            if (!string.IsNullOrEmpty(filter.Input))
            {
                query = query.Where(i => (i.Name.Contains(filter.Input) || i.Account.Email.Contains(filter.Input)));
            }

            if (filter.Role != null)
            {
                query = query.Where(i => i.Account.Role == filter.Role);
            }

            if (filter.ParentId != null)
            {
                query = query.Where(i => i.ParentId == filter.ParentId);
            }

            return query;
        }
    }
}

