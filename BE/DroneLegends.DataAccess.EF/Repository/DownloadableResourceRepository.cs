﻿using DroneLegends.BL.RequiredInterfaces;
using DroneLegends.DataAccess.EF.Context;
using DroneLegends.DataAccess.EFBase.Repository;
using DroneLegends.DataModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DroneLegends.DataAccess.EF.Repository
{
    /// <summary>
    /// Subscription Repository.
    /// </summary>
    public class DownloadableResourceRepository : RepositoryBaseT<SourceContext, DownloadableResource>, IDownloadableResourceRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DownloadableResourceRepository"/> class.
        /// </summary>
        /// <param name="context">Source context.</param>
        public DownloadableResourceRepository(SourceContext context)
          : base(context)
        {
        }

        public List<DownloadableResource> GetByLessonId(Guid lessonId)
        {
            return this.BaseDbSet()
                .Where(d => d.LessonId == lessonId).OrderBy(d => d.Position).ToList();
        }

        protected override IQueryable<DownloadableResource> DbSetForGettingSingle()
        {
            return this.BaseDbSet()
                .Include(d => d.Lesson)
                    .ThenInclude(l => l.Module);
        }
    }
}
