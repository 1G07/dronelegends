﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using DroneLegends.BL.RequiredInterfaces;
using DroneLegends.DataAccess.EF.Context;
using DroneLegends.DataAccess.EFBase.Repository;
using DroneLegends.DataModels;

namespace DroneLegends.DataAccess.EF.Repository
{
    /// <summary>
    /// Subscription Repository.
    /// </summary>
    public class EmbededResourceRepository : RepositoryBaseT<SourceContext, EmbededResource>, IEmbededResourceRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EmbededResourceRepository"/> class.
        /// </summary>
        /// <param name="context">Source context.</param>
        public EmbededResourceRepository(SourceContext context)
          : base(context)
        {
        }

        public List<EmbededResource> GetByLessonId(Guid lessonId)
        {
            return this.BaseDbSet()
                .Where(d => d.LessonId == lessonId).OrderBy(d => d.Position).ToList();
        }

        protected override IQueryable<EmbededResource> DbSetForGettingSingle()
        {
            return this.BaseDbSet()
                .Include(d => d.Lesson);
        }
    }
}
