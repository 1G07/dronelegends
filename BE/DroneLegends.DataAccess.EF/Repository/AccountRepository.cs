﻿using DroneLegends.BL.RequiredInterfaces;
using DroneLegends.DataAccess.EF.Context;
using DroneLegends.DataAccess.EFBase.Repository;
using DroneLegends.DataModels;
using DroneLegends.DataModels.Enums;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DroneLegends.DataAccess.EF.Repository
{
    /// <summary>
    /// Account Repository.
    /// </summary>
    public class AccountRepository : RepositoryBaseT<SourceContext, Account>, IAccountRepository
    {
        private const double RefreshTokenDurationInHours = 4;

        /// <summary>
        /// Initializes a new instance of the <see cref="AccountRepository"/> class.
        /// </summary>
        /// <param name="context">Source context.</param>
        public AccountRepository(SourceContext context)
          : base(context)
        {
        }

        /// <summary>
        /// Gets account by access token.
        /// </summary>
        /// <param name="accessToken">Access token by which the account is searched.</param>
        /// <returns>Account or null.</returns>
        public Account GetAccount(string accessToken)
        {
            return this.DbSetForGettingSingle()
                .Where(a => a.AccessToken == accessToken && a.AccessTokenIssued != null && a.AccessTokenIssued.Value.AddHours(Account.AccessTokenDurationInHours) > DateTime.Now)
                .SingleOrDefault();
        }

        /// <summary>
        /// Gets account by access token.
        /// </summary>
        /// <param name="accessToken">Access token by which the account is searched.</param>
        /// <returns>Account or null.</returns>
        public Account GetAccount(string email, string password)
        {
            return this.DbSetForGettingSingle()
                .Where(a => a.Email == email && a.Password == password)
                .SingleOrDefault();
        }

        /// <summary>
        /// Get account by refresh token.
        /// </summary>
        /// <param name="refreshToken">Refresh token.</param>
        /// <returns>Account.</returns>
        public Account GetAccountByRefreshToken(string refreshToken)
        {
            return this.DbSetForGettingSingle()
                .Where(a => a.RefreshToken == refreshToken && a.RefreshTokenIssued != null && a.RefreshTokenIssued.Value.AddHours(RefreshTokenDurationInHours) > DateTime.Now)
                .SingleOrDefault();
        }

        /// <summary>
        /// Get account by unsubscribe token.
        /// </summary>
        /// <param name="unsubscribeToken">Unsubscribe token.</param>
        /// <returns>Account.</returns>
        public Account GetAccountByUnsubscribeToken(string unsubscribeToken)
        {
            return this.DbSetForGettingSingle()
                .Where(a => a.UnsubscribeToken == unsubscribeToken)
                .SingleOrDefault();
        }

        /// <summary>
        /// Gets account by email.
        /// </summary>
        /// <param name="email">Account email.</param>
        /// <returns>Account.</returns>
        public Account GetAccountByEmail(string email)
        {
            return this.DbSetForGettingSingle()
                .Where(a => a.Email == email)
                .SingleOrDefault();
        }
        
        /// <summary>
        /// Gets account by role.
        /// </summary>
        /// <param name="role">Account role.</param>
        /// <returns>List of accounts.</returns>
        public List<Account> GetAccountsByRole(Role role)
        {
            return this.DbSetForGettingSingle()
                .Where(a => a.Role == role)
                .ToList();
        }

        /// <summary>
        /// Get account by reset token.
        /// </summary>
        /// <param name="resetToken">Reset token.</param>
        /// <returns>Account.</returns>
        public Account GetAccountByResetToken(string resetToken)
        {
            return this.DbSetForGettingSingle()
                .Where(u => u.ResetToken == resetToken)
                .SingleOrDefault();
        }

        /// <summary>
        /// Gets used Licenses for account.
        /// </summary>
        /// <param name="licenseId">License id.</param>
        /// <param name="role">Role.</param>
        /// <returns>Number of used licenses.</returns>
        public int GetUsedLicenses(Guid licenseId, Role role)
        {
            return this.DbSetForGettingMany()
                .Where(a => a.LicenseId == licenseId && a.Role == role)
                .Count();
        }

        /// <summary>
        /// Gets accounts associated with the license.
        /// </summary>
        /// <param name="licenseId">License id.</param>
        /// <returns>List of accounts.</returns>
        public List<Account> GetByLicenseId(Guid licenseId)
        {
            return this.DbSetForGettingMany()
                .Where(a => a.LicenseId == licenseId)
                .ToList();
        }

        protected override IQueryable<Account> DbSetForGettingSingle()
        {
            return this.BaseDbSet().Include(a => a.License);
        }
        
        protected override IQueryable<Account> DbSetForGettingMany()
        {
            return this.BaseDbSet().Include(a => a.License);
        }
    }
}
