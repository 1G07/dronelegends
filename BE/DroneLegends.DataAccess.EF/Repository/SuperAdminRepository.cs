﻿using System;
using System.Collections.Generic;
using System.Linq;
using DroneLegends.BL.RequiredInterfaces;
using DroneLegends.DataAccess.EF.Context;
using DroneLegends.DataAccess.EFBase.Repository;
using DroneLegends.DataModels;
using Microsoft.EntityFrameworkCore; 

namespace DroneLegends.DataAccess.EF.Repository
{
    public class SuperAdminRepository : RepositoryBaseT<SourceContext, SuperAdmin>, ISuperAdminRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SuperAdminRepository"/> class.
        /// </summary>
        /// <param name="context">Source context.</param>
        public SuperAdminRepository(SourceContext context)
          : base(context)
        {
        }

        public List<SuperAdmin> Get(SuperAdminFilter filter)
        {
            return this.GetFilterQuery(filter).Skip((filter.Offset - 1) * filter.Limit)
                .Take(filter.Limit)
                .OrderBy(t => t.CreationDate)
                .ToList();
        }

        public int GetTotalCount(SuperAdminFilter filter)
        {
            return this.GetFilterQuery(filter).Count();
        }

        public SuperAdmin GetByAccountId(Guid accountId)
        {
            return this.BaseDbSet().Include(t => t.Account).FirstOrDefault(t => t.AccountId == accountId);
        }

        protected override IQueryable<SuperAdmin> DbSetForGettingSingle()
        {
            return this.BaseDbSet()
                .Include(i => i.Account)
                 .ThenInclude(a => a.License);
        }

        private IQueryable<SuperAdmin> GetFilterQuery(SuperAdminFilter filter)
        {
            IQueryable<SuperAdmin> query = this.DbSetForGettingMany()
               .Include(t => t.Account)
                .ThenInclude(t => t.License);

            if (!string.IsNullOrEmpty(filter.Input))
            {
                query = query.Where(t => (t.Account.FirstName.Contains(filter.Input) || t.Account.LastName.Contains(filter.Input) || t.Account.Email.Contains(filter.Input)));
            }

            return query;
        }
    }
}

