﻿using DroneLegends.BL.RequiredInterfaces;
using DroneLegends.DataAccess.EF.Context;
using DroneLegends.DataAccess.EFBase.Repository;
using DroneLegends.DataModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DroneLegends.DataAccess.EF.Repository
{
    /// <summary>
    /// Subscription Repository.
    /// </summary>
    public class SubscriptionRepository : RepositoryBaseT<SourceContext, Subscription>, ISubscriptionRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SubscriptionRepository"/> class.
        /// </summary>
        /// <param name="context">Source context.</param>
        public SubscriptionRepository(SourceContext context)
          : base(context)
        {
        }

        public List<Subscription> GetByLicenseId(Guid licenseId)
        {
            return this.BaseDbSet()
                .Include(s => s.Module)
                .Where(s => s.LicenseId == licenseId).ToList();
        }
    }
}
