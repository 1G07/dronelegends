﻿using System;
using System.Collections.Generic;
using System.Linq;
using DroneLegends.BL.RequiredInterfaces;
using DroneLegends.DataAccess.EF.Context;
using DroneLegends.DataAccess.EFBase.Repository;
using DroneLegends.DataModels;
using Microsoft.EntityFrameworkCore; 

namespace DroneLegends.DataAccess.EF.Repository
{
    public class TeacherRepository : RepositoryBaseT<SourceContext, Teacher>, ITeacherRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InstitutionRepository"/> class.
        /// </summary>
        /// <param name="context">Source context.</param>
        public TeacherRepository(SourceContext context)
          : base(context)
        {
        }

        public List<Teacher> Get(TeacherFilter filter)
        {
            return this.GetFilterQuery(filter).Skip((filter.Offset - 1) * filter.Limit)
                .Take(filter.Limit)
                .OrderBy(t => t.CreationDate)
                .ToList();
        }

        public int GetTotalCount(TeacherFilter filter)
        {
            return this.GetFilterQuery(filter).Count();
        }

        public Teacher GetByAccountId(Guid accountId)
        {
            return this.BaseDbSet().Include(t => t.Account).FirstOrDefault(t => t.AccountId == accountId);
        }

        /// <summary>
        /// Gets Teacher by account id.
        /// </summary>
        /// <param name="id">Id.</param>
        /// <param name="parentAccountId">Parent account id.</param>
        /// <returns>Teacher</returns>
        public Teacher GetByIdAndParentAccountId(Guid id, Guid? parentAccountId)
        {
            return this.BaseDbSet()
                .Include(i => i.Parent)
                .Include(i => i.Account)
                .FirstOrDefault(i => i.Id == id && i.Parent.AccountId == parentAccountId);
        }

        public int GetNumberOfSavedTeachers(Guid parentId)
        {
            return this.BaseDbSet()
               .Include(i => i.Account)
               .Where(i => i.ParentId == parentId)
               .Count();
        }

        protected override IQueryable<Teacher> DbSetForGettingSingle()
        {
            return this.BaseDbSet()
                .Include(i => i.Account)
                 .ThenInclude(a => a.License)
                     .ThenInclude(l => l.Owner);
        }

        private IQueryable<Teacher> GetFilterQuery(TeacherFilter filter)
        {
            IQueryable<Teacher> query = this.DbSetForGettingMany()
               .Include(t => t.Account)
                .ThenInclude(t => t.License)
                  .ThenInclude(t => t.Owner);

            if (!string.IsNullOrEmpty(filter.Input))
            {
                query = query.Where(t => (t.FirstName.Contains(filter.Input) || t.LastName.Contains(filter.Input) || t.Account.Email.Contains(filter.Input)));
            }

            if (filter.ParentId != null)
            {
                query = query.Where(t => t.ParentId == filter.ParentId);
            }

            return query;
        }
    }
}

