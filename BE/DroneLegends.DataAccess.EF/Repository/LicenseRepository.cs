﻿using System.Linq;
using DroneLegends.DataAccess.Common.Interfaces.Generic;
using DroneLegends.DataAccess.EF.Context;
using DroneLegends.DataAccess.EFBase.Repository;
using DroneLegends.DataModels;
using Microsoft.EntityFrameworkCore;

namespace DroneLegends.DataAccess.EF.Repository
{
    public class LicenseRepository : RepositoryBaseT<SourceContext, License>, IRepository<License>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LicenseRepository"/> class.
        /// </summary>
        /// <param name="context">Source context.</param>
        public LicenseRepository(SourceContext context)
          : base(context)
        {
        }

        protected override IQueryable<License> DbSetForGettingSingle()
        {
            return this.BaseDbSet().Include(l => l.Owner);
        }
    }
}

