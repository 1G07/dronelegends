﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using DroneLegends.BL.RequiredInterfaces;
using DroneLegends.DataAccess.EF.Context;
using DroneLegends.DataAccess.EFBase.Repository;
using DroneLegends.DataModels;


namespace DroneLegends.DataAccess.EF.Repository
{
    /// <summary>
    /// Lesson Repository.
    /// </summary>
    public class LessonRepository : RepositoryBaseT<SourceContext, Lesson>, ILessonRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LessonRepository"/> class.
        /// </summary>
        /// <param name="context">Source context.</param>
        public LessonRepository(SourceContext context)
          : base(context)
        {
        }

        public List<Lesson> GetForModule(Guid moduleId)
        {
            return this.BaseDbSet()
                .Include(l => l.Module)
                .Where(l => l.ModuleId == moduleId).OrderBy(l => l.Position).ToList();
        }

        public Lesson GetLessonDetails(Guid lessonId)
        {
            return this.BaseDbSet()
                .Include(l => l.Module)
                .FirstOrDefault(l => l.Id == lessonId);
        }
    }
}
