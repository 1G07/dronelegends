﻿using DroneLegends.DataAccess.EF.Configuration;
using DroneLegends.DataModels;
using Microsoft.EntityFrameworkCore;

namespace DroneLegends.DataAccess.EF.Context
{
    public class SourceContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SourceContext"/> class.
        /// </summary>
        /// <param name="options">DB context options.</param>
        public SourceContext(DbContextOptions<SourceContext> options)
            : base(options)
        {
        }

        /// <summary>
        /// Gets or sets Acounts db set.
        /// </summary>
        public DbSet<Account> Accounts { get; set; }

        /// <summary>
        /// Gets or sets Contacts db set.
        /// </summary>
        public DbSet<Contact> Contacts { get; set; }

        /// <summary>
        /// Gets or sets Licences db set.
        /// </summary>
        public DbSet<License> Licenses { get; set; }

        /// <summary>
        /// Gets or sets SuperAdmins db set.
        /// </summary>
        public DbSet<SuperAdmin> SuperAdmins { get; set; }
        
        /// <summary>
        /// Gets or sets Institutions db set.
        /// </summary>
        public DbSet<Institution> Institutions { get; set; }
        
        /// <summary>
        /// Gets or sets Teachers db set.
        /// </summary>
        public DbSet<Teacher> Teachers { get; set; }

        /// <summary>
        /// Gets or sets Modules db set.
        /// </summary>
        public DbSet<Module> Modules { get; set; }

        /// <summary>
        /// Gets or sets Lessons db set.
        /// </summary>
        public DbSet<Lesson> Lessons { get; set; }

        /// <summary>
        /// Gets or sets Subscription db set.
        /// </summary>
        public DbSet<Subscription> Subscriptions { get; set; }

        /// <summary>
        /// Gets or sets DownloadableResource db set.
        /// </summary>
        public DbSet<DownloadableResource> DownloadableResources { get; set; }

        /// <summary>
        /// Gets or sets EmbededResource db set.
        /// </summary>
        public DbSet<EmbededResource> EmbededResources { get; set; }

        /// <summary>
        /// Gets or sets Tickets db set.
        /// </summary>
        public DbSet<Ticket> Tickets { get; set; }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new AccountDbConfiguration());
            modelBuilder.ApplyConfiguration(new LicenseDbConfiguration());
            modelBuilder.ApplyConfiguration(new SuperAdminDbConfiguration());
            modelBuilder.ApplyConfiguration(new InstitutionDbConfiguration());
            modelBuilder.ApplyConfiguration(new TeacherDbConfiguration());
            modelBuilder.ApplyConfiguration(new AddressDbConfiguration());
            modelBuilder.Entity<Contact>().HasQueryFilter(c => c.IsDeleted == false);
            modelBuilder.Entity<Module>().HasQueryFilter(c => c.IsDeleted == false);
            modelBuilder.ApplyConfiguration(new LessonDbConfiguration());
            modelBuilder.ApplyConfiguration(new DownloadableResourceDbConfiguration());
            modelBuilder.ApplyConfiguration(new EmbededResourceDbConfiguration());
            modelBuilder.Entity<Ticket>().HasQueryFilter(c => c.IsDeleted == false);

            base.OnModelCreating(modelBuilder);
        }
    }
}
