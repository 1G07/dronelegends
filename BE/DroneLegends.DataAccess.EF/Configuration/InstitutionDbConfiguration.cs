﻿using DroneLegends.DataModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DroneLegends.DataAccess.EF.Configuration
{
    public class InstitutionDbConfiguration : IEntityTypeConfiguration<Institution>
    {
        public void Configure(EntityTypeBuilder<Institution> builder)
        {
            builder.HasQueryFilter(i => i.IsDeleted == false)
                .HasOne(i => i.Account)
                .WithOne()
                .HasForeignKey<Institution>(i => i.AccountId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasOne(i => i.Contact)
                .WithOne()
                .HasForeignKey<Institution>(c => c.ContactId);

            builder.HasOne(i => i.Address)
                .WithOne()
                .HasForeignKey<Institution>(i => i.AddressId);
            
            builder.HasOne(i => i.ParentInstitution)
                .WithOne()
                .HasForeignKey<Institution>(i => i.ParentId);
        }
    }
}
