﻿using DroneLegends.DataModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DroneLegends.DataAccess.EF.Configuration
{
    public class TeacherDbConfiguration : IEntityTypeConfiguration<Teacher>
    {
        public void Configure(EntityTypeBuilder<Teacher> builder)
        {
            builder.HasQueryFilter(t => t.IsDeleted == false)
                .HasOne(t => t.Account)
                .WithOne()
                .HasForeignKey<Teacher>(t => t.AccountId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasOne(i => i.Parent)
                .WithOne()
                .HasForeignKey<Teacher>(c => c.ParentId);
        }
    }
}
