﻿using DroneLegends.DataModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DroneLegends.DataAccess.EF.Configuration
{
    public class EmbededResourceDbConfiguration : IEntityTypeConfiguration<EmbededResource>
    {
        public void Configure(EntityTypeBuilder<EmbededResource> builder)
        {
            builder.HasQueryFilter(d => d.IsDeleted == false).HasOne(d => d.Lesson);
        }
    }
}
