﻿using DroneLegends.DataModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DroneLegends.DataAccess.EF.Configuration
{
    public class LicenseDbConfiguration : IEntityTypeConfiguration<License>
    {
        public void Configure(EntityTypeBuilder<License> builder)
        { 
            builder.HasQueryFilter(l => l.IsDeleted == false).HasOne(l => l.Owner);
        }
    }
}
