﻿using DroneLegends.DataModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DroneLegends.DataAccess.EF.Configuration
{
    public class DownloadableResourceDbConfiguration : IEntityTypeConfiguration<DownloadableResource>
    {
        public void Configure(EntityTypeBuilder<DownloadableResource> builder)
        {
            builder.HasQueryFilter(d => d.IsDeleted == false).HasOne(d => d.Module);
            
            builder.HasOne(d => d.Lesson);
        }
    }
}
