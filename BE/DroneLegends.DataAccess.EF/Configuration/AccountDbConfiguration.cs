﻿using DroneLegends.DataModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DroneLegends.DataAccess.EF.Configuration
{
    public class AccountDbConfiguration : IEntityTypeConfiguration<Account>
    {
        public void Configure(EntityTypeBuilder<Account> builder)
        {
            builder.HasQueryFilter(a => a.IsDeleted == false).HasOne(a => a.License);
        }
    }
}
