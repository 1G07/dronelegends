﻿using DroneLegends.DataModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DroneLegends.DataAccess.EF.Configuration
{
    public class SuperAdminDbConfiguration : IEntityTypeConfiguration<SuperAdmin>
    {
        public void Configure(EntityTypeBuilder<SuperAdmin> builder)
        {
            builder.HasQueryFilter(sa => sa.IsDeleted == false)
                .HasOne(sa => sa.Account)
                .WithOne()
                .HasForeignKey<SuperAdmin>(sa => sa.AccountId);
        }
    }
}
