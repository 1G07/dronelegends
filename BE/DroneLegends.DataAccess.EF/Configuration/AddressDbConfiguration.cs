﻿using DroneLegends.DataModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DroneLegends.DataAccess.EF.Configuration
{
    public class AddressDbConfiguration : IEntityTypeConfiguration<Address>
    {
        public void Configure(EntityTypeBuilder<Address> builder)
        {
            builder.ToTable("Addresses").HasQueryFilter(a => a.IsDeleted == false);
        }
    }
}
