using DroneLegends.API.Authorization;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using FluentMigrator.Runner;
using Newtonsoft.Json.Serialization;
using DroneLegends.BL.RequiredInterfaces;
using DroneLegends.DataAccess.EF.UnitOfWork;
using DroneLegends.DataMigrations.Version_0001;
using DroneLegends.BL.Services;
using DroneLegends.DataAccess.EF.Context;
using DroneLegends.API.Middleware;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;
using DroneLegends.BL.DomainModels;
using DroneLegends.API.BackgroundTasks;

namespace DroneLegends.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews().AddNewtonsoftJson(options => options.SerializerSettings.ContractResolver = new DefaultContractResolver() { NamingStrategy = new CamelCaseNamingStrategy() });

            services.AddDbContext<SourceContext>(
               options => options.UseSqlServer(this.Configuration.GetConnectionString("DbConnection")),
               ServiceLifetime.Transient,
               ServiceLifetime.Transient);

            services.Configure<MailConfiguration>(this.Configuration.GetSection("MailConfiguration"));
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddTransient<IAuthorizationHandler, AuthorizationHandler>();
            services.AddTransient<IUnitOfWorkFactory, UnitOfWorkFactory>();
            services.AddScoped<ContextService>();
            services.AddTransient<MailService>();
            services.AddTransient<AccountService>();
            services.AddTransient<InstitutionService>();
            services.AddTransient<LicenseService>();
            services.AddTransient<TeacherService>();
            services.AddTransient<ModuleService>();
            services.AddTransient<TicketService>();
            services.AddTransient<SuperAdminService>();
            services.AddTransient<NotificationEmailsService>();

            services.AddSpaStaticFiles(configuration => configuration.RootPath = "wwwroot");
            
            services.AddFluentMigratorCore().ConfigureRunner(runnerBuilder =>
            {
                runnerBuilder
                    .AddSqlServer()
                    .WithGlobalConnectionString(this.Configuration.GetConnectionString("DbConnection"))
                    .ScanIn(typeof(Migration_0001_Schema).Assembly).For.Migrations();
            });

            services.AddHostedService<SendNotificationEmailsTask>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(x => x.AllowAnyMethod().WithOrigins("*").AllowAnyHeader());
            app.UseMiddleware<ServerResourcesAuthMiddleware>();
            app.UseMiddleware<ValidationExceptionHandlingMiddlware>();
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSpaStaticFiles();

            app.UseRouting();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");
            });

            app.UseSpa(spa => spa.Options.SourcePath = "wwwroot");
        }
    }
}
