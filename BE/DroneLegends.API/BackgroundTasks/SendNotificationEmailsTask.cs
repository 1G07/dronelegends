﻿using System;
using System.Threading;
using System.Threading.Tasks;
using DroneLegends.BL.Exceptions;
using DroneLegends.BL.Services;
using Microsoft.Extensions.Hosting;

namespace DroneLegends.API.BackgroundTasks
{
    public class SendNotificationEmailsTask : BackgroundService
    {
        private const int StartHour = 6;
        private const int TimeoutMinutes = 30;
        private readonly NotificationEmailsService notificationEmailsService;

        public SendNotificationEmailsTask(NotificationEmailsService notificationEmailsService)
        {
            this.notificationEmailsService = notificationEmailsService;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            stoppingToken.Register(() => Console.WriteLine("SendNotificationEmailsTask is stopping."));

            DateTime lastDateTime = DateTime.Today.AddHours(StartHour).AddDays(-1);
            while (!stoppingToken.IsCancellationRequested)
            {
                DateTime now = DateTime.Now;
                if (lastDateTime.Date < now.Date && now.Hour == StartHour)
                {
                    try
                    {
                        this.notificationEmailsService.SendNotifications();
                    }
                    catch (BusinessException)
                    {
                    }

                    lastDateTime = DateTime.Today.AddHours(StartHour);
                }

                await Task.Delay(TimeSpan.FromMinutes(TimeoutMinutes), stoppingToken).ConfigureAwait(false);
            }
        }
    }
}