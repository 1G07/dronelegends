﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using DroneLegends.BL.Exceptions;
using DroneLegends.BL.RequiredInterfaces;
using DroneLegends.DataModels;

namespace DroneLegends.API.Middleware
{
    /// <summary>
    /// Middleware for protecting server resources.
    /// </summary>
    public class ServerResourcesAuthMiddleware
    {
        private readonly RequestDelegate next;

        private IUnitOfWorkFactory UnitOfWorkFactory { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ServerResourcesAuthMiddleware"/> class.
        /// </summary>
        /// <param name="next">Request delegate.</param>
        public ServerResourcesAuthMiddleware(RequestDelegate next, IUnitOfWorkFactory unitOfWorkFactory)
        {
            this.next = next;
            this.UnitOfWorkFactory = unitOfWorkFactory;
        }

        /// <summary>
        /// Invokes the handler.
        /// </summary>
        /// <param name="context">Context.</param>
        /// <returns>Task.</returns>
        public async Task Invoke(HttpContext context)
        {
            if (context.Request.Path.HasValue &&
               context.Request.Path.Value.Contains("PresentationResources/Fundamentals/Lesson1/index") ||
               context.Request.Path.Value.Contains("PresentationResources/Fundamentals/Lesson2/index") ||
               context.Request.Path.Value.Contains("PresentationResources/Fundamentals/Lesson3/index") ||
               context.Request.Path.Value.Contains("PresentationResources/Fundamentals/Lesson4/index") ||
               context.Request.Path.Value.Contains("PresentationResources/Fundamentals/Lesson5/index") ||
               context.Request.Path.Value.Contains("PresentationResources/Fundamentals/Lesson6/index") ||
               context.Request.Path.Value.Contains("PresentationResources/Fundamentals/Lesson7/index") ||
               context.Request.Path.Value.Contains("PresentationResources/Fundamentals/Lesson8/index") ||
               context.Request.Path.Value.Contains("PresentationResources/Fundamentals/Lesson9/index") ||
               context.Request.Path.Value.Contains("PresentationResources/Fundamentals/Lesson10/index"))
            {
                if (!context.Request.QueryString.Value.Contains("lessonId") || !context.Request.QueryString.Value.Contains("hash")) {
                    throw new UnauthorizedException("Not authorized to access this file.");
                }

                IUnitOfWork unitOfWork = this.UnitOfWorkFactory.CreateUnitOfWork();
                Lesson lesson = this.GetLessonFromContext(context, unitOfWork);
                lesson.PresentationHash = null;
                lesson.PresentationHashExpiry = null;
                unitOfWork.LessonRepository.UpdateEntity(lesson);
                unitOfWork.SaveChanges();
            }
            
            await next.Invoke(context);
        }

        private Lesson GetLessonFromContext(HttpContext context, IUnitOfWork unitOfWork)
        {
            string lessonIdQuery = context.Request.Query["lessonId"].ToString();
            Guid lessonId;

            try
            {
                lessonId = Guid.Parse(lessonIdQuery);
            }
            catch (ArgumentNullException)
            {
                throw new BusinessException("Lesson id format invalid!");
            }
            catch (FormatException)
            {
                throw new BusinessException("Lesson id format invalid!");
            }

            Lesson lesson = unitOfWork.LessonRepository.GetById(lessonId);

            if (lesson == null)
            {
                throw new BusinessException("Presentation can not be found for provided lesson.");
            }

            if (lesson.PresentationHash != context.Request.Query["hash"].ToString() || lesson.PresentationHashExpiry == null || lesson.PresentationHashExpiry < DateTime.Now)
            {
                throw new BusinessException("Presentation hash invalid!");
            }

            return lesson;
        }
    }
}
