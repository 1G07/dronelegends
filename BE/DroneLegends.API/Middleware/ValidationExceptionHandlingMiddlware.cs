﻿using System;
using System.Net;
using System.Threading.Tasks;
using DroneLegends.API.DataModels;
using DroneLegends.BL.Exceptions;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace DroneLegends.API.Middleware
{
    /// <summary>
    /// Middleware for exception handling.
    /// </summary>
    public class ValidationExceptionHandlingMiddlware
    {
        private readonly RequestDelegate next;

        /// <summary>
        /// Initializes a new instance of the <see cref="ValidationExceptionHandlingMiddlware"/> class.
        /// </summary>
        /// <param name="next">Request delegate.</param>
        public ValidationExceptionHandlingMiddlware(RequestDelegate next)
        {
            this.next = next;
        }

        /// <summary>
        /// Invokes the handler.
        /// </summary>
        /// <param name="context">Context.</param>
        /// <returns>Task.</returns>
        public async Task Invoke(HttpContext context)
        {
            try
            {
                await this.next(context).ConfigureAwait(false);
            }
            catch (UnauthenticatedException ex)
            {
                await HandleUnauthenticatedExceptionAsync(context, ex).ConfigureAwait(false);
            }
            catch (UnauthorizedException ex)
            {
                await HandleUnauthorizedExceptionAsync(context, ex).ConfigureAwait(false);
            }
            catch (BusinessException ex)
            {
                await HandleBusinessExceptionAsync(context, ex).ConfigureAwait(false);
            }
            catch (ArgumentNullException ex)
            {
                await HandleBusinessExceptionAsync(context, ex).ConfigureAwait(false);
            }
            catch (System.ComponentModel.DataAnnotations.ValidationException ex)
            {
                await HandleBusinessExceptionAsync(context, ex).ConfigureAwait(false);
            }
        }

        private static Task HandleUnauthenticatedExceptionAsync(HttpContext context, UnauthenticatedException ex)
        {
            var result = JsonConvert.SerializeObject(
                new HandledErrorModel()
                {
                    Code = "Unauthenticated",
                    Message = ex.Message,
                });

            return CreateExceptionResponse(context, HttpStatusCode.Unauthorized, result);
        }

        private static Task HandleUnauthorizedExceptionAsync(HttpContext context, UnauthorizedException ex)
        {
            var result = JsonConvert.SerializeObject(
                new HandledErrorModel()
                {
                    Code = "Unauthorized",
                    Message = ex.Message,
                });

            return CreateExceptionResponse(context, HttpStatusCode.Forbidden, result);
        }

        private static Task HandleBusinessExceptionAsync(HttpContext context, Exception ex)
        {
            var result = JsonConvert.SerializeObject(
                new HandledErrorModel()
                {
                    Code = "Bad request",
                    Message = ex.Message,
                });

            return CreateExceptionResponse(context, HttpStatusCode.BadRequest, result);
        }

        private static Task CreateExceptionResponse(HttpContext context, HttpStatusCode code, string result)
        {
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)code;
            return context.Response.WriteAsync(result);
        }
    }
}
