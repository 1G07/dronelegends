﻿using System;
using System.Linq;
using System.Threading.Tasks;
using DroneLegends.BL.Exceptions;
using DroneLegends.BL.Services;
using DroneLegends.DataModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authorization.Infrastructure;
using Microsoft.AspNetCore.Http;

namespace DroneLegends.API.Authorization
{
    public class AuthorizationHandler : AuthorizationHandler<RolesAuthorizationRequirement>
    {
        private readonly HttpContext httpContext;
        
        private readonly AccountService accountService;

        private readonly ContextService contextService;

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthorizationHandler"/> class.
        /// </summary>
        /// <param name="accountService">Account service.</param>
        /// <param name="httpContextAccessor">Http Context Accessor.</param>
        public AuthorizationHandler(AccountService accountService, IHttpContextAccessor httpContextAccessor, ContextService contextService)
        {
            this.accountService = accountService;
            this.httpContext = httpContextAccessor.HttpContext;
            this.contextService = contextService;
        }

        /// <summary>
        /// Implementation of base method. It contains authorization logic.
        /// </summary>
        /// <param name="context">Authorization context.</param>
        /// <param name="requirement">Authorization requirement.</param>
        /// <returns>Task.</returns>
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, RolesAuthorizationRequirement requirement)
        {
            this.ValidateTokenHeader();

            string token = this.ExtractTokenFromHeader(this.httpContext.Request.Headers["Authorization"]);

            Account account = this.GetAccountByToken(token);

            bool isAuthenticated = account != null;

            if (!isAuthenticated)
            {
                throw new UnauthenticatedException("Access token is invalid.");
            }

            if (account.License == null || account.License.ExpiryDate == null || account.License.ExpiryDate < DateTime.Now)
            {
                throw new BusinessException("License is expired. Please contact the support to renew the license.");
            }

            if (account.IsLockedDueToLicenseChange)
            {
                throw new BusinessException("Account is locked due to license downgrade.");
            }

            bool isAuthorized = requirement.AllowedRoles.Contains(account.Role.ToString(), StringComparer.OrdinalIgnoreCase);

            if (!isAuthorized)
            {
                throw new UnauthorizedException("Not authorized to access this action.");
            }

            this.httpContext.Items.Add("CurrentAccount", account);

            context.Succeed(requirement);

            this.contextService.CurrentAccount = account;

            return Task.CompletedTask;
        }

        private string ExtractTokenFromHeader(string authorizationHeader)
        {
            return authorizationHeader.Split(' ', 2, StringSplitOptions.RemoveEmptyEntries)[1]?.Trim();
        }

        private void ValidateTokenHeader()
        {
            string tokenHeader = this.httpContext.Request.Headers["Authorization"];

            if (string.IsNullOrWhiteSpace(tokenHeader))
            {
                throw new UnauthenticatedException("Access token empty or not found.");
            }

            string[] tokenHeaderParts = tokenHeader.Split(' ', 2, StringSplitOptions.RemoveEmptyEntries);

            if (tokenHeaderParts.Length != 2)
            {
                throw new UnauthenticatedException("Invalid access token format.");
            }

            // Get the token type
            string tokenType = tokenHeaderParts[0]?.Trim();

            if (tokenType != "Bearer")
            {
                throw new UnauthenticatedException("Invalid access token type.");
            }
        }

        private Account GetAccountByToken(string token)
        {
            Account account;

            try
            {
                account = this.accountService.GetAccount(token);
            }
            catch (BusinessException)
            {
                account = null;
            }

            return account;
        }
    }
}
