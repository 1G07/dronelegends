﻿using Microsoft.AspNetCore.Authorization;

namespace DroneLegends.API.Authorization
{
    /// <summary>
    /// Authorization attribute.
    /// </summary>
    public sealed class AuthorizationAttribute : AuthorizeAttribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AuthorizationAttribute"/> class.
        /// </summary>
        public AuthorizationAttribute()
            : base()
        {
        }
    }
}
