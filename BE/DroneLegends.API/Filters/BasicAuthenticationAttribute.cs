﻿using System;
using System.Globalization;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Primitives;

namespace DroneLegends.API.Filters
{
    public sealed class BasicAuthenticationAttribute : ActionFilterAttribute
    {
        public BasicAuthenticationAttribute()
        {
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            IConfiguration configuration = context.HttpContext.RequestServices.GetService<IConfiguration>();

            var usernameConfig = configuration.GetValue<string>("BasicAuth:Username");
            var passwordConfig = configuration.GetValue<string>("BasicAuth:Password");

            var request = context.HttpContext.Request;
            StringValues auth = request.Headers["Authorization"];

            if (string.IsNullOrEmpty(auth))
            {
                this.SetUnauthorized(context, configuration);
                return;
            }

            var startString = "Basic ";
            var cred = ASCIIEncoding.ASCII.GetString(Convert.FromBase64String(auth[0].Substring(startString.Length))).Split(':');

            if (cred.Length < 2)
            {
                this.SetUnauthorized(context, configuration);
                return;
            }

            var username = cred[0];
            var password = cred[1];

            if (username == usernameConfig && password == passwordConfig)
            {
                // success
                return;
            }

            this.SetUnauthorized(context, configuration);
            return;
        }

        private void SetUnauthorized(ActionExecutingContext filterContext, IConfiguration configuration)
        {
            var realm = configuration.GetValue<string>("BasicAuth:Realm");

            filterContext.HttpContext.Response.Headers["WWW-Authenticate"] = string.Format(CultureInfo.InvariantCulture, "Basic realm=\"{0}\"", realm);
            filterContext.Result = new UnauthorizedResult();
        }
    }
}