﻿using DroneLegends.DataModels;
using System.ComponentModel.DataAnnotations;

namespace DroneLegends.API.DataModels
{
    public class ContactModel
    {
        public ContactModel()
        {
        }

        public ContactModel(Contact entity)
        {
            this.Title = entity.Title;
            this.Email = entity.Email;
            this.FirstName = entity.FirstName;
            this.LastName = entity.LastName;
            this.Phone = entity.Phone;
        }

        public string Title { get; set; }

        public string Email { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Phone { get; set; }
    }
}
