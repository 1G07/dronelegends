﻿using System;
using DroneLegends.DataModels;
using DroneLegends.DataModels.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace DroneLegends.API.DataModels
{
    public class TeacherSaveResponseModel
    {
        public TeacherSaveResponseModel()
        {
        }

        public TeacherSaveResponseModel(Teacher entity)
        {
            this.Id = entity.Id;
            this.Email = entity.Account.Email;
            this.RegistrationStatus = entity.Account.RegistrationStatus;
            this.ShouldSendEmailCopy = entity.Account.ShouldSendEmailCopy;
            this.FirstName = entity.FirstName;
            this.LastName = entity.LastName;
            this.Title = entity.Title;
            this.Phone = entity.Phone;
            this.AvatarImage = entity.Account.AvatarImage;

            if (entity.Parent != null)
            {
                this.ParentInstitution = new InstitutionSaveResponseModel(entity.Parent);
            }
        }

        public Guid? Id { get; set; }
        
        public string Email { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public RegistrationStatus RegistrationStatus { get; set; }
        
        public bool? ShouldSendEmailCopy { get; set; }

        /// <summary>
        /// Gets or sets FirstName.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets LastName.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets Title.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets Phone.
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Gets or sets Avatar image.
        /// </summary>
        public string AvatarImage { get; set; }

        public InstitutionSaveResponseModel ParentInstitution { get; set; }
    }
}
