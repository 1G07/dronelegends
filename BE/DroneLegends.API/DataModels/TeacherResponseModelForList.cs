﻿using System;
using System.Collections.Generic;
using DroneLegends.BL.DomainModels;
using DroneLegends.DataModels;
using DroneLegends.DataModels.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace DroneLegends.API.DataModels
{
    public class TeacherResponseModelForList
    {
        public TeacherResponseModelForList()
        {
        }

        public TeacherResponseModelForList(Teacher entity, List<DroneLegends.DataModels.Module> modules)
        {
            this.Id = entity.Id;
            this.Email = entity.Account.Email;
            this.RegistrationStatus = entity.Account.RegistrationStatus;
            this.FirstName = entity.FirstName;
            this.LastName = entity.LastName;
            this.Title = entity.Title;
            this.Phone = entity.Phone;
            this.ShouldSendEmailCopy = entity.Account.ShouldSendEmailCopy;

            if (entity.Parent != null)
            {
                this.ParentInstitution = new InstitutionResponseModelWithoutDetails(entity.Parent, modules);
                this.License = new LicenseModel(new LicenseDO(entity.Parent.Account.License, modules));
            }
        }

        public Guid? Id { get; set; }
        
        public string Email { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public RegistrationStatus RegistrationStatus { get; set; }

        /// <summary>
        /// Gets or sets FirstName.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets LastName.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets Title.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets Phone.
        /// </summary>
        public string Phone { get; set; }
        
        /// <summary>
        /// Gets or sets value indicating should the email copy be sent.
        /// </summary>
        public bool? ShouldSendEmailCopy { get; set; }

        public LicenseModel License { get; set; }
        
        public InstitutionResponseModelWithoutDetails ParentInstitution { get; set; }
    }
}
