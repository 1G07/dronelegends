﻿using System;
using System.Collections.Generic;
using DroneLegends.BL.DomainModels;

namespace DroneLegends.API.DataModels
{
    public class LicenseModel
    {
        public LicenseModel()
        {
        }

        public LicenseModel(LicenseDO licenseDO)
        {
            if (licenseDO != null)
            {
                this.Id = licenseDO.License.Id.Value;

                if (licenseDO.License.StartDate != null)
                {
                    this.StartDate = ((DateTimeOffset)licenseDO.License.StartDate).ToUnixTimeSeconds();
                }
                
                if (licenseDO.License.ExpiryDate != null)
                {
                    this.ExpiryDate = ((DateTimeOffset)licenseDO.License.ExpiryDate).ToUnixTimeSeconds();
                }

                this.OwnerId = licenseDO.License.Owner.Id.Value;
                this.OwnerName = licenseDO.License.Owner.Name;
                this.TotalSchoolAccounts = licenseDO.License.NumberOfSchools;
                this.UsedSchoolAccounts = licenseDO.License.UsedSchoolAccounts;
                this.TotalTeacherAccounts = licenseDO.License.NumberOfTeachers;
                this.DurationInMonths = licenseDO.License.DurationInMonths;
                this.UsedTeacherAccounts = licenseDO.License.UsedTeacherAccounts != null ? licenseDO.License.UsedTeacherAccounts : 0;
                this.UsedSchoolAccountsPerDistrict = licenseDO.License.UsedSchoolAccountsPerDistrict != null ? licenseDO.License.UsedSchoolAccountsPerDistrict : 0;
                this.Modules = new List<Module>();

                foreach (DroneLegends.DataModels.Module module in licenseDO.Modules)
                {
                    this.Modules.Add(new Module(module));
                }
            }
        }

        public Guid Id { get; set; }

        public long? StartDate { get; set; }

        public long? ExpiryDate { get; set; }

        public Guid OwnerId { get; set; }
        
        public string OwnerName { get; set; }
        
        public int TotalSchoolAccounts { get; set; }
        
        public int UsedSchoolAccounts { get; set; }
        
        public int? TotalTeacherAccounts { get; set; }
        
        public int DurationInMonths { get; set; }
        
        public int? UsedTeacherAccounts { get; set; }
        
        public int? UsedSchoolAccountsPerDistrict { get; set; }
        
        public List<Module> Modules { get; set; }
    }
}
