﻿using System.Collections.Generic;
using DroneLegends.DataModels;

namespace DroneLegends.API.DataModels
{
    public class TeacherResponseModel: TeacherResponseModelForList
    {
        public TeacherResponseModel()
        {
        }

        public TeacherResponseModel(Teacher entity, List<DroneLegends.DataModels.Module> modules)
            :base (entity, modules)
        {
            this.AvatarImage = entity.Account.AvatarImage;
        }

        /// <summary>
        /// Gets or sets Avatar image.
        /// </summary>
        public string AvatarImage { get; set; }
    }
}
