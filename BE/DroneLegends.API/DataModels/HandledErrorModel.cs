﻿using Newtonsoft.Json;

namespace DroneLegends.API.DataModels
{
    /// <summary>
    /// Handled error data model. Should be used when returning handled error to client.
    /// </summary>
    public class HandledErrorModel
    {
        /// <summary>
        /// Gets or sets Code.
        /// </summary>
        [JsonProperty("code")]
        public string Code { get; set; }

        /// <summary>
        /// Gets or sets Message.
        /// </summary>
        [JsonProperty("message")]
        public string Message { get; set; }
    }
}
