﻿using System;

namespace DroneLegends.API.DataModels
{
    public class ModuleDetails
    {
        public ModuleDetails()
        {

        }

        public ModuleDetails(DroneLegends.DataModels.Module entity)
        {
            this.Id = entity.Id.Value;
            this.Name = entity.Name;
            this.Description = entity.Description;
            this.Image = entity.Image;
        }

        public Guid Id { get; set; }

        public string Name { get; set; }
        
        public string Description { get; set; }
        
        public string Image { get; set; }
    }
}
