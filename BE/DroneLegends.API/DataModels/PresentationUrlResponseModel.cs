﻿namespace DroneLegends.API.DataModels
{
    public class PresentationUrlResponseModel
    {
        public string Url { get; set; }
    }
}
