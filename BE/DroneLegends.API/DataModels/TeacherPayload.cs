﻿using System;
using System.ComponentModel.DataAnnotations;
using DroneLegends.DataModels;

namespace DroneLegends.API.DataModels
{
    public class TeacherPayload
    {
        public Guid? Id { get; set; }
        
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        public string AvatarImage { get; set; }

        /// <summary>
        /// Gets or sets FirstName.
        /// </summary>
        [Required]
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets LastName.
        /// </summary>
        [Required]
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets Title.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets Phone.
        /// </summary>
        public string Phone { get; set; }
        
        public bool? ShouldSendEmailCopy { get; set; }

        /// <summary>
        /// Gets or sets ParentId.
        /// </summary>
        public Guid? ParentId { get; set; }

        public Teacher ToEntity()
        {
            Teacher result = new Teacher()
            {
                FirstName = this.FirstName,
                LastName = this.LastName,
                Title = this.Title,
                Phone = this.Phone,
                ParentId = this.ParentId ?? Guid.Empty,
                Account = new Account() { Email = Email, Role = DroneLegends.DataModels.Enums.Role.Teacher, AvatarImage = this.AvatarImage, FirstName = this.FirstName, LastName = this.LastName, ShouldSendEmailCopy = this.ShouldSendEmailCopy },
            };

            return result;
        }
    }
}
