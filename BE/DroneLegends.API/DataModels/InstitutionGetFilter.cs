﻿using System;
using System.ComponentModel.DataAnnotations;
using DroneLegends.DataModels;
using DroneLegends.DataModels.Enums;

namespace DroneLegends.API.DataModels
{
    public class InstitutionGetFilter
    {
        public string Input { get; set; }

        public Role? Role { get; set; }
        
        public Guid? ParentId { get; set; }

        [Required]
        [Range(-1, 100, ErrorMessage = "Only positive numbers in range 1-100 allowed")]
        public int Limit { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Only positive number allowed")]
        public int Offset { get; set; }

        /// <summary>
        /// Transforms view model to entity.
        /// </summary>
        /// <returns>UserFilter entity.</returns>
        public InstitutionFilter ToEntity()
        {
            return new InstitutionFilter()
            {
                Input = this.Input,
                Role = this.Role,
                ParentId = this.ParentId,
                Limit = this.Limit,
                Offset = this.Offset
            };
        }
    }
}
