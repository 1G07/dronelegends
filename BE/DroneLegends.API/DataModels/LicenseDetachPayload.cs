﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DroneLegends.API.DataModels
{
    public class LicenseDetachPayload
    {
        [Required]
        [EmailAddress]
        public string AccountEmail { get; set; }
    }
}
