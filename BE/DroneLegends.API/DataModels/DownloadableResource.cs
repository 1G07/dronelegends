﻿using System;

namespace DroneLegends.API.DataModels
{
    public class DownloadableResource
    {
        public DownloadableResource()
        {

        }

        public DownloadableResource(DroneLegends.DataModels.DownloadableResource entity)
        {
            this.Id = entity.Id.Value;
            this.Name = entity.Name;
        }

        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}
