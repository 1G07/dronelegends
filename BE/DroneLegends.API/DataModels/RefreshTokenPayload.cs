﻿using System.ComponentModel.DataAnnotations;

namespace DroneLegends.API.DataModels
{
    /// <summary>
    /// Refresh token payload data model.
    /// </summary>
    public class RefreshTokenPayload
    {
        /// <summary>
        /// Gets or sets RefreshToken.
        /// </summary>
        [Required]
        public string RefreshToken { get; set; }
    }
}
