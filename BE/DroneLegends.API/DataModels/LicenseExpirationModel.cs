﻿using System;
using System.Collections.Generic;
using DroneLegends.BL.DomainModels;

namespace DroneLegends.API.DataModels
{
    public class LicenseExpirationModel
    {
        public LicenseExpirationModel()
        {
        }

        public long? ExpiryDate { get; set; }
    }
}
