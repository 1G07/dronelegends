﻿using DroneLegends.DataModels;
using System.ComponentModel.DataAnnotations;

namespace DroneLegends.API.DataModels
{
    public class AddressModel
    {
        public AddressModel()
        {
        }

        public AddressModel(Address entity)
        {
            this.Street = entity.Street;
            this.City = entity.City;
            this.State = entity.State;
            this.PostCode = entity.PostCode;
        }

        public string Street { get; set; }

        [Required]
        public string City { get; set; }

        [Required]
        public string State { get; set; }

        public string PostCode { get; set; }
    }
}
