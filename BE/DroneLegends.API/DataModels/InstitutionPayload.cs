﻿using System;
using System.ComponentModel.DataAnnotations;
using DroneLegends.DataModels;
using DroneLegends.DataModels.Enums;

namespace DroneLegends.API.DataModels
{
    public class InstitutionPayload
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        public string AvatarImage { get; set; }
        
        public bool? ShouldSendEmailCopy { get; set; }
        
        public Guid? ParentId { get; set; }

        [Required]
        [Range(2, 5, ErrorMessage = "Role is not valid for an institution.")]
        public Role? Role { get; set; }

        [Required]
        public AddressModel Address { get; set; }

        public ContactModel Contact { get; set; }

        public Institution ToEntity()
        {
            Institution result =  new Institution()
            {
                Name = this.Name,
                ParentId = this.ParentId,
                Account = new Account() { Email = Email, Role = this.Role.Value, AvatarImage = this.AvatarImage, FirstName = this.FirstName, LastName = this.LastName, ShouldSendEmailCopy = this.ShouldSendEmailCopy },
                Address = new Address() { City = this.Address.City, Street = this.Address.Street, PostCode = this.Address.PostCode, State = this.Address.State }
            };

            if (this.Contact != null)
            {
                result.Contact = new Contact()
                {
                    Title = this.Contact.Title,
                    Email = this.Contact.Email,
                    FirstName = this.Contact.FirstName,
                    LastName = this.Contact.LastName,
                    Phone = this.Contact.Phone
                };
            }

            return result;
        }
    }
}
