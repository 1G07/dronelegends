﻿using System;
using System.ComponentModel.DataAnnotations;
using DroneLegends.DataModels;

namespace DroneLegends.API.DataModels
{
    public class SuperAdminPayload
    {
        public Guid? Id { get; set; }
        
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        public string AvatarImage { get; set; }

        /// <summary>
        /// Gets or sets FirstName.
        /// </summary>
        [Required]
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets LastName.
        /// </summary>
        [Required]
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets Title.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets Phone.
        /// </summary>
        public string Phone { get; set; }
        
        public bool? ShouldSendEmailCopy { get; set; }

        public SuperAdmin ToEntity()
        {
            SuperAdmin result = new SuperAdmin()
            {
                Phone = this.Phone,
                Account = new Account() { Email = Email, Role = DroneLegends.DataModels.Enums.Role.SuperAdmin, AvatarImage = this.AvatarImage, FirstName = this.FirstName, LastName = this.LastName, ShouldSendEmailCopy = this.ShouldSendEmailCopy },
            };

            return result;
        }
    }
}
