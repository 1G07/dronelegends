﻿namespace DroneLegends.API.DataModels
{
    public class EmbededResource
    {
        public EmbededResource()
        {

        }

        public EmbededResource(DroneLegends.DataModels.EmbededResource entity)
        {
            this.Url = entity.Url;
            this.Title = entity.Title;
        }

        public string Url { get; set; }
        
        public string Title { get; set; }
    }
}
