﻿using System;
using System.ComponentModel.DataAnnotations;
using DroneLegends.DataModels;

namespace DroneLegends.API.DataModels
{
    public class TeacherFilterQuery
    {
        public string Input { get; set; }
        
        public Guid? ParentId { get; set; }

        [Required]
        [Range(-1, 100, ErrorMessage = "Only positive numbers in range 1-100 allowed")]
        public int Limit { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Only positive number allowed")]
        public int Offset { get; set; }

        /// <summary>
        /// Transforms view model to entity.
        /// </summary>
        /// <returns>TeacherFilter entity.</returns>
        public TeacherFilter ToEntity()
        {
            return new TeacherFilter()
            {
                Input = this.Input,
                ParentId = this.ParentId,
                Limit = this.Limit,
                Offset = this.Offset
            };
        }
    }
}
