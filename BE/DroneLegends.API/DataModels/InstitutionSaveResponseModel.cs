﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using DroneLegends.DataModels;
using DroneLegends.DataModels.Enums;

namespace DroneLegends.API.DataModels
{
    public class InstitutionSaveResponseModel
    {
        public InstitutionSaveResponseModel()
        {
        }

        public InstitutionSaveResponseModel(Institution entity)
        {
            this.Id = entity.Id.Value;
            this.Email = entity.Account.Email;
            this.Role = entity.Account.Role;
            this.RegistrationStatus = entity.Account.RegistrationStatus;
            this.Name = entity.Name;
            this.FirstName = entity.Account.FirstName;
            this.LastName = entity.Account.LastName;
            this.AvatarImage = entity.Account.AvatarImage;
            this.ShouldSendEmailCopy = entity.Account.ShouldSendEmailCopy;
            this.IsLockedDueToLicenseChange = entity.Account.IsLockedDueToLicenseChange;
            this.Address = new AddressModel(entity.Address);

            if (entity.Contact != null)
            {
                this.Contact = new ContactModel(entity.Contact);
            }
            
            if (entity.ParentInstitution != null)
            {
                this.ParentInstitution = new InstitutionSaveResponseModel(entity.ParentInstitution);
            }
        }
        
        public Guid Id { get; set; }

        public string Email { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public Role Role { get; set; }
        
        [JsonConverter(typeof(StringEnumConverter))]
        public RegistrationStatus RegistrationStatus { get; set; }
        
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets FirstName.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets LastName.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets Avatar image.
        /// </summary>
        public string AvatarImage { get; set; }
        
        public bool? ShouldSendEmailCopy { get; set; }
        
        public bool IsLockedDueToLicenseChange { get; set; }

        public AddressModel Address { get; set; }

        public ContactModel Contact { get; set; }
        
        public InstitutionSaveResponseModel ParentInstitution { get; set; }
    }
}
