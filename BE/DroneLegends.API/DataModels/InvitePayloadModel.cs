﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DroneLegends.API.DataModels
{
    /// <summary>
    /// User invite payload.
    /// </summary>
    public class InvitePayloadModel
    {
        /// <summary>
        /// Gets or sets Redirect url.
        /// </summary>
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets Url.
        /// </summary>
        [Required]
        public Uri Url { get; set; }
    }
}
