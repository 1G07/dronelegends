﻿using System.ComponentModel.DataAnnotations;

namespace DroneLegends.API.DataModels
{
    /// <summary>
    /// Payload for change password request.
    /// </summary>
    public class ChangePasswordPayloadModel
    {
        /// <summary>
        /// Gets or sets Old Password.
        /// </summary>
        [Required]
        public string OldPassword { get; set; }

        /// <summary>
        /// Gets or sets New Password.
        /// </summary>
        [Required]
        [RegularExpression(@"^(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[0-9]).{8,}$")]
        public string NewPassword { get; set; }

        /// <summary>
        /// Gets or sets New Password Confirmation.
        /// </summary>
        [Required]
        [RegularExpression(@"^(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[0-9]).{8,}$")]
        public string NewPasswordConf { get; set; }
    }
}
