﻿using System;
using DroneLegends.DataModels;

namespace DroneLegends.API.DataModels
{
    public class SuperAdminSaveResponseModel
    {
        public SuperAdminSaveResponseModel()
        {
        }

        public SuperAdminSaveResponseModel(SuperAdmin entity)
        {
            this.Id = entity.Id;
            this.Email = entity.Account.Email;
            this.FirstName = entity.Account.FirstName;
            this.LastName = entity.Account.LastName;
            this.Phone = entity.Phone;
            this.AvatarImage = entity.Account.AvatarImage;
            this.ShouldSendEmailCopy = entity.Account.ShouldSendEmailCopy;
        }

        public Guid? Id { get; set; }
        
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets FirstName.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets LastName.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets Phone.
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Gets or sets Avatar image.
        /// </summary>
        public string AvatarImage { get; set; }
        
        public bool? ShouldSendEmailCopy { get; set; }
    }
}
