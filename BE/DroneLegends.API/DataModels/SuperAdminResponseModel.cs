﻿using DroneLegends.DataModels;

namespace DroneLegends.API.DataModels
{
    public class SuperAdminResponseModel : SuperAdminResponseModelForList
    {
        public SuperAdminResponseModel()
        {
        }

        public SuperAdminResponseModel(SuperAdmin entity)
            :base (entity)
        {
            this.AvatarImage = entity.Account.AvatarImage;
        }

        /// <summary>
        /// Gets or sets Avatar image.
        /// </summary>
        public string AvatarImage { get; set; }
    }
}
