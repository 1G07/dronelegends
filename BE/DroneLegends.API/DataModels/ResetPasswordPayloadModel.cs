﻿using System.ComponentModel.DataAnnotations;

namespace DroneLegends.API.DataModels
{
    /// <summary>
    /// Reset password payload.
    /// </summary>
    public class ResetPasswordPayloadModel
    {
        /// <summary>
        /// Gets or sets Reset Code.
        /// </summary>
        [Required]
        public string ResetCode { get; set; }

        /// <summary>
        /// Gets or sets New Password.
        /// </summary>
        [Required]
        [RegularExpression(@"^(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[0-9]).{8,}$")]
        public string NewPassword { get; set; }

        /// <summary>
        /// Gets or sets New Password Confirmation.
        /// </summary>
        [Required]
        [RegularExpression(@"^(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[0-9]).{8,}$")]
        public string NewPasswordConf { get; set; }
    }
}
