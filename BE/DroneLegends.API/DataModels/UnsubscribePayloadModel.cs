﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DroneLegends.API.DataModels
{
    /// <summary>
    /// Unsubscribe Payload Model.
    /// </summary>
    public class UnsubscribePayloadModel
    {
        /// <summary>
        /// Gets or sets Token.
        /// </summary>
        [Required]
        public string Token { get; set; }
    }
}
