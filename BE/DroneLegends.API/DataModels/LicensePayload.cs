﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DroneLegends.DataModels;

namespace DroneLegends.API.DataModels
{
    public class LicensePayload
    {
        private const int DefaultNumberOfTeachers = 5;

        public int? TotalSchoolAccounts { get; set; }

        public int? TotalTeacherAccounts { get; set; }

        [Required]
        public int DurationInMonths { get; set; }

        [Required]
        public List<Guid> Modules { get; set; }

        public License ToEntity()
        {
            if (this.TotalSchoolAccounts == null)
            {
                this.TotalSchoolAccounts = 0;
            }

            if (this.TotalSchoolAccounts < 0)
            {
                throw new ValidationException("Number of schools must be positive number!");
            }

            if (this.TotalTeacherAccounts == null || this.TotalTeacherAccounts < 0)
            {
                this.TotalTeacherAccounts = DefaultNumberOfTeachers;
            }

            return new License() { NumberOfSchools = this.TotalSchoolAccounts.Value, NumberOfTeachers = this.TotalTeacherAccounts.Value, DurationInMonths = this.DurationInMonths };
        }
    }
}
