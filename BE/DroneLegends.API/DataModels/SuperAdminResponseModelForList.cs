﻿using System;
using DroneLegends.DataModels;

namespace DroneLegends.API.DataModels
{
    public class SuperAdminResponseModelForList
    {
        public SuperAdminResponseModelForList()
        {
        }

        public SuperAdminResponseModelForList(SuperAdmin entity)
        {
            this.Id = entity.Id;
            this.Email = entity.Account.Email;
            this.FirstName = entity.Account.FirstName;
            this.LastName = entity.Account.LastName;
            this.ShouldSendEmailCopy = entity.Account.ShouldSendEmailCopy;
            this.Phone = entity.Phone;
        }

        public Guid? Id { get; set; }
        
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets FirstName.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets LastName.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets Phone.
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Gets or sets ShouldSendEmailCopy.
        /// </summary>
        public bool? ShouldSendEmailCopy { get; set; }
    }
}
