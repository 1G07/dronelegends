﻿using DroneLegends.DataModels;
using DroneLegends.DataModels.Enums;

namespace DroneLegends.API.DataModels
{
    public class TicketPayload
    {
        /// <summary>
        /// Gets or sets Type.
        /// </summary>
        public TicketType Type { get; set; }

        /// <summary>
        /// Gets or sets Text.
        /// </summary>
        public string Text { get; set; }

        public Ticket ToEntity()
        {
            return new Ticket() { Type = this.Type, Text = this.Text };
        }
    }
}
