﻿using System;

namespace DroneLegends.API.DataModels
{
    public class LessonOverview
    {
        public LessonOverview()
        {

        }

        public LessonOverview(DroneLegends.DataModels.Lesson entity)
        {
            this.Id = entity.Id.Value;
            this.Name = entity.Name;
            this.Description = entity.Description;
            this.Introduction = entity.Introduction;
            this.Image = entity.Image;
            this.Parent = new Module(entity.Module);
        }

        public Guid Id { get; set; }

        public string Name { get; set; }
        
        public string Description { get; set; }
        
        public string Introduction { get; set; }
        
        public string Image { get; set; }

        public Module Parent { get; set; }
    }
}
