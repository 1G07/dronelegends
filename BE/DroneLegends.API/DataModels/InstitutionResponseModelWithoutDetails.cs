﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using DroneLegends.DataModels;
using DroneLegends.DataModels.Enums;
using System.Collections.Generic;
using DroneLegends.BL.DomainModels;

namespace DroneLegends.API.DataModels
{
    public class InstitutionResponseModelWithoutDetails
    {
        public InstitutionResponseModelWithoutDetails()
        {
        }

        public InstitutionResponseModelWithoutDetails(Institution entity, List<DroneLegends.DataModels.Module> modules)
        {
            this.Id = entity.Id;
            this.Email = entity.Account.Email;
            this.Role = entity.Account.Role;
            this.RegistrationStatus = entity.Account.RegistrationStatus;
            this.ShouldSendEmailCopy = entity.Account.ShouldSendEmailCopy;
            this.IsLockedDueToLicenseChange = entity.Account.IsLockedDueToLicenseChange;
            this.Name = entity.Name;
            this.FirstName = entity.Account.FirstName;
            this.LastName = entity.Account.LastName;
            this.Address = new AddressModel(entity.Address);

            if (entity.Contact != null)
            {
                this.Contact = new ContactModel(entity.Contact);
            }

            if (entity.Account.License != null)
            {
                this.License = new LicenseModel(new LicenseDO(entity.Account.License, modules));
            }

            if (entity.ParentInstitution != null)
            {
                entity.ParentInstitution.Account.License = entity.Account.License;
                this.ParentInstitution = new InstitutionResponseModelWithoutDetails(entity.ParentInstitution, modules);
            }
        }

        public Guid? Id { get; set; }
        
        public string Email { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public Role Role { get; set; }
        
        [JsonConverter(typeof(StringEnumConverter))]
        public RegistrationStatus RegistrationStatus { get; set; }
        
        public bool? ShouldSendEmailCopy { get; set; }

        public bool IsLockedDueToLicenseChange { get; set; }
        
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets FirstName.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets LastName.
        /// </summary>
        public string LastName { get; set; }

        public ContactModel Contact { get; set; }
        
        public AddressModel Address { get; set; }
     
        public LicenseModel License { get; set; }

        /// <summary>
        /// Gets or sets ParentInstitution.
        /// </summary>
        public InstitutionResponseModelWithoutDetails ParentInstitution { get; set; }
    }
}
