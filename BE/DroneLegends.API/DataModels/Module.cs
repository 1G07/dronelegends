﻿using System;

namespace DroneLegends.API.DataModels
{
    public class Module
    {
        public Module()
        {

        }

        public Module(DroneLegends.DataModels.Module entity)
        {
            this.Id = entity.Id;
            this.Name = entity.Name;
        }

        public Guid? Id { get; set; }

        public string Name { get; set; }
    }
}
