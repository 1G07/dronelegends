﻿using DroneLegends.DataModels;
using DroneLegends.DataModels.Enums;

namespace DroneLegends.API.DataModels
{
    /// <summary>
    /// Login response.
    /// </summary>
    public class LoginResponseModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LoginResponseModel"/> class.
        /// </summary>
        public LoginResponseModel()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LoginResponseModel"/> class.
        /// </summary>
        /// <param name="source">Entity.</param>
        public LoginResponseModel(Account source)
        {
            this.FirstName = source.FirstName;
            this.LastName = source.LastName;
            this.Token = source.AccessToken;
            this.RefreshToken = source.RefreshToken;
            this.TokenExpiry = source.AccessTokenExpiryTime;
            
            string role = source.Role.ToString();
            this.Role = $"{char.ToLowerInvariant(role[0])}{role.Substring(1)}";

        }

        /// <summary>
        /// Gets or sets FirstName.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets LastName.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets token.
        /// </summary>
        public string Token { get; set; }

        /// <summary>
        /// Gets or sets token expiry.
        /// </summary>
        public long TokenExpiry { get; set; }

        /// <summary>
        /// Gets or sets refresh token.
        /// </summary>
        public string RefreshToken { get; set; }
        
        /// <summary>
        /// Gets or sets role.
        /// </summary>
        public string Role { get; set; }
    }
}
