﻿using System.Collections.Generic;
using DroneLegends.BL.DomainModels;

namespace DroneLegends.API.DataModels
{
    public class LessonDetails : LessonOverview
    {
        public LessonDetails()
        {
            this.DownloadableResources = new List<DownloadableResource>();
        }

        public LessonDetails(LessonDO entity) : base(entity.Lesson)
        {
            this.PresentationCoverImage = entity.Lesson.PresentationCoverImage;

            this.DownloadableResources = new List<DownloadableResource>();

            foreach (DroneLegends.DataModels.DownloadableResource dm in entity.DownloadableResources)
            {
                this.DownloadableResources.Add(new DownloadableResource(dm));
            }
            
            this.EmbededResources = new List<EmbededResource>();

            foreach (DroneLegends.DataModels.EmbededResource er in entity.EmbededResources)
            {
                this.EmbededResources.Add(new EmbededResource(er));
            }
        }

        public List<DownloadableResource> DownloadableResources { get; set; }
        
        public List<EmbededResource> EmbededResources { get; set; }
        
        public string PresentationCoverImage { get; set; }
    }
}
