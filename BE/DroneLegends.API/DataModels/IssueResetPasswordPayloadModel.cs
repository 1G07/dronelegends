﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DroneLegends.API.DataModels
{
    /// <summary>
    /// Payload for issuing reset password.
    /// </summary>
    public class IssueResetPasswordPayloadModel
    {
        /// <summary>
        /// Gets or sets Email.
        /// </summary>
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets Redirect url.
        /// </summary>
        [Required]
        public Uri Url { get; set; }
    }
}
