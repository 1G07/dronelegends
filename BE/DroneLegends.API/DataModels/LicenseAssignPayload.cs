﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DroneLegends.API.DataModels
{
    public class LicenseAssignPayload
    {
        [Required]
        [EmailAddress]
        public string AccountEmail { get; set; }

        [Required]
        public Guid LicenseId { get; set; }
    }
}
