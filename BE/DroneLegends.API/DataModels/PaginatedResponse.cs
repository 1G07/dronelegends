﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DroneLegends.API.DataModels
{
    public class PaginatedResponse<T>
    {
        public PaginatedResponse(List<T> data, int total, int page, int pageSize)
        {
            this.Data = data;
            this.Total = total;
            this.Page = page;
            this.PageSize = pageSize;
        }

        public List<T> Data { get; }

        public int Total { get; set; }

        public int Page { get; set; }

        public int PageSize { get; set; }
    }
}
