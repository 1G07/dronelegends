﻿using System.Collections.Generic;
using DroneLegends.DataModels;

namespace DroneLegends.API.DataModels
{
    public class InstitutionResponseModel: InstitutionResponseModelWithoutDetails
    {
        public InstitutionResponseModel()
        {
        }

        public InstitutionResponseModel(Institution entity, List<DroneLegends.DataModels.Module> modules)
            :base(entity, modules)
        {
            this.AvatarImage = entity.Account.AvatarImage;
        }

        /// <summary>
        /// Gets or sets Avatar image.
        /// </summary>
        public string AvatarImage { get; set; }
    }
}
