﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using DroneLegends.API.Authorization;
using DroneLegends.API.DataModels;
using DroneLegends.BL.Services;
using DroneLegends.DataModels;
using System;

namespace DroneLegends.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly AccountService accountService;

        /// <summary>
        /// Initializes a new instance of the <see cref="AccountController"/> class.
        /// </summary>
        /// <param name="accountService">Account service.</param>
        public AccountController(AccountService accountService)
        {
            this.accountService = accountService;
        }

        /// <summary>
        /// Documented at <see cref=""/>.
        /// </summary>
        /// <param name="payload">Payload.</param>
        /// <returns>Login response.</returns>
        [HttpPost]
        [Route("login")]
        public LoginResponseModel LoginAction([FromBody]LoginPayloadModel payload)
        {
            if (payload == null)
            {
                throw new ValidationException("Login payload can not be null.");
            }

            Account account = this.accountService.Login(payload.Email, payload.Password);

            return new LoginResponseModel(account);
        }

        /// <summary>
        /// Documented at <see cref=""/>.
        /// </summary>
        /// <returns>HTTP 200 if successfull.</returns>
        [HttpPost]
        [Route("logout")]
        [Authorization(Roles = "superAdmin,esa,schoolDistrict,school,otherBusiness,teacher")]
        public ActionResult LogoutAction()
        {
            this.accountService.Logout();

            return this.Ok();
        }

        /// <summary>
        /// Refreshes the access token.
        /// </summary>
        /// <param name="payload">Payload.</param>
        /// <returns>Refresh token response.</returns>
        [HttpPost]
        [Route("refreshToken")]
        public LoginResponseModel RefreshAccessTokenAction([FromBody]RefreshTokenPayload payload)
        {
            if (string.IsNullOrEmpty(payload.RefreshToken))
            {
                throw new ValidationException("Refresh token can not be null.");
            }

            Account account = this.accountService.RefreshAccessToken(payload.RefreshToken);

            return new LoginResponseModel(account);
        }

        /// <summary>
        /// Documented at <see cref=""/>.
        /// </summary>
        /// <param name="payload">Payload.</param>
        /// <returns>HTTP 200 if successfull.</returns>
        [HttpPost]
        [Route("resetPassword")]
        public ActionResult IssueResetPasswordAction([FromBody]IssueResetPasswordPayloadModel payload)
        {
            if (payload == null)
            {
                throw new ValidationException("Payload for issuing reset password can not be null.");
            }

            this.accountService.IssueResetPassword(payload.Email, payload.Url);

            return this.Ok();
        }

        /// <summary>
        /// Documented at <see cref=""/>.
        /// </summary>
        /// <param name="payload">Payload.</param>
        /// <returns>HTTP 200 if successfull.</returns>
        [HttpPut]
        [Route("resetPassword")]
        public ActionResult ResetPasswordAction([FromBody]ResetPasswordPayloadModel payload)
        {
            if (payload == null)
            {
                throw new ValidationException("Payload for reset password can not be null.");
            }

            if (payload.NewPassword != payload.NewPasswordConf)
            {
                throw new ValidationException("Password and password confirmation do not match.");
            }

            this.accountService.ResetPassword(payload.ResetCode, payload.NewPassword);

            return this.Ok();
        }

        /// <summary>
        /// Documented at <see cref=""/>.
        /// </summary>
        /// <param name="payload">Payload.</param>
        /// <returns>HTTP 200 if successfull.</returns>
        [HttpPost]
        [Route("changePassword")]
        [Authorization(Roles = "superAdmin,esa,schoolDistrict,school,otherBusiness,teacher")]
        public ActionResult ChangePasswordAction([FromBody]ChangePasswordPayloadModel payload)
        {
            if (payload == null)
            {
                throw new ValidationException("Payload for change password can not be null.");
            }

            if (payload.NewPassword != payload.NewPasswordConf)
            {
                throw new ValidationException("Password and password confirmation do not match.");
            }

            this.accountService.UpdatePassword(payload.OldPassword, payload.NewPassword);

            return this.Ok();
        }

        /// <summary>
        /// Documented at <see cref=""/>.
        /// </summary>
        /// <param name="id">Account id.</param>
        /// <param name="payload">Account for updating.</param>
        /// <returns>Account user.</returns>
        [HttpPut("invite")]
        [Authorization(Roles = "superAdmin,esa,schoolDistrict,school,otherBusiness")]
        public ActionResult SendInvite([FromBody]InvitePayloadModel payload)
        {
            if (payload == null)
            {
                throw new ValidationException("Payload for sending an invitation can not be null.");
            }

            this.accountService.SendInvite(payload.Email, payload.Url);

            return this.Ok();
        }
        
        /// <summary>
        /// Documented at <see cref=""/>.
        /// </summary>
        /// <param name="payload">Unsubscribe payload.</param>
        /// <returns>OK.</returns>
        [HttpPut("unsubscribe")]
        public ActionResult Unsubscribe([FromBody] UnsubscribePayloadModel payload)
        {
            if (payload == null)
            {
                throw new ValidationException("Payload for unsubscribe can not be null.");
            }

            this.accountService.Unsubscribe(payload.Token);

            return this.Ok();
        }

        [HttpPut]
        [Route("{email}/unlock")]
        [Authorization(Roles = "superadmin,esa,schoolDistrict")]
        public ActionResult Unlock(string email)
        {
            this.accountService.Unlock(email);

            return this.Ok();
        }

        [HttpPut]
        [Route("{email}/lock")]
        [Authorization(Roles = "superadmin,esa,schoolDistrict")]
        public ActionResult Lock(string email)
        {
            this.accountService.Lock(email);

            return this.Ok();
        }

        /// <summary>
        /// Documented at <see cref=""/>.
        /// </summary>
        /// <returns>Possible user roles.</returns>
        [HttpGet("roles")]
        [Authorization(Roles = "superAdmin,esa,schoolDistrict,school,otherBusiness,teacher")]
        public List<KeyValuePair<string, string>> GetRoles()
        {
            return new List<KeyValuePair<string, string>>() { 
                new KeyValuePair<string, string>("esa", "ESA"),
                new KeyValuePair<string, string>("schoolDistrict", "School district"),
                new KeyValuePair<string, string>("school", "School"),
                new KeyValuePair<string, string>("otherBusiness", "Other business"),
                new KeyValuePair<string, string>("teacher", "Teacher")
            };
        }
    }
}
