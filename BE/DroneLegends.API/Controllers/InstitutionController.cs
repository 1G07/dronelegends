﻿using System;
using System.Collections.Generic;
using DroneLegends.API.Authorization;
using DroneLegends.API.DataModels;
using DroneLegends.BL.Services;
using DroneLegends.DataModels;
using Microsoft.AspNetCore.Mvc;

namespace DroneLegends.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InstitutionController : ControllerBase
    {
        private readonly InstitutionService insitutionService;
        private readonly ModuleService moduleService;

        /// <summary>
        /// Initializes a new instance of the <see cref="InstitutionService"/> class.
        /// </summary>
        /// <param name="InstitutionService">Insitution service.</param>
        public InstitutionController(InstitutionService insitutionService, ModuleService moduleService)
        {
            this.insitutionService = insitutionService;
            this.moduleService = moduleService;
        }

        /// <summary>
        /// Get the insitutions by filter.
        /// </summary>
        /// <param name="filter">Filter.</param>
        /// <returns>List of insitutions.</returns>
        [HttpGet]
        [Authorization(Roles = "superAdmin,esa,schoolDistrict")]
        public PaginatedResponse<InstitutionResponseModelWithoutDetails> Get([FromQuery]InstitutionGetFilter filter)
        {
            List<InstitutionResponseModelWithoutDetails> result = new List<InstitutionResponseModelWithoutDetails>();

            InstitutionFilter institutionFilter = filter.ToEntity();

            List<Institution> institutions = this.insitutionService.Get(institutionFilter);
            int total = this.insitutionService.GetTotalCount(institutionFilter);

            foreach (Institution ins in institutions)
            {
                result.Add(new InstitutionResponseModelWithoutDetails(ins, this.GetModulesForInstitution(ins)));
            }

            return new PaginatedResponse<InstitutionResponseModelWithoutDetails>(result, total, filter.Offset, filter.Limit);
        }

        /// <summary>
        /// Get self.
        /// </summary>
        /// <returns>Current institution.</returns>
        [HttpGet]
        [Route("self")]
        [Authorization(Roles = "esa,schoolDistrict,school,otherBusiness")]
        public InstitutionResponseModel GetSelf()
        {
            Institution entity = this.insitutionService.GetSelf();

            return new InstitutionResponseModel(entity, this.GetModulesForInstitution(entity));
        }

        /// <summary>
        /// Get the insitution by id.
        /// </summary>
        /// <param name="id">Id.</param>
        /// <returns>Insitution.</returns>
        [HttpGet]
        [Route("{id}")]
        [Authorization(Roles = "superAdmin,esa,schoolDistrict,school,otherBusiness")]
        public InstitutionResponseModel GetById(Guid id)
        {
            Institution entity = this.insitutionService.GetById(id);

            return new InstitutionResponseModel(entity, this.GetModulesForInstitution(entity));
        }

        /// <summary>
        /// Saves the insitution.
        /// </summary>
        /// <param name="payload">Insitution Payload.</param>
        /// <returns>Saved insitution.</returns>
        [HttpPost]
        [Authorization(Roles = "superAdmin,esa,schoolDistrict")]
        public InstitutionSaveResponseModel Post([FromBody]InstitutionPayload payload)
        {
            Institution insitution = this.insitutionService.Save(payload.ToEntity());

            return new InstitutionSaveResponseModel(insitution);
        }

        /// <summary>
        /// Updates the insitution.
        /// </summary>
        /// <param name="payload">insitution Payload.</param>
        /// <returns>Updated insitution.</returns>
        [HttpPut]
        [Route("{id}")]
        [Authorization(Roles = "superAdmin,esa,schoolDistrict,school,otherBusiness")]
        public InstitutionSaveResponseModel Put(Guid id, [FromBody]InstitutionPayload payload)
        {
            Institution insitution = payload.ToEntity();
            insitution.Id = id;

            insitution = this.insitutionService.Update(insitution);

            return new InstitutionSaveResponseModel(insitution);
        }

        /// <summary>
        /// Deletes the insitution.
        /// </summary>
        /// <returns>200 OK.</returns>
        [HttpDelete]
        [Route("{id}")]
        [Authorization(Roles = "superAdmin,esa,schoolDistrict,school,otherBusiness")]
        public ActionResult Delete(Guid id)
        {
            this.insitutionService.Delete(id);

            return this.Ok();
        }

        private List<DroneLegends.DataModels.Module> GetModulesForInstitution(Institution institution)
        {
            List<DroneLegends.DataModels.Module> modules = new List<DroneLegends.DataModels.Module>();
            
            if (institution.Account.LicenseId != null)
            {
                modules = this.moduleService.GetAllModules(institution.Account.LicenseId.Value);
            }

            return modules;
        }
    }
}
