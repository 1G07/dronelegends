﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using DroneLegends.API.Authorization;
using DroneLegends.API.DataModels;
using DroneLegends.BL.Services;

namespace DroneLegends.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TicketController : ControllerBase
    {
        private readonly TicketService ticketService;

        /// <summary>
        /// Initializes a new instance of the <see cref="TicketController"/> class.
        /// </summary>
        /// <param name="ticketService">Ticket service.</param>
        public TicketController(TicketService ticketService)
        {
            this.ticketService = ticketService;
        }

        /// <summary>
        /// Documented at <see cref=""/>.
        /// </summary>
        /// <param name="payload">Payload.</param>
        /// <returns>HTTP 200 if successfull.</returns>
        [HttpGet("types")]
        [Authorization(Roles = "superAdmin,school,otherBusiness,teacher")]
        public List<KeyValuePair<string, string>> GetOptions()
        {
            return new List<KeyValuePair<string, string>>() {
                new KeyValuePair<string, string>("question", "I have a question"),
                new KeyValuePair<string, string>("request ", "I have a request"),
                new KeyValuePair<string, string>("bug", "I want to report a bug")
            };
        }

        /// <summary>
        /// Documented at <see cref=""/>.
        /// </summary>
        /// <param name="payload">Payload.</param>
        /// <returns>HTTP 200 if successfull.</returns>
        [HttpPost]
        [Authorization(Roles = "superAdmin,school,otherBusiness,teacher")]
        public ActionResult SaveTicket([FromBody]TicketPayload payload)
        {
            if (payload == null)
            {
                throw new ValidationException("Payload for saving the ticket can not be null.");
            }

            this.ticketService.SaveTicket(payload.ToEntity());

            return this.Ok();
        }
    }
}
