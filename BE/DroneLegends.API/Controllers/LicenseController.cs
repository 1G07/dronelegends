﻿using System;
using Microsoft.AspNetCore.Mvc;
using DroneLegends.API.Authorization;
using DroneLegends.API.DataModels;
using DroneLegends.BL.DomainModels;
using DroneLegends.BL.Services;
using DroneLegends.DataModels;

namespace DroneLegends.API.Controllers
{
    [Route("api/[controller]")]
    [Authorization(Roles = "superAdmin")]
    [ApiController]
    public class LicenseController : ControllerBase
    {
        private readonly LicenseService licenseService;

        /// <summary>
        /// Initializes a new instance of the <see cref="LicenseController"/> class.
        /// </summary>
        /// <param name="LicenseService">License service.</param>
        public LicenseController(LicenseService licenseService)
        {
            this.licenseService = licenseService;
        }

        /// <summary>
        /// Saves the License.
        /// </summary>
        /// <param name="institutionId">Institution Id.</param>
        /// <param name="payload">License Payload.</param>
        /// <returns>Saved License.</returns>
        [HttpPost]
        [Route("{institutionId}")]
        public LicenseModel Post(Guid institutionId, [FromBody] LicensePayload payload)
        {
            LicenseDO license = this.licenseService.Save(institutionId, payload.ToEntity(), payload.Modules);

            return new LicenseModel(license);
        }

        /// <summary>
        /// Updates the license.
        /// </summary>
        /// <param name="payload">License payload.</param>
        /// <returns>Updated License.</returns>
        [HttpPut]
        [Route("{id}")]
        public LicenseModel Put(Guid id, [FromBody] LicensePayload payload)
        {
            License license = payload.ToEntity();
            license.Id = id;

            LicenseDO savedLicense = this.licenseService.Update(license, payload.Modules);

            return new LicenseModel(savedLicense);
        }

        /// <summary>
        /// Assigns existing license to an account.
        /// </summary>
        /// <param name="payload">License Assign Payload.</param>
        /// <returns>LicenseModel</returns>
        [HttpPatch]
        [Route("assign")]
        public LicenseModel Assign([FromBody] LicenseAssignPayload payload)
        {
            return new LicenseModel(this.licenseService.Assign(payload.AccountEmail, payload.LicenseId));
        }

        /// <summary>
        /// Detaches license from an account.
        /// </summary>
        /// <param name="payload">LicenseDetachPayload.</param>
        /// <returns>OK</returns>
        [HttpPatch]
        [Route("detach")]
        public ActionResult Detach(LicenseDetachPayload payload)
        {
            this.licenseService.Detach(payload.AccountEmail);

            return this.Ok();
        }

        [HttpGet]
        [Route("{id}/expiration")]
        public LicenseExpirationModel GetExpirationDate(Guid id, int numberOfMonths)
        {
            return new LicenseExpirationModel() { ExpiryDate = this.licenseService.CalcualteExpirationDate(id, numberOfMonths) };
        }

        /// <summary>
        /// Deletes the License.
        /// </summary>
        /// <returns>200 OK.</returns>
        [HttpDelete]
        [Route("{id}")]
        public ActionResult Delete(Guid id)
        {
            this.licenseService.Delete(id);

            return this.Ok();
        }
    }
}
