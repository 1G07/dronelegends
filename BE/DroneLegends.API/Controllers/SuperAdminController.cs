﻿using System;
using System.Collections.Generic;
using DroneLegends.API.Authorization;
using DroneLegends.API.DataModels;
using DroneLegends.BL.Services;
using DroneLegends.DataModels;
using Microsoft.AspNetCore.Mvc;

namespace DroneLegends.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorization(Roles = "superAdmin")]
    public class SuperAdminController : ControllerBase
    {
        private readonly SuperAdminService superAdminService;

        /// <summary>
        /// Initializes a new instance of the <see cref="SuperAdminController"/> class.
        /// </summary>
        /// <param name="superAdminService">Super Admin service.</param>
        public SuperAdminController(SuperAdminService superAdminService)
        {
            this.superAdminService = superAdminService;
        }

        /// <summary>
        /// Get the super admins by filter.
        /// </summary>
        /// <param name="filter">Filter.</param>
        /// <returns>List of super admins.</returns>
        [HttpGet]
        public PaginatedResponse<SuperAdminResponseModelForList> Get([FromQuery]SuperAdminFilterQuery filter)
        {
            List<SuperAdminResponseModelForList> result = new List<SuperAdminResponseModelForList>();
            SuperAdminFilter filterEntity = filter.ToEntity();

            List<SuperAdmin> superAdmins = this.superAdminService.Get(filterEntity);
            int total = this.superAdminService.GetTotalCount(filterEntity);

            foreach (SuperAdmin sa in superAdmins)
            {
                result.Add(new SuperAdminResponseModelForList(sa));
            }

            return new PaginatedResponse<SuperAdminResponseModelForList>(result, total, filter.Offset, filter.Limit);
        }

        /// <summary>
        /// Get the super admin by id.
        /// </summary>
        /// <param name="id">Id.</param>
        /// <returns>Super admin.</returns>
        [HttpGet]
        [Route("{id}")]
        public SuperAdminResponseModel GetById(Guid id)
        {
            SuperAdmin entity = this.superAdminService.GetById(id);

            return new SuperAdminResponseModel(entity);
        }

        /// <summary>
        /// Get self.
        /// </summary>
        /// <returns>Current super admin.</returns>
        [HttpGet]
        [Route("self")]
        public SuperAdminResponseModel GetSelf()
        {
            SuperAdmin entity = this.superAdminService.GetSelf();

            return new SuperAdminResponseModel(entity);
        }

        /// <summary>
        /// Saves the super admin.
        /// </summary>
        /// <param name="payload">Super admin Payload.</param>
        /// <returns>Saved super admin.</returns>
        [HttpPost]
        public SuperAdminSaveResponseModel Post([FromBody]SuperAdminPayload payload)
        {
            SuperAdmin sa = this.superAdminService.Save(payload.ToEntity());

            return new SuperAdminSaveResponseModel(sa);
        }

        /// <summary>
        /// Updates the super admin.
        /// </summary>
        /// <param name="id">Super admin Payload.</param>
        /// <param name="payload">Super admin Payload.</param>
        /// <returns>Updated Super admin.</returns>
        [HttpPut]
        [Route("{id}")]
        public SuperAdminSaveResponseModel Put(Guid id, [FromBody]SuperAdminPayload payload)
        {
            SuperAdmin sa = payload.ToEntity();
            sa.Id = id;

            sa = this.superAdminService.Update(sa);

            return new SuperAdminSaveResponseModel(sa);
        }

        /// <summary>
        /// Deletes the super admin.
        /// </summary>
        /// <returns>200 OK.</returns>
        [HttpDelete]
        [Route("{id}")]
        public ActionResult Delete(Guid id)
        {
            this.superAdminService.Delete(id);

            return this.Ok();
        }
    }
}
