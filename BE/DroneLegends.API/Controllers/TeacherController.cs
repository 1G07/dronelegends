﻿using System;
using System.Collections.Generic;
using DroneLegends.API.Authorization;
using DroneLegends.API.DataModels;
using DroneLegends.BL.Services;
using DroneLegends.DataModels;
using Microsoft.AspNetCore.Mvc;

namespace DroneLegends.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeacherController : ControllerBase
    {
        private readonly TeacherService teacherService;
        private readonly ModuleService moduleService;

        /// <summary>
        /// Initializes a new instance of the <see cref="TeacherService"/> class.
        /// </summary>
        /// <param name="teacherService">Teacher service.</param>
        public TeacherController(TeacherService teacherService, ModuleService moduleService)
        {
            this.teacherService = teacherService;
            this.moduleService = moduleService;
        }

        /// <summary>
        /// Get the teachers by filter.
        /// </summary>
        /// <param name="filter">Filter.</param>
        /// <returns>List of teachers.</returns>
        [HttpGet]
        [Authorization(Roles = "superAdmin,school,otherBusiness")]
        public PaginatedResponse<TeacherResponseModelForList> Get([FromQuery]TeacherFilterQuery filter)
        {
            List<TeacherResponseModelForList> result = new List<TeacherResponseModelForList>();
            TeacherFilter filterEntity = filter.ToEntity();

            List<Teacher> teachers = this.teacherService.Get(filterEntity);
            int total = this.teacherService.GetTotalCount(filterEntity);

            foreach (Teacher t in teachers)
            {
                result.Add(new TeacherResponseModelForList(t, this.moduleService.GetAllModules(t.Account.LicenseId.Value)));
            }

            return new PaginatedResponse<TeacherResponseModelForList>(result, total, filter.Offset, filter.Limit);
        }

        /// <summary>
        /// Get the teacher by id.
        /// </summary>
        /// <param name="id">Id.</param>
        /// <returns>Teacher.</returns>
        [HttpGet]
        [Route("{id}")]
        [Authorization(Roles = "superAdmin,school,otherBusiness,teacher")]
        public TeacherResponseModel GetById(Guid id)
        {
            Teacher entity = this.teacherService.GetById(id);

            return new TeacherResponseModel(entity, this.moduleService.GetAllModules(entity.Account.LicenseId.Value));
        }

        /// <summary>
        /// Get self.
        /// </summary>
        /// <returns>Current teacher.</returns>
        [HttpGet]
        [Route("self")]
        [Authorization(Roles = "teacher")]
        public TeacherResponseModel GetSelf()
        {
            Teacher entity = this.teacherService.GetSelf();

            return new TeacherResponseModel(entity, this.moduleService.GetAllModules(entity.Account.LicenseId.Value));
        }

        /// <summary>
        /// Saves the teacher.
        /// </summary>
        /// <param name="payload">Teacher Payload.</param>
        /// <returns>Saved teacher.</returns>
        [HttpPost]
        [Authorization(Roles = "superAdmin,school,otherBusiness")]
        public TeacherSaveResponseModel Post([FromBody]TeacherPayload payload)
        {
            Teacher teacher = this.teacherService.Save(payload.ToEntity());

            return new TeacherSaveResponseModel(teacher);
        }

        /// <summary>
        /// Updates the teacher.
        /// </summary>
        /// <param name="id">Teacher Payload.</param>
        /// <param name="payload">Teacher Payload.</param>
        /// <returns>Updated Teacher.</returns>
        [HttpPut]
        [Route("{id}")]
        [Authorization(Roles = "superAdmin,school,otherBusiness,teacher")]
        public TeacherSaveResponseModel Put(Guid id, [FromBody]TeacherPayload payload)
        {
            Teacher teacher = payload.ToEntity();
            teacher.Id = id;

            teacher = this.teacherService.Update(teacher);

            return new TeacherSaveResponseModel(teacher);
        }

        /// <summary>
        /// Deletes the teacher.
        /// </summary>
        /// <returns>200 OK.</returns>
        [HttpDelete]
        [Route("{id}")]
        [Authorization(Roles = "superAdmin,school,otherBusiness")]
        public ActionResult Delete(Guid id)
        {
            this.teacherService.Delete(id);

            return this.Ok();
        }
    }
}
