﻿using System;
using System.Collections.Generic;
using DroneLegends.API.Authorization;
using DroneLegends.BL.Services;
using ModuleEntity = DroneLegends.DataModels.Module;
using Microsoft.AspNetCore.Mvc;
using DroneLegends.API.DataModels;
using DroneLegends.DataModels;
using DroneLegends.BL.DomainModels;

namespace DroneLegends.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ModuleController : ControllerBase
    {
        private readonly ModuleService moduleService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ModuleService"/> class.
        /// </summary>
        /// <param name="teacherService">Module service.</param>
        public ModuleController(ModuleService moduleService)
        {
            this.moduleService = moduleService;
        }

        /// <summary>
        /// Get list of all modules.
        /// </summary>
        /// <returns>List of modules.</returns>
        [HttpGet]
        [Authorization(Roles = "superAdmin,school,otherBusiness,teacher")]
        public List<ModuleDetails> Get()
        {
            List<ModuleDetails> result = new List<ModuleDetails>();

            foreach (ModuleEntity entity in this.moduleService.GetAllForUser())
            {
                result.Add(new ModuleDetails(entity));
            }

            return result;
        }

        /// <summary>
        /// Get list of all modules.
        /// </summary>
        /// <returns>List of modules.</returns>
        [HttpGet]
        [Route("{moduleId}")]
        [Authorization(Roles = "superAdmin,school,otherBusiness,teacher")]
        public List<LessonOverview> Get(Guid moduleId)
        {
            List<LessonOverview> result = new List<LessonOverview>();

            foreach (Lesson entity in this.moduleService.GetLessons(moduleId))
            {
                result.Add(new LessonOverview(entity));
            }

            return result;
        }

        /// <summary>
        /// Get list of all modules.
        /// </summary>
        /// <returns>List of modules.</returns>
        [HttpGet]
        [Route("lesson/{lessonId}")]
        [Authorization(Roles = "superAdmin,school,otherBusiness,teacher")]
        public LessonDetails GetLessonDetails(Guid lessonId)
        {
            LessonDO lesson = this.moduleService.GetLesson(lessonId);
            
            return new LessonDetails(lesson);
        }
        
        /// <summary>
        /// Get presentation url.
        /// </summary>
        /// <returns>Presentation url.</returns>
        [HttpGet]
        [Route("lesson/{lessonId}/presentation/url")]
        [Authorization(Roles = "superAdmin,school,otherBusiness,teacher")]
        public PresentationUrlResponseModel GetLessonPresentationUrl(Guid lessonId)
        {
            string presentationUrl = this.moduleService.GetPresentationUrl(lessonId);

            return new PresentationUrlResponseModel() { Url = presentationUrl };
        }
        
        /// <summary>
        /// Downloads the resource.
        /// </summary>
        /// <returns>Resource for download.</returns>
        [HttpGet]
        [Route("resource/download/{resourceId}")]
        [Authorization(Roles = "superAdmin,school,otherBusiness,teacher")]
        public FileResult GetResource(Guid resourceId)
        {
            return this.moduleService.DownloadResource(resourceId);
        }

        /// <summary>
        /// Get list of all module options.
        /// </summary>
        /// <returns>List of modules.</returns>
        [HttpGet]
        [Route("options")]
        [Authorization(Roles = "superAdmin")]
        public List<KeyValuePair<Guid, string>> GetOptions()
        {
            List<KeyValuePair<Guid, string>> result = new List<KeyValuePair<Guid, string>>();

            foreach (ModuleEntity entity in this.moduleService.GetAll())
            {
                result.Add(new KeyValuePair<Guid, string>(entity.Id.Value, entity.Name));
            }

            return result;
        }
    }
}
