﻿using FluentMigrator.Runner;
using Microsoft.AspNetCore.Mvc;
using DroneLegends.API.Filters;
using DroneLegends.BL.Services;
using DroneLegends.API.DataModels;

namespace DroneLegends.API.Controllers
{
    [BasicAuthentication]
    public class DataMigrationController : Controller
    {
        private readonly IMigrationRunner migrationRunner;
        private readonly AccountService accountService;

        public DataMigrationController(IMigrationRunner migrationRunner, AccountService accountService)
        {
            this.migrationRunner = migrationRunner;
            this.accountService = accountService;
        }

        public JsonResult Migrate()
        {
            this.migrationRunner.MigrateUp();

            return this.Json(new { status = "OK" });
        }
        
        public JsonResult ChangePasswordForAdmin([FromBody]ChangePasswordPayloadModel payload)
        {
            if (payload.NewPassword != payload.NewPasswordConf)
            {
                throw new BL.Exceptions.BusinessException("Passwords don't match!");
            }
            this.accountService.UpdatePasswordForAdmin(payload.NewPassword);

            return this.Json(new { status = "OK" });
        }
    }
}

