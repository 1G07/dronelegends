﻿using System.Collections.Generic;
using DroneLegends.DataAccess.Common.Enum;

namespace DroneLegends.DataAccess.Common.Interfaces
{
    public interface IUnitOfWork
    {
        void SaveChanges(bool setTimestamps = true);

        IReadOnlyDictionary<EntityBase, AffectedEntityState> GetAffectedEntities();
    }
}
