﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DroneLegends.DataAccess.Common.Interfaces
{
    public interface IRepository
    {
        /// <summary>
        /// Indicates that entity should be saved as a new one during the next save operation
        /// </summary>
        /// <param name="entity">The entity.</param>
        void AddEntity(EntityBase entity);

        /// <summary>
        /// Indicates that entity should be deleted during the next save operation
        /// </summary>
        /// <param name="entity">The entity.</param>
        void DeleteEntity(EntityBase entity);

        /// <summary>
        /// Indicates that entity should be saved as updated during the next save operation
        /// </summary>
        /// <param name="entity">The entity.</param>
        void UpdateEntity(EntityBase entity);
    }
}
