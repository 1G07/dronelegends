﻿using System;
using System.Collections.Generic;

namespace DroneLegends.DataAccess.Common.Interfaces.Generic
{
    public interface IRepository<TEntity> : IRepository
        where TEntity : EntityBase
    {
        IEnumerable<TEntity> GetAll();

        TEntity GetById(Guid id);

        void AddEntity(TEntity entity);

        void DeleteEntity(TEntity entity);

        void UpdateEntity(TEntity entity);
    }
}
