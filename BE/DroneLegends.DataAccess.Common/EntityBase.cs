﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DroneLegends.DataAccess.Common
{

    /// <summary>
    /// Base entity class
    /// </summary>
    public class EntityBase
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [Key]
        public Guid? Id { get; set; }

        /// <summary>
        /// Gets or sets the creation date.
        /// </summary>
        [Required]
        public DateTimeOffset? CreationDate { get; set; }

        /// <summary>
        /// Gets or sets the modification date.
        /// </summary>
        [Required]
        public DateTimeOffset? ModificationDate { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is deleted.
        /// </summary>
        [Required]
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is new.
        /// </summary>
        [NotMapped]
        public bool IsNew { get; set; }
    }
}
