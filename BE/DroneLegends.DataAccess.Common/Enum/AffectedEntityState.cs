﻿namespace DroneLegends.DataAccess.Common.Enum
{
    public enum AffectedEntityState
    {
        Added = 0,
        Updated = 1,
        Deleted = 2
    }
}
