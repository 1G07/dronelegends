﻿namespace DroneLegends.DataMigrations
{
    public static class MigratorExtensions
    {
        public static FluentMigrator.Builders.Create.Table.ICreateTableWithColumnSyntax TableWithCommonColumns(this FluentMigrator.Builders.Create.ICreateExpressionRoot create, string tableName)
        {
            return create.Table(tableName)
               .WithColumn("Id").AsGuid().PrimaryKey()
               .WithColumn("CreationDate").AsDateTimeOffset().NotNullable()
               .WithColumn("ModificationDate").AsDateTimeOffset().NotNullable()
               .WithColumn("IsDeleted").AsBoolean().NotNullable();
        }
    }
}
