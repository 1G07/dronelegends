﻿using System;
using FluentMigrator;

namespace DroneLegends.DataMigrations.Version_0001
{
    /// <summary>
    ///  dotnet-fm migrate -p SqlServer -c "Server=.\SQLEXPRESS2017;Database=DroneLegends;Trusted_Connection=True;MultipleActiveResultSets=true" -a .\DroneLegends.DataMigrations\bin\Debug\netcoreapp3.0\DroneLegends.DataMigrations.dll
    /// </summary>
    [Migration(20, "Add indicator if account is locked.")]
    public class Migration_0020_Schema : Migration
    {
        public override void Up()
        {
            Alter.Table("Accounts").AddColumn("IsLockedDueToLicenseChange").AsBoolean().NotNullable().WithDefaultValue(false);
            Alter.Table("Accounts").AddColumn("FirstLoginWithChangedLicense").AsDateTime().Nullable();
            Alter.Table("Accounts").AddColumn("IsLockedEmailSent").AsBoolean().NotNullable().WithDefaultValue(false);
        }

        public override void Down()
        {
            throw new NotImplementedException();
        }
    }
}
