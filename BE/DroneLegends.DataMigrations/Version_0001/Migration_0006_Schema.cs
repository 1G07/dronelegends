﻿using System;
using FluentMigrator;

namespace DroneLegends.DataMigrations.Version_0001
{
    /// <summary>
    ///  dotnet-fm migrate -p SqlServer -c "Server=.\SQLEXPRESS2017;Database=DroneLegends;Trusted_Connection=True;MultipleActiveResultSets=true" -a .\DroneLegends.DataMigrations\bin\Debug\netcoreapp3.0\DroneLegends.DataMigrations.dll
    /// </summary>
    [Migration(6, "Alter lessons database structure")]
    public class Migration_0006_Schema : Migration
    {
        public override void Up()
        {
            Update.Table("Lessons").Set(new
            {
                Image = string.Empty
            }).AllRows();

            Alter.Table("Lessons")
                .AlterColumn("Description").AsString(4000)
                .AlterColumn("Image").AsString(1000)
                .AddColumn("PresentationCoverImage").AsString(999999999).Nullable();
        }

        public override void Down()
        {
            throw new NotImplementedException();
        }
    }
}
