﻿using System;
using FluentMigrator;

namespace DroneLegends.DataMigrations.Version_0001
{
    /// <summary>
    ///  dotnet-fm migrate -p SqlServer -c "Server=.\SQLEXPRESS2017;Database=DroneLegends;Trusted_Connection=True;MultipleActiveResultSets=true" -a .\DroneLegends.DataMigrations\bin\Debug\netcoreapp3.0\DroneLegends.DataMigrations.dll
    /// </summary>
    [Migration(14, "Add new column RegistrationStatus to accounts table.")]
    public class Migration_0014_Schema : Migration
    {
        public override void Up()
        {
            Alter.Table("Accounts")
              .AddColumn("RegistrationStatus").AsInt32().NotNullable().WithDefaultValue(1)
              .AddColumn("ShouldSendEmailCopy").AsBoolean().Nullable();
        }

        public override void Down()
        {
            throw new NotImplementedException();
        }
    }
}
