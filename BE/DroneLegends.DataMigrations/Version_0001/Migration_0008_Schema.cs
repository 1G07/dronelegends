﻿using System;
using FluentMigrator;

namespace DroneLegends.DataMigrations.Version_0001
{
    /// <summary>
    ///  dotnet-fm migrate -p SqlServer -c "Server=.\SQLEXPRESS2017;Database=DroneLegends;Trusted_Connection=True;MultipleActiveResultSets=true" -a .\DroneLegends.DataMigrations\bin\Debug\netcoreapp3.0\DroneLegends.DataMigrations.dll
    /// </summary>
    [Migration(8, "Add tickets database structure")]
    public class Migration_0008_Schema : Migration
    {
        public override void Up()
        {
            Create.TableWithCommonColumns("Tickets")
             .WithColumn("Type").AsInt32().NotNullable()
             .WithColumn("Text").AsString(10000).NotNullable()
             .WithColumn("AccountId").AsGuid().ForeignKey("Accounts", "Id").OnDelete(System.Data.Rule.Cascade).OnUpdate(System.Data.Rule.None).NotNullable();
        }

        public override void Down()
        {
            throw new NotImplementedException();
        }
    }
}
