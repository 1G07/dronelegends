﻿using System;
using FluentMigrator;

namespace DroneLegends.DataMigrations.Version_0001
{
    /// <summary>
    ///  dotnet-fm migrate -p SqlServer -c "Server=.\SQLEXPRESS2017;Database=DroneLegends;Trusted_Connection=True;MultipleActiveResultSets=true" -a .\DroneLegends.DataMigrations\bin\Debug\netcoreapp3.0\DroneLegends.DataMigrations.dll
    /// </summary>
    [Migration(10, "Change school role value")]
    public class Migration_0010_Data : Migration
    {
        public override void Up()
        {
            Update.Table("Accounts").Set(new { Role = 6 }).Where(new { Role = 5 });
        }

        public override void Down()
        {
            throw new NotImplementedException();
        }
    }
}
