﻿using System;
using FluentMigrator;

namespace DroneLegends.DataMigrations.Version_0001
{
    /// <summary>
    ///  dotnet-fm migrate -p SqlServer -c "Server=.\SQLEXPRESS2017;Database=DroneLegends;Trusted_Connection=True;MultipleActiveResultSets=true" -a .\DroneLegends.DataMigrations\bin\Debug\netcoreapp3.0\DroneLegends.DataMigrations.dll
    /// </summary>
    [Migration(3, "Alter modules database structure")]
    public class Migration_0003_Schema : Migration
    {
        public override void Up()
        {
            Alter.Table("Modules")
                .AddColumn("FolderName").AsString().NotNullable()
                .AddColumn("Description").AsString().NotNullable()
                .AddColumn("Image").AsString(999999999).Nullable()
                .AddColumn("Position").AsInt32().NotNullable();

            Alter.Table("Accounts")
                .AlterColumn("AvatarImage").AsString(999999999).Nullable();

            Alter.Table("Lessons")
                .AddColumn("Description").AsString().NotNullable()
                .AddColumn("FolderName").AsString().NotNullable()
                .AddColumn("Image").AsString(999999999).Nullable()
                .AddColumn("Position").AsInt32().NotNullable()
                .AddColumn("Introduction").AsString().Nullable();

            Create.TableWithCommonColumns("DownloadableResources")
               .WithColumn("Name").AsString().NotNullable()
               .WithColumn("FileName").AsString().NotNullable()
               .WithColumn("Position").AsInt32().NotNullable()
               .WithColumn("LessonId").AsGuid().ForeignKey("Lessons", "Id").OnDelete(System.Data.Rule.None).OnUpdate(System.Data.Rule.None).Nullable()
               .WithColumn("ModuleId").AsGuid().ForeignKey("Modules", "Id").OnDelete(System.Data.Rule.None).OnUpdate(System.Data.Rule.None).Nullable();

            Create.TableWithCommonColumns("Subscriptions")
              .WithColumn("LicenseId").AsGuid().ForeignKey("Licenses", "Id").OnDelete(System.Data.Rule.Cascade).OnUpdate(System.Data.Rule.None).Nullable()
              .WithColumn("ModuleId").AsGuid().ForeignKey("Modules", "Id").OnDelete(System.Data.Rule.Cascade).OnUpdate(System.Data.Rule.None).Nullable();
        }

        public override void Down()
        {
            throw new NotImplementedException();
        }
    }
}
