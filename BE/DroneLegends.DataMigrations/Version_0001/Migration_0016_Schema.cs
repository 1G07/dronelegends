﻿using System;
using FluentMigrator;

namespace DroneLegends.DataMigrations.Version_0001
{
    /// <summary>
    ///  dotnet-fm migrate -p SqlServer -c "Server=.\SQLEXPRESS2017;Database=DroneLegends;Trusted_Connection=True;MultipleActiveResultSets=true" -a .\DroneLegends.DataMigrations\bin\Debug\netcoreapp3.0\DroneLegends.DataMigrations.dll
    /// </summary>
    [Migration(16, "Add new columns IsSubscribed, UnsubscribeToken to accounts table.")]
    public class Migration_0016_Schema : Migration
    {
        public override void Up()
        {
            Alter.Table("Accounts")
              .AddColumn("IsSubscribedToMails").AsBoolean().NotNullable().WithDefaultValue(true)
              .AddColumn("UnsubscribeToken").AsString().Nullable();
        }

        public override void Down()
        {
            throw new NotImplementedException();
        }
    }
}
