﻿using System;
using FluentMigrator;

namespace DroneLegends.DataMigrations.Version_0001
{
    /// <summary>
    ///  dotnet-fm migrate -p SqlServer -c "Server=.\SQLEXPRESS2017;Database=DroneLegends;Trusted_Connection=True;MultipleActiveResultSets=true" -a .\DroneLegends.DataMigrations\bin\Debug\netcoreapp3.0\DroneLegends.DataMigrations.dll
    /// </summary>
    [Migration(17, "Add license expiration in months, update account first login.")]
    public class Migration_0017_Schema : Migration
    {
        public override void Up()
        {
            Alter.Table("Accounts").AddColumn("FirstLogin").AsDateTime().Nullable();
            Alter.Table("Licenses").AddColumn("DurationInMonths").AsInt32().NotNullable().WithDefaultValue(0);
            Alter.Table("Licenses").AddColumn("StartDate").AsDateTime().Nullable();
        }

        public override void Down()
        {
            throw new NotImplementedException();
        }
    }
}
