﻿using System;
using FluentMigrator;

namespace DroneLegends.DataMigrations.Version_0001
{
    /// <summary>
    ///  dotnet-fm migrate -p SqlServer -c "Server=.\SQLEXPRESS2017;Database=DroneLegends;Trusted_Connection=True;MultipleActiveResultSets=true" -a .\DroneLegends.DataMigrations\bin\Debug\netcoreapp3.0\DroneLegends.DataMigrations.dll
    /// </summary>
    [Migration(5, "Alter lessons database structure")]
    public class Migration_0005_Schema : Migration
    {
        public override void Up()
        {
            Alter.Table("Lessons")
                .AddColumn("PresentationHash").AsString().Nullable()
                .AddColumn("PresentationHashExpiry").AsDateTime().Nullable();
        }

        public override void Down()
        {
            throw new NotImplementedException();
        }
    }
}
