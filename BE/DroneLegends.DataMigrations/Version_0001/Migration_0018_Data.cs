﻿using System;
using FluentMigrator;

namespace DroneLegends.DataMigrations.Version_0001
{
    /// <summary>
    ///  dotnet-fm migrate -p SqlServer -c "Server=.\SQLEXPRESS2017;Database=DroneLegends;Trusted_Connection=True;MultipleActiveResultSets=true" -a .\DroneLegends.DataMigrations\bin\Debug\netcoreapp3.0\DroneLegends.DataMigrations.dll
    /// </summary>
    [Migration(18, "Update duration in months for existing licenses")]
    public class Migration_0018_Data : Migration
    {
        public override void Up()
        {
            Execute.Sql(RawSql.Insert("UPDATE a SET a.FirstLogin = (SELECT ac.CreationDate from [Accounts] ac WHERE ac.Id = a.Id AND ac.LastLogin is not null) FROM [Accounts] a").Value);
            Execute.Sql(RawSql.Insert("UPDATE l SET l.StartDate = (SELECT min(a.FirstLogin) from [Accounts] a WHERE a.LicenseId = l.Id) FROM [Licenses] l").Value);
            Execute.Sql(RawSql.Insert("UPDATE l SET l.DurationInMonths = DATEDIFF(MONTH, l.StartDate, l.ExpiryDate) FROM [Licenses] l where l.StartDate is not null").Value);
        }

        public override void Down()
        {
            throw new NotImplementedException();
        }
    }
}
