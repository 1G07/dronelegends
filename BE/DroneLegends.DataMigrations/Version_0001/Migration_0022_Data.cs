﻿using System;
using FluentMigrator;

namespace DroneLegends.DataMigrations.Version_0001
{
    /// <summary>
    ///  dotnet-fm migrate -p SqlServer -c "Server=.\SQLEXPRESS2017;Database=DroneLegends;Trusted_Connection=True;MultipleActiveResultSets=true" -a .\DroneLegends.DataMigrations\bin\Debug\netcoreapp3.0\DroneLegends.DataMigrations.dll
    /// </summary>
    [Migration(22, "Add embeded resources title")]
    public class Migration_022_Data : Migration 
    {
        public override void Up()
        {
            Execute.Sql(RawSql.Insert("UPDATE [EmbededResources] SET [Title] = 'Hurricanes Mission 1:  Aerial Search & Rescue!' WHERE [LessonId] = (SELECT [Id] FROM [Lessons] WHERE [Position] = 1)").Value);
            Execute.Sql(RawSql.Insert("UPDATE [EmbededResources] SET [URL] = 'https://www.youtube.com/embed/QGNbDUTDY80', [Title] = 'Firefighting Mission 2:  Controlled Burns with Drones and Dragon Eggs.' WHERE [LessonId] = (SELECT [Id] FROM [Lessons] WHERE [Position] = 2)").Value);
            Execute.Sql(RawSql.Insert("UPDATE [EmbededResources] SET [Title] = 'Antarctica Research Mission 3:  Arctic Exploration with Drones!' WHERE [LessonId] = (SELECT [Id] FROM [Lessons] WHERE [Position] = 3)").Value);
            Execute.Sql(RawSql.Insert("UPDATE [EmbededResources] SET [Title] = 'Sharks Mission 4: Sea Life From the Sky!' WHERE [LessonId] = (SELECT [Id] FROM [Lessons] WHERE [Position] = 4)").Value);
            Execute.Sql(RawSql.Insert("UPDATE [EmbededResources] SET [Title] = 'Medicine Delivery Mission 5: Drones Deliver Medical Supplies To Rural Areas In Rwanda And Ghana.' WHERE [LessonId] = (SELECT [Id] FROM [Lessons] WHERE [Position] = 5)").Value);
            Execute.Sql(RawSql.Insert("UPDATE [EmbededResources] SET [Title] = 'Helper Bees Mission 6: Pollinating with Drones.' WHERE [LessonId] = (SELECT [Id] FROM [Lessons] WHERE [Position] = 6)").Value);
            Execute.Sql(RawSql.Insert("UPDATE [EmbededResources] SET [Title] = 'Course Mapping Racing Mission 7: Million Dollar Drone Race!' WHERE [LessonId] = (SELECT [Id] FROM [Lessons] WHERE [Position] = 7)").Value);
            Execute.Sql(RawSql.Insert("UPDATE [EmbededResources] SET [Title] = 'Life on Mars Mission 8: NASA’s “Mars Helicopter”, Ingenuity!' WHERE [LessonId] = (SELECT [Id] FROM [Lessons] WHERE [Position] = 8)").Value);
            Execute.Sql(RawSql.Insert("UPDATE [EmbededResources] SET [Title] = 'Koala Care Mission 9: Drones count Koalas.' WHERE [LessonId] = (SELECT [Id] FROM [Lessons] WHERE [Position] = 9)").Value);
            Execute.Sql(RawSql.Insert("UPDATE [EmbededResources] SET [Title] = 'Cinematography Mission 10:  Aerial Cinematography in Action!' WHERE [LessonId] = (SELECT [Id] FROM [Lessons] WHERE [Position] = 10)").Value);
        }

        public override void Down()
        {
            throw new NotImplementedException();
        }
    }
}
