﻿using System;
using FluentMigrator;

namespace DroneLegends.DataMigrations.Version_0001
{
    /// <summary>
    ///  dotnet-fm migrate -p SqlServer -c "Server=.\SQLEXPRESS2017;Database=DroneLegends;Trusted_Connection=True;MultipleActiveResultSets=true" -a .\DroneLegends.DataMigrations\bin\Debug\netcoreapp3.0\DroneLegends.DataMigrations.dll
    /// </summary>
    [Migration(13, "Add embeded resources url")]
    public class Migration_0013_Data : Migration 
    {
        public override void Up()
        {
            Execute.Sql(RawSql.Insert("INSERT INTO [EmbededResources]([Id],[CreationDate],[ModificationDate],[IsDeleted],[Url],[Position],[LessonId]) VALUES (NEWID(),'2021-10-12T18:22:55+02:00','2021-10-12T18:22:55+02:00',0,'https://youtube.com/embed/XHqUOYFAc6c',1,(SELECT [Id] FROM [Lessons] WHERE [Position] = 1))").Value);
            Execute.Sql(RawSql.Insert("INSERT INTO [EmbededResources]([Id],[CreationDate],[ModificationDate],[IsDeleted],[Url],[Position],[LessonId]) VALUES (NEWID(),'2021-10-12T18:22:55+02:00','2021-10-12T18:22:55+02:00',0,'https://www.youtube.com/embed/QGNbDUTDY80',1,(SELECT [Id] FROM [Lessons] WHERE [Position] = 2))").Value);
            Execute.Sql(RawSql.Insert("INSERT INTO [EmbededResources]([Id],[CreationDate],[ModificationDate],[IsDeleted],[Url],[Position],[LessonId]) VALUES (NEWID(),'2021-10-12T18:22:55+02:00','2021-10-12T18:22:55+02:00',0,'https://youtube.com/embed/zlXoq0E6lSA',1,(SELECT [Id] FROM [Lessons] WHERE [Position] = 3))").Value);
            Execute.Sql(RawSql.Insert("INSERT INTO [EmbededResources]([Id],[CreationDate],[ModificationDate],[IsDeleted],[Url],[Position],[LessonId]) VALUES (NEWID(),'2021-10-12T18:22:55+02:00','2021-10-12T18:22:55+02:00',0,'https://youtube.com/embed/ZLJkzXXg1oY',1,(SELECT [Id] FROM [Lessons] WHERE [Position] = 4))").Value);
            Execute.Sql(RawSql.Insert("INSERT INTO [EmbededResources]([Id],[CreationDate],[ModificationDate],[IsDeleted],[Url],[Position],[LessonId]) VALUES (NEWID(),'2021-10-12T18:22:55+02:00','2021-10-12T18:22:55+02:00',0,'https://www.facebook.com/plugins/video.php?height=314&href=https%3A%2F%2Fwww.facebook.com%2FBusinessInsiderToday%2Fvideos%2F702825110544900%2F&show_text=false&width=560&t=0',1,(SELECT [Id] FROM [Lessons] WHERE [Position] = 5))").Value);
            Execute.Sql(RawSql.Insert("INSERT INTO [EmbededResources]([Id],[CreationDate],[ModificationDate],[IsDeleted],[Url],[Position],[LessonId]) VALUES (NEWID(),'2021-10-12T18:22:55+02:00','2021-10-12T18:22:55+02:00',0,'https://youtube.com/embed/cBywK27IZj0',1,(SELECT [Id] FROM [Lessons] WHERE [Position] = 6))").Value);
            Execute.Sql(RawSql.Insert("INSERT INTO [EmbededResources]([Id],[CreationDate],[ModificationDate],[IsDeleted],[Url],[Position],[LessonId]) VALUES (NEWID(),'2021-10-12T18:22:55+02:00','2021-10-12T18:22:55+02:00',0,'https://youtube.com/embed/pZ0viMxYDA4',1,(SELECT [Id] FROM [Lessons] WHERE [Position] = 7))").Value);
            Execute.Sql(RawSql.Insert("INSERT INTO [EmbededResources]([Id],[CreationDate],[ModificationDate],[IsDeleted],[Url],[Position],[LessonId]) VALUES (NEWID(),'2021-10-12T18:22:55+02:00','2021-10-12T18:22:55+02:00',0,'https://youtube.com/embed/0RQWv1ybsjM',1,(SELECT [Id] FROM [Lessons] WHERE [Position] = 8))").Value);
            Execute.Sql(RawSql.Insert("INSERT INTO [EmbededResources]([Id],[CreationDate],[ModificationDate],[IsDeleted],[Url],[Position],[LessonId]) VALUES (NEWID(),'2021-10-12T18:22:55+02:00','2021-10-12T18:22:55+02:00',0,'https://youtube.com/embed/C0peca9YGro',1,(SELECT [Id] FROM [Lessons] WHERE [Position] = 9))").Value);
            Execute.Sql(RawSql.Insert("INSERT INTO [EmbededResources]([Id],[CreationDate],[ModificationDate],[IsDeleted],[Url],[Position],[LessonId]) VALUES (NEWID(),'2021-10-12T18:22:55+02:00','2021-10-12T18:22:55+02:00',0,'https://youtube.com/embed/bLZRG4d3iUk',1,(SELECT [Id] FROM [Lessons] WHERE [Position] = 10))").Value);
        }

        public override void Down()
        {
            throw new NotImplementedException();
        }
    }
}
