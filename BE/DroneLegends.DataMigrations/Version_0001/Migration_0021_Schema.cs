﻿using System;
using FluentMigrator;

namespace DroneLegends.DataMigrations.Version_0001
{
    /// <summary>
    ///  dotnet-fm migrate -p SqlServer -c "Server=.\SQLEXPRESS2017;Database=DroneLegends;Trusted_Connection=True;MultipleActiveResultSets=true" -a .\DroneLegends.DataMigrations\bin\Debug\netcoreapp3.0\DroneLegends.DataMigrations.dll
    /// </summary>
    [Migration(21, "Alter embeded resources schema")]
    public class Migration_0021_Schema : Migration
    {
        public override void Up()
        {
            if (!Schema.Table("EmbededResources").Column("Title").Exists())
            {
                Alter.Table("EmbededResources").AddColumn("Title").AsString().NotNullable().WithDefaultValue("Temporary title");
            }
        }

        public override void Down()
        {
            throw new NotImplementedException();
        }
    }
}
