﻿using System;
using FluentMigrator;

namespace DroneLegends.DataMigrations.Version_0001
{
    /// <summary>
    ///  dotnet-fm migrate -p SqlServer -c "Server=.\SQLEXPRESS2017;Database=DroneLegends;Trusted_Connection=True;MultipleActiveResultSets=true" -a .\DroneLegends.DataMigrations\bin\Debug\netcoreapp3.0\DroneLegends.DataMigrations.dll
    /// </summary>
    [Migration(11, "Add new downloadable materials")]
    public class Migration_0011_Data : Migration
    {
        public override void Up()
        {
            Update.Table("Lessons").Set(new { Name = "Introduction / Mission 1: Flight Training" }).Where(new { Position = 1 });

            // New downloadable material is added for all lessons.
            Execute.Sql(RawSql.Insert("INSERT INTO [DownloadableResources]([Id],[CreationDate],[ModificationDate],[IsDeleted],[Name],[FileName],[Position],[LessonId],[ModuleId]) VALUES (NEWID(),'2021-09-17T18:22:55+02:00','2021-09-17T18:22:55+02:00',0,'Engeneering design planner','Engeneering_Design_Planner.pdf',2,(SELECT [LessonId] FROM [DownloadableResources] WHERE [FileName] = 'Mission_1_Notes.pdf'), null)").Value);
            Execute.Sql(RawSql.Insert("INSERT INTO [DownloadableResources]([Id],[CreationDate],[ModificationDate],[IsDeleted],[Name],[FileName],[Position],[LessonId],[ModuleId]) VALUES (NEWID(),'2021-09-17T18:22:55+02:00','2021-09-17T18:22:55+02:00',0,'Engeneering design planner','Engeneering_Design_Planner.pdf',2,(SELECT [LessonId] FROM [DownloadableResources] WHERE [FileName] = 'Mission_2_Notes.pdf'), null)").Value);
            Execute.Sql(RawSql.Insert("INSERT INTO [DownloadableResources]([Id],[CreationDate],[ModificationDate],[IsDeleted],[Name],[FileName],[Position],[LessonId],[ModuleId]) VALUES (NEWID(),'2021-09-17T18:22:55+02:00','2021-09-17T18:22:55+02:00',0,'Engeneering design planner','Engeneering_Design_Planner.pdf',3,(SELECT [LessonId] FROM [DownloadableResources] WHERE [FileName] = 'Mission_3_Notes.pdf'), null)").Value);
            Execute.Sql(RawSql.Insert("INSERT INTO [DownloadableResources]([Id],[CreationDate],[ModificationDate],[IsDeleted],[Name],[FileName],[Position],[LessonId],[ModuleId]) VALUES (NEWID(),'2021-09-17T18:22:55+02:00','2021-09-17T18:22:55+02:00',0,'Engeneering design planner','Engeneering_Design_Planner.pdf',3,(SELECT [LessonId] FROM [DownloadableResources] WHERE [FileName] = 'Mission_4_Notes.pdf'), null)").Value);
            Execute.Sql(RawSql.Insert("INSERT INTO [DownloadableResources]([Id],[CreationDate],[ModificationDate],[IsDeleted],[Name],[FileName],[Position],[LessonId],[ModuleId]) VALUES (NEWID(),'2021-09-17T18:22:55+02:00','2021-09-17T18:22:55+02:00',0,'Engeneering design planner','Engeneering_Design_Planner.pdf',3,(SELECT [LessonId] FROM [DownloadableResources] WHERE [FileName] = 'Mission_5_Notes.pdf'), null)").Value);
            Execute.Sql(RawSql.Insert("INSERT INTO [DownloadableResources]([Id],[CreationDate],[ModificationDate],[IsDeleted],[Name],[FileName],[Position],[LessonId],[ModuleId]) VALUES (NEWID(),'2021-09-17T18:22:55+02:00','2021-09-17T18:22:55+02:00',0,'Engeneering design planner','Engeneering_Design_Planner.pdf',4,(SELECT [LessonId] FROM [DownloadableResources] WHERE [FileName] = 'Mission_6_Notes.pdf'), null)").Value);
            Execute.Sql(RawSql.Insert("INSERT INTO [DownloadableResources]([Id],[CreationDate],[ModificationDate],[IsDeleted],[Name],[FileName],[Position],[LessonId],[ModuleId]) VALUES (NEWID(),'2021-09-17T18:22:55+02:00','2021-09-17T18:22:55+02:00',0,'Engeneering design planner','Engeneering_Design_Planner.pdf',4,(SELECT [LessonId] FROM [DownloadableResources] WHERE [FileName] = 'Mission_7_Notes.pdf'), null)").Value);
            Execute.Sql(RawSql.Insert("INSERT INTO [DownloadableResources]([Id],[CreationDate],[ModificationDate],[IsDeleted],[Name],[FileName],[Position],[LessonId],[ModuleId]) VALUES (NEWID(),'2021-09-17T18:22:55+02:00','2021-09-17T18:22:55+02:00',0,'Engeneering design planner','Engeneering_Design_Planner.pdf',4,(SELECT [LessonId] FROM [DownloadableResources] WHERE [FileName] = 'Mission_8_Notes.pdf'), null)").Value);
            Execute.Sql(RawSql.Insert("INSERT INTO [DownloadableResources]([Id],[CreationDate],[ModificationDate],[IsDeleted],[Name],[FileName],[Position],[LessonId],[ModuleId]) VALUES (NEWID(),'2021-09-17T18:22:55+02:00','2021-09-17T18:22:55+02:00',0,'Engeneering design planner','Engeneering_Design_Planner.pdf',6,(SELECT [LessonId] FROM [DownloadableResources] WHERE [FileName] = 'Mission_9_Notes.pdf'), null)").Value);
            Execute.Sql(RawSql.Insert("INSERT INTO [DownloadableResources]([Id],[CreationDate],[ModificationDate],[IsDeleted],[Name],[FileName],[Position],[LessonId],[ModuleId]) VALUES (NEWID(),'2021-09-17T18:22:55+02:00','2021-09-17T18:22:55+02:00',0,'Engeneering design planner','Engeneering_Design_Planner.pdf',6,(SELECT [LessonId] FROM [DownloadableResources] WHERE [FileName] = 'Mission_10_Notes.pdf'), null)").Value);
        }

        public override void Down()
        {
            throw new NotImplementedException();
        }
    }
}
