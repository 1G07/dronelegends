﻿using System;
using FluentMigrator;

namespace DroneLegends.DataMigrations.Version_0001
{
    /// <summary>
    ///  dotnet-fm migrate -p SqlServer -c "Server=.\SQLEXPRESS2017;Database=DroneLegends;Trusted_Connection=True;MultipleActiveResultSets=true" -a .\DroneLegends.DataMigrations\bin\Debug\netcoreapp3.0\DroneLegends.DataMigrations.dll
    /// </summary>
    [Migration(19, "Remove ExpiryDate from licenses.")]
    public class Migration_0019_Schema : Migration
    {
        public override void Up()
        {
            Delete.Column("ExpiryDate").FromTable("Licenses");
        }

        public override void Down()
        {
            throw new NotImplementedException();
        }
    }
}
