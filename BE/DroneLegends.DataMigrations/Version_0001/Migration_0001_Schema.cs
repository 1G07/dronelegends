﻿using System;
using FluentMigrator;

namespace DroneLegends.DataMigrations.Version_0001
{
    /// <summary>
    ///  dotnet-fm migrate -p SqlServer -c "Server=.\SQLEXPRESS2017;Database=DroneLegends;Trusted_Connection=True;MultipleActiveResultSets=true" -a .\DroneLegends.DataMigrations\bin\Debug\netcoreapp3.0\DroneLegends.DataMigrations.dll
    /// </summary>
    [Migration(1, "Create database structure")]
    public class Migration_0001_Schema : Migration
    {
        public override void Up()
        {
            Create.TableWithCommonColumns("Contacts")
              .WithColumn("Title").AsString(20).Nullable()
              .WithColumn("FirstName").AsString(255).Nullable()
              .WithColumn("LastName").AsString(255).Nullable()
              .WithColumn("Email").AsString(255).Nullable()
              .WithColumn("Phone").AsString(100).Nullable();

            Create.TableWithCommonColumns("Addresses")
               .WithColumn("Street").AsString(255).NotNullable()
               .WithColumn("City").AsString(255).NotNullable()
               .WithColumn("State").AsString(255).Nullable()
               .WithColumn("PostCode").AsString(255).NotNullable();

            Create.TableWithCommonColumns("Licenses")
              .WithColumn("ExpiryDate").AsDateTime().NotNullable()
              .WithColumn("NumberOfSchools").AsInt32().NotNullable()
              .WithColumn("NumberOfTeachers").AsInt32().NotNullable();

            Create.TableWithCommonColumns("Accounts")
               .WithColumn("Email").AsString(255).NotNullable().Unique()
               .WithColumn("Password").AsString(100).Nullable()
               .WithColumn("AvatarImage").AsString().Nullable()
               .WithColumn("FirstName").AsString(255).NotNullable()
               .WithColumn("LastName").AsString(255).NotNullable()
               .WithColumn("Role").AsInt32().NotNullable()
               .WithColumn("AccessToken").AsString(512).Nullable()
               .WithColumn("AccessTokenIssued").AsDateTime().Nullable()
               .WithColumn("RefreshToken").AsString(512).Nullable()
               .WithColumn("IsResetTokenIssuedForInvitation").AsBoolean().NotNullable().WithDefaultValue(false)
               .WithColumn("RefreshTokenIssued").AsDateTime().Nullable()
               .WithColumn("ResetToken").AsString(512).Nullable()
               .WithColumn("ResetTokenIssued").AsDateTime().Nullable()
               .WithColumn("LastLogin").AsDateTime().Nullable()
               .WithColumn("LicenseId").AsGuid().Nullable().ForeignKey("Licenses", "Id").OnDelete(System.Data.Rule.None).OnUpdate(System.Data.Rule.None);

            Create.TableWithCommonColumns("SuperAdmins")
               .WithColumn("Phone").AsString(100).Nullable()
               .WithColumn("AccountId").AsGuid().ForeignKey("Accounts", "Id").OnDelete(System.Data.Rule.Cascade).OnUpdate(System.Data.Rule.None);

            Create.TableWithCommonColumns("Institutions")
                .WithColumn("Name").AsString(255).NotNullable()
                .WithColumn("AddressId").AsGuid().ForeignKey("Addresses", "Id").OnDelete(System.Data.Rule.None).OnUpdate(System.Data.Rule.None)
                .WithColumn("AccountId").AsGuid().ForeignKey("Accounts", "Id").OnDelete(System.Data.Rule.Cascade).OnUpdate(System.Data.Rule.None)
                .WithColumn("ContactId").AsGuid().ForeignKey("Contacts", "Id").OnDelete(System.Data.Rule.None).OnUpdate(System.Data.Rule.None).Nullable()
                .WithColumn("ParentId").AsGuid().ForeignKey("Institutions", "Id").OnDelete(System.Data.Rule.None).OnUpdate(System.Data.Rule.None).Nullable();

            Create.TableWithCommonColumns("Teachers")
                .WithColumn("FirstName").AsString(255).NotNullable()
                .WithColumn("LastName").AsString(255).NotNullable()
                .WithColumn("Title").AsString(255).NotNullable()
                .WithColumn("Phone").AsString(255).NotNullable()
                .WithColumn("ParentId").AsGuid().ForeignKey("Institutions", "Id").OnDelete(System.Data.Rule.None).OnUpdate(System.Data.Rule.None)
                .WithColumn("AccountId").AsGuid().ForeignKey("Accounts", "Id").OnDelete(System.Data.Rule.Cascade).OnUpdate(System.Data.Rule.None);

            Create.TableWithCommonColumns("Modules")
                .WithColumn("Name").AsString().NotNullable();

            Create.TableWithCommonColumns("Lessons")
                .WithColumn("Name").AsString().NotNullable()
                .WithColumn("ModuleId").AsGuid().ForeignKey("Modules", "Id").OnDelete(System.Data.Rule.Cascade).OnUpdate(System.Data.Rule.None);

            Alter.Table("Licenses").AddColumn("OwnerId").AsGuid().Nullable().ForeignKey("Institutions", "Id").OnDelete(System.Data.Rule.Cascade).OnUpdate(System.Data.Rule.None);
        }

        public override void Down()
        {
            throw new NotImplementedException();
        }
    }
}
