﻿using System;
using FluentMigrator;

namespace DroneLegends.DataMigrations.Version_0001
{
    /// <summary>
    ///  dotnet-fm migrate -p SqlServer -c "Server=.\SQLEXPRESS2017;Database=DroneLegends;Trusted_Connection=True;MultipleActiveResultSets=true" -a .\DroneLegends.DataMigrations\bin\Debug\netcoreapp3.0\DroneLegends.DataMigrations.dll
    /// </summary>
    [Migration(12, "Add embeded resources schema")]
    public class Migration_0012_Schema : Migration
    {
        public override void Up()
        {
            Create.TableWithCommonColumns("EmbededResources")
               .WithColumn("Url").AsString().NotNullable()
               .WithColumn("Position").AsInt32().NotNullable()
               .WithColumn("LessonId").AsGuid().ForeignKey("Lessons", "Id").OnDelete(System.Data.Rule.None).OnUpdate(System.Data.Rule.None).Nullable();
        }

        public override void Down()
        {
            throw new NotImplementedException();
        }
    }
}
