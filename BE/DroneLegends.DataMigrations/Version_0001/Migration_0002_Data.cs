﻿using System;
using FluentMigrator;

namespace DroneLegends.DataMigrations.Version_0001
{
    /// <summary>
    ///  dotnet-fm migrate -p SqlServer -c "Server=.\SQLEXPRESS2017;Database=DroneLegends;Trusted_Connection=True;MultipleActiveResultSets=true" -a .\DroneLegends.DataMigrations\bin\Debug\netcoreapp3.0\DroneLegends.DataMigrations.dll
    /// </summary>
    [Migration(2, "Insert SuperAdmin account")]
    public class Migration_0002_Schema : Migration
    {
        public override void Up()
        {
            DateTimeOffset now = DateTimeOffset.Now;
            Guid licenseId = Guid.NewGuid();
            Guid accountId = Guid.NewGuid();

            Insert.IntoTable("Licenses").Row(new { Id = licenseId, CreationDate = now, ModificationDate = now, IsDeleted = false, ExpiryDate = DateTime.MaxValue, NumberOfSchools = Int32.MaxValue, NumberOfTeachers = Int32.MaxValue });

            // Password = Dr0neLegends132. => 3bfd9abb55f047866422e213febcf2402313d76444fa5f30c55fd1fd05579e63 when hashed
            Insert.IntoTable("Accounts").Row(new { Id = accountId, CreationDate = now, ModificationDate = now, IsDeleted = false, Email = "dev@dronelegends.com", Password = "3bfd9abb55f047866422e213febcf2402313d76444fa5f30c55fd1fd05579e63", AvatarImage = "", FirstName = "Scott", LastName = "Buell", Role = 1, LicenseId = licenseId, IsResetTokenIssuedForInvitation = false });
            Insert.IntoTable("SuperAdmins").Row(new { Id = Guid.NewGuid(), CreationDate = now, ModificationDate = now, IsDeleted = false, Phone = string.Empty, AccountId = accountId });
        }

        public override void Down()
        {
            throw new NotImplementedException();
        }
    }
}
