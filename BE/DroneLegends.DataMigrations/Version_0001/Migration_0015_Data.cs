﻿using System;
using FluentMigrator;

namespace DroneLegends.DataMigrations.Version_0001
{
    /// <summary>
    ///  dotnet-fm migrate -p SqlServer -c "Server=.\SQLEXPRESS2017;Database=DroneLegends;Trusted_Connection=True;MultipleActiveResultSets=true" -a .\DroneLegends.DataMigrations\bin\Debug\netcoreapp3.0\DroneLegends.DataMigrations.dll
    /// </summary>
    [Migration(15, "Update RegistrationStatus in Accounts table")]
    public class Migration_0015_Data : Migration
    {
        public override void Up()
        {
            Execute.Sql(RawSql.Insert("UPDATE Accounts SET RegistrationStatus = 1 WHERE Password IS NULL AND ResetToken IS NULL AND IsResetTokenIssuedForInvitation = 0").Value);
            Execute.Sql(RawSql.Insert("UPDATE Accounts SET RegistrationStatus = 2 WHERE Password IS NULL AND ResetToken IS NOT NULL AND IsResetTokenIssuedForInvitation = 1").Value);
            Execute.Sql(RawSql.Insert("UPDATE Accounts SET RegistrationStatus = 3 WHERE Password IS NOT NULL").Value);
        }

        public override void Down()
        {
            throw new NotImplementedException();
        }
    }
}
