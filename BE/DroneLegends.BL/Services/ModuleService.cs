﻿using DroneLegends.BL.DomainModels;
using DroneLegends.BL.Exceptions;
using DroneLegends.BL.RequiredInterfaces;
using DroneLegends.DataModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace DroneLegends.BL.Services
{
    public class ModuleService : ServiceBase
    {
        private readonly ContextService context;
        private readonly IConfiguration configuration;
        private readonly AccountService accountService;

        public ModuleService(IUnitOfWorkFactory unitOfWorkFactory, ContextService context, IConfiguration configuration, AccountService accountService)
       : base(unitOfWorkFactory.CreateUnitOfWork())
        {
            this.context = context;
            this.configuration = configuration;
            this.accountService = accountService;
        }

        private IUnitOfWork AppUnitOfWork
        {
            get { return (IUnitOfWork)this.UnitOfWork; }
        }

        public List<Module> GetAllForUser()
        {
            List<Module> result = new List<Module>();

            if (this.context.CurrentAccount.Role == DataModels.Enums.Role.SuperAdmin) {
                result = this.GetAll();
            } 
            else
            {
                foreach (Subscription subscription in this.AppUnitOfWork.SubscriptionRepository.GetByLicenseId(this.context.CurrentAccount.LicenseId.Value))
                {
                    result.Add(subscription.Module);
                }
            }

            return result;
        }

        public List<Module> GetAll()
        {
            return (List<Module>)this.AppUnitOfWork.ModuleRepository.GetAll();
        }
        
        public List<Module> GetAllModules(Guid licenseId)
        {
            List<Module> result = new List<Module>();

            foreach (Subscription subscription in this.AppUnitOfWork.SubscriptionRepository.GetByLicenseId(licenseId))
            {
                result.Add(subscription.Module);
            }

            return result;
        }
        
        public List<Lesson> GetLessons(Guid moduleId)
        {
            if (this.context.CurrentAccount.Role != DataModels.Enums.Role.SuperAdmin)
            {
                this.CheckSubscription(moduleId);
            }

            return this.AppUnitOfWork.LessonRepository.GetForModule(moduleId);
        }
        
        public LessonDO GetLesson(Guid lessonId)
        {
            if (this.context.CurrentAccount.Role != DataModels.Enums.Role.SuperAdmin)
            {
                Lesson needleLesson = this.AppUnitOfWork.LessonRepository.GetById(lessonId);
                this.CheckSubscription(needleLesson != null ? needleLesson.ModuleId : Guid.Empty);
            }

            Lesson lesson = this.AppUnitOfWork.LessonRepository.GetLessonDetails(lessonId);

            if (lesson == null)
            {
                throw new BusinessException("Lesson with provided ID can not be found");
            }

            return new LessonDO(lesson, this.AppUnitOfWork.DownloadableResourceRepository.GetByLessonId(lessonId), this.AppUnitOfWork.EmbededResourceRepository.GetByLessonId(lessonId));
        }
        
        public string GetPresentationUrl(Guid lessonId)
        {
            LessonDO lessonDO = this.GetLesson(lessonId);

            lessonDO.Lesson.PresentationHash = AccountService.Encrypt(this.context.CurrentAccount.AccessToken, this.context.CurrentAccount.Email + lessonId + DateTime.Now);
            lessonDO.Lesson.PresentationHashExpiry = DateTime.Now.AddMinutes(5);

            this.AppUnitOfWork.LessonRepository.UpdateEntity(lessonDO.Lesson);
            this.Save();

            string baseUrl = this.configuration["BaseUrl"].TrimEnd('/');

            return $"{baseUrl}/assets/PresentationResources/{lessonDO.Lesson.Module.FolderName}/{lessonDO.Lesson.FolderName}/index.html?lessonId={lessonDO.Lesson.Id}&hash={lessonDO.Lesson.PresentationHash}";
        }

        public FileStreamResult DownloadResource(Guid resourceId)
        {
            DownloadableResource resource = this.AppUnitOfWork.DownloadableResourceRepository.GetById(resourceId);
            
            if (resource == null)
            {
                throw new BusinessException("Resource can not be found on server.");
            }

            if (this.context.CurrentAccount.Role != DataModels.Enums.Role.SuperAdmin)
            {
                this.CheckLessonSubscription(resource.LessonId.Value);
            }
            
            string filePath = Path.Combine(Path.Combine(this.configuration.GetValue<string>("ResourceDirectory"), $"{resource.Lesson.Module.FolderName}"), $"{resource.Lesson.FolderName}");
            filePath = Path.Combine(filePath, $"{resource.FileName}");
            FileStream fs = File.Open(filePath, FileMode.Open);

            return new FileStreamResult(fs, "application/pdf") { FileDownloadName = Path.GetFileName(filePath) };
        }

        private void CheckSubscription(Guid moduleId)
        {
            List<Subscription> subscriptions = this.AppUnitOfWork.SubscriptionRepository.GetByLicenseId(this.context.CurrentAccount.LicenseId.Value);

            if (subscriptions == null || subscriptions.FirstOrDefault(s => s.ModuleId == moduleId) == null)
            {
                throw new UnauthorizedException("You are not subscribed to this module. Please contact the Administrator to extend the license plan.");
            }
        }

        private void CheckLessonSubscription(Guid lessonId)
        {
            List<Subscription> subscriptions = this.AppUnitOfWork.SubscriptionRepository.GetByLicenseId(this.context.CurrentAccount.LicenseId.Value);
            Lesson lesson = this.AppUnitOfWork.LessonRepository.GetById(lessonId);

            if (subscriptions == null || subscriptions.FirstOrDefault(s => s.ModuleId == lesson.ModuleId) == null)
            {
                throw new UnauthorizedException("You are not subscribed to this module. Please contact the Administrator to extend the license plan.");
            }
        }
    }
}
