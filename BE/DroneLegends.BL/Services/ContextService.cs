﻿using DroneLegends.DataModels;

namespace DroneLegends.BL.Services
{
    /// <summary>
    /// Service that hold current context per request.
    /// </summary>
    public class ContextService
    {
        /// <summary>
        /// Gets or sets current account.
        /// </summary>
        public Account CurrentAccount { get; set; }
    }
}
