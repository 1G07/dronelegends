﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DroneLegends.BL.RequiredInterfaces;
using DroneLegends.DataModels;
using Microsoft.Extensions.Configuration;

namespace DroneLegends.BL.Services
{
    public class NotificationEmailsService
    {
        private readonly MailService mailService;
        private readonly IConfiguration configuration;
        private IUnitOfWork unitOfWork { get; set; }

        public NotificationEmailsService(IUnitOfWorkFactory unitOfWorkFactory, MailService mailService, IConfiguration configuration)
        {
            this.mailService = mailService;
            this.configuration = configuration;
            this.unitOfWork = unitOfWorkFactory.CreateUnitOfWork();
        }

        public void SendNotifications()
        {
            List<Account> allAccounts = this.unitOfWork.AccountRepository.GetAll().ToList();

            foreach (Account account in allAccounts)
            {
                if (!account.IsSubscribedToMails)
                {
                    continue;
                }

                this.SendEmailForLicenseExpiry(account, 90);
                this.SendEmailForLicenseExpiry(account, 15);
                this.SendEmailForLicenseExpiry(account, 1);

                if (account.IsLockedDueToLicenseChange && !account.IsLockedEmailSent)
                {
                    this.SendEmailForLockedAccount(account);

                    account.IsLockedEmailSent = true;
                    this.unitOfWork.AccountRepository.UpdateEntity(account);
                }
            }

            this.unitOfWork.SaveChanges();
        }

        public void SendEmailForLockedAccount(Account account)
        {
            this.mailService.Send(account.Email, this.GetLockedEmailSubject(account), this.GetLockedAccountMailMessage(account));
        }

        private void SendEmailForLicenseExpiry(Account account, int numberOfDays)
        {
            Institution accountInstitution = this.unitOfWork.InstitutionRepository.GetByAccountId(account.Id.Value);

            if (account.License == null || account.License.ExpiryDate == null || accountInstitution == null || accountInstitution.Id != account.License.OwnerId)
            {
                return;
            }

            if (DateTime.Today.AddDays(numberOfDays).Date == account.License.ExpiryDate.Value.Date)
            {
                this.mailService.Send(account.Email, this.GetLicenseExpirationMailSubjectBasedOnNumberOfDays(account, numberOfDays), this.GetLicenseExpiryMailMessage(account, numberOfDays));
            }
        }

        private string GetLicenseExpirationMailSubjectBasedOnNumberOfDays(Account account, int numberOfDays)
        {
            if (numberOfDays == 90)
            {
                return $"{account.FirstName}, will you stay with us for another year?";
            }
            
            if (numberOfDays == 15)
            {
                return "Oh no! There’s only 15 days left until your license expires.";
            }

            return "We’ll miss you!";
        }

        private string GetLicenseExpiryMailMessage(Account account, int numberOfDays)
        {
            account.UnsubscribeToken = AccountService.Encrypt(account.FirstName, $"{account.FirstName}{account.LastName}{account.IsSubscribedToMails}");

            this.unitOfWork.AccountRepository.UpdateEntity(account);
            this.unitOfWork.SaveChanges();

            string result;

            using (StreamReader sourceReader = File.OpenText(Path.Combine(this.mailService.Configuration.TemplateDirectory.ToString(), $"license-expiry-{numberOfDays}.html")))
            {
                result = sourceReader.ReadToEnd();
            }

            string url = this.configuration["BaseUrl"] + this.configuration["UnsubscribeUrl"].Replace(":token", account.UnsubscribeToken, StringComparison.Ordinal);

            return result.Replace("{unsubscribeUrl}", url, StringComparison.Ordinal).Replace("{firstName}", account.FirstName).Replace("{expiryDate}", account.License.ExpiryDate.Value.Date.ToString());
        }

        private string GetLockedAccountMailMessage(Account account)
        {
            string result;

            using (StreamReader sourceReader = File.OpenText(Path.Combine(this.mailService.Configuration.TemplateDirectory.ToString(), "account-locked.html")))
            {
                result = sourceReader.ReadToEnd();
            }

            string url = this.configuration["BaseUrl"] + this.configuration["UnsubscribeUrl"].Replace(":token", account.UnsubscribeToken, StringComparison.Ordinal);

            // TODO Change when defined!
            return result.Replace("{unsubscribeUrl}", url, StringComparison.Ordinal);
        }

        private string GetLockedEmailSubject(Account account)
        {
            // TODO: Change when defined!
            return "Account locked";
        }
    }
}
