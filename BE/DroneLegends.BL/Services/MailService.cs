﻿using System;
using System.Net.Mail;
using DroneLegends.BL.DomainModels;
using DroneLegends.BL.Exceptions;
using Microsoft.Extensions.Options;

namespace DroneLegends.BL.Services
{
    /// <summary>
    /// Mail service.
    /// </summary>
    public class MailService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MailService"/> class.
        /// </summary>
        /// <param name="configuration">Mail configuration.</param>
        public MailService(IOptions<MailConfiguration> configuration)
        {
            this.Configuration = configuration.Value;
        }

        /// <summary>
        /// Mail configuration.
        /// </summary>
        public MailConfiguration Configuration { get; }

        /// <summary>
        /// Sends an email based on configuration and params.
        /// </summary>
        /// <param name="to">To who.</param>
        /// <param name="subject">Email subject.</param>
        /// <param name="message">Email message.</param>
        public void Send(string to, string subject, string message, string from = null)
        {
            if (to is null)
            {
                throw new ArgumentNullException(nameof(to));
            }

            if (subject is null)
            {
                throw new ArgumentNullException(nameof(subject));
            }

            if (message is null)
            {
                throw new ArgumentNullException(nameof(message));
            }

            MailMessage mail = new MailMessage()
            {
                From = new MailAddress(string.IsNullOrEmpty(from) ? this.Configuration.Sender : from),
                Subject = subject,
                Body = message,
                IsBodyHtml = true
            };

            mail.To.Add(to);

            using SmtpClient smtpServer = new SmtpClient(this.Configuration.SmtpServer)
            {
                EnableSsl = this.Configuration.EnableSsl,
                Port = this.Configuration.Port,
                UseDefaultCredentials = false,
                Credentials = new System.Net.NetworkCredential(this.Configuration.Username.Trim(), this.Configuration.Password.Trim())
            };

            try
            {
                smtpServer.Send(mail);
            }
            catch (InvalidOperationException ex)
            {
                throw new MailSendException(ex.Message, ex);
            }
            catch (SmtpFailedRecipientException ex)
            {
                throw new MailSendException("The message could not be delivered", ex, to);
            }
            catch (SmtpException ex)
            {
                throw new MailSendException(ex.Message, ex);
            }
        }
    }
}
