﻿using System;
using System.Collections.Generic;
using DroneLegends.BL.Exceptions;
using DroneLegends.BL.RequiredInterfaces;
using DroneLegends.DataModels;
using DroneLegends.DataModels.Enums;

namespace DroneLegends.BL.Services
{
    /// <summary>
    /// Teacher manipulation service.
    /// </summary>
    public class TeacherService : ServiceBase
    {
        private readonly ContextService context;
        private readonly InstitutionService institutionService;

        public TeacherService(IUnitOfWorkFactory unitOfWorkFactory, ContextService context, InstitutionService institutionService)
         : base(unitOfWorkFactory.CreateUnitOfWork())
        {
            this.context = context;
            this.institutionService = institutionService;
        }

        private IUnitOfWork AppUnitOfWork
        {
            get { return (IUnitOfWork)this.UnitOfWork; }
        }

        /// <summary>
        /// Gets the list by sent filters.
        /// </summary>
        /// <param name="filter">Filter</param>
        /// <returns>List of teachers.</returns>
        public List<Teacher> Get(TeacherFilter filter)
        {
            if (this.context.CurrentAccount.Role != Role.SuperAdmin)
            {
                Institution currentInstitution = this.AppUnitOfWork.InstitutionRepository.GetByAccountId(this.context.CurrentAccount.Id.Value);
                filter.ParentId = currentInstitution.Id;
            }
            
            if (filter.Limit == -1)
            {
                filter.Limit = int.MaxValue;
            }

            List<Teacher> teachers = this.AppUnitOfWork.TeacherRepository.Get(filter);

            foreach (Teacher t in teachers)
            {
                this.PopulateParentTree(t);
            }

            return teachers;
        }

        /// <summary>
        /// Get total count by defined filter.
        /// </summary>
        /// <param name="filter">Filter</param>
        /// <returns>Total count</returns>
        public int GetTotalCount(TeacherFilter filter)
        {
            return this.AppUnitOfWork.TeacherRepository.GetTotalCount(filter);
        }

        /// <summary>
        /// Gets the teacher by id.
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Teacher.</returns>
        public Teacher GetById(Guid id)
        {
            Teacher result = this.GetTeacher(id);

            this.PopulateParentTree(result);

            return result;
        }

        /// <summary>
        /// Gets self.
        /// </summary>
        /// <returns>Teacher.</returns>
        public Teacher GetSelf()
        {
            Teacher currentTeacher = this.AppUnitOfWork.TeacherRepository.GetByAccountId(this.context.CurrentAccount.Id.Value);

            if (currentTeacher == null)
            {
                throw new BusinessException("Can not find the record by provided account id.");
            }

            return this.GetById(currentTeacher.Id.Value);
        }

        /// <summary>
        /// Saves the new teacher.
        /// </summary>
        /// <param name="teacher">Teacher for saving.</param>
        /// <returns>Saved teacher.</returns>
        public Teacher Save(Teacher teacher)
        {
            this.ValidateTeacherForSave(teacher);
            this.SetTeacherLicense(teacher);
            this.SetTeacherParent(teacher);

            this.AppUnitOfWork.AccountRepository.AddEntity(teacher.Account);
            teacher.AccountId = teacher.Account.Id.Value;

            this.AppUnitOfWork.TeacherRepository.AddEntity(teacher);

            this.AppUnitOfWork.SaveChanges();

            this.PopulateParentTree(teacher);

            return teacher;
        }

        /// <summary>
        /// Updates the existing teacher.
        /// </summary>
        /// <param name="teacher">Teacher for saving.</param>
        /// <returns>Saved teacher.</returns>
        public Teacher Update(Teacher teacher)
        {
            Teacher savedTeacher = this.GetTeacher(teacher.Id.Value);

            if (this.context.CurrentAccount.Role == Role.SuperAdmin)
            {
                this.ValidateTeacherParent(teacher);
            }
            this.SetTeacherParent(teacher);

            if (savedTeacher.Account.Email != teacher.Account.Email && this.AppUnitOfWork.AccountRepository.GetAccountByEmail(teacher.Account.Email) != null)
            {
                throw new BusinessException("Teacher with email already exists.", teacher.Account.Email);
            }

            savedTeacher.Account.Email = teacher.Account.Email;
            savedTeacher.Account.Role = teacher.Account.Role;
            savedTeacher.Account.AvatarImage = teacher.Account.AvatarImage;
            savedTeacher.Account.FirstName = teacher.Account.FirstName;
            savedTeacher.Account.LastName = teacher.Account.LastName;
            teacher.Account = savedTeacher.Account;
            this.SetEntityBaseFields(teacher, savedTeacher);
            this.AppUnitOfWork.AccountRepository.UpdateEntity(savedTeacher.Account);
            this.AppUnitOfWork.TeacherRepository.UpdateEntity(teacher);

            this.AppUnitOfWork.SaveChanges();

            this.PopulateParentTree(teacher);

            return teacher;
        }

        /// <summary>
        /// Deletes teacher by id.
        /// </summary>
        /// <param name="id">Id of teacher for delete.</param>
        public void Delete(Guid id)
        {
            Teacher teacher = this.GetTeacher(id);

            this.AppUnitOfWork.AccountRepository.DeleteEntity(teacher.Account);

            this.Save();
        }

        private Teacher GetTeacher(Guid id)
        {
            Teacher teacher;
            Teacher currentTeacher = this.AppUnitOfWork.TeacherRepository.GetByAccountId(this.context.CurrentAccount.Id.Value);

            if (this.context.CurrentAccount.Role == Role.SuperAdmin || (currentTeacher != null && currentTeacher.Id == id))
            {
                teacher = this.AppUnitOfWork.TeacherRepository.GetById(id);
            }
            else
            {
                teacher = this.AppUnitOfWork.TeacherRepository.GetByIdAndParentAccountId(id, this.context.CurrentAccount.Id.Value);
            }

            if (teacher == null)
            {
                throw new BusinessException("Can not find the record by provided id.");
            }

            return teacher;
        }

        private void ValidateTeacherForSave(Teacher teacher)
        {
            if (this.context.CurrentAccount.Role != Role.SuperAdmin)
            {
                if (teacher.ParentId != Guid.Empty)
                {
                    throw new BusinessException("Parent ID should not be set for this role.");
                }

                Institution currentInstitution = this.AppUnitOfWork.InstitutionRepository.GetByAccountId(this.context.CurrentAccount.Id.Value);
                int numberOfSavedTeachers = this.AppUnitOfWork.TeacherRepository.GetNumberOfSavedTeachers(currentInstitution.Id.Value);

                if (this.context.CurrentAccount.License.NumberOfTeachers == numberOfSavedTeachers)
                {
                    throw new BusinessException("Number of teachers exceeded for a license. Please contact Administrator to expand the license plan.");
                }
            }
            else
            {
                this.ValidateTeacherParent(teacher);
                this.ValidateParentLicense(teacher);
            }

            if (teacher.Id != null)
            {
                throw new BusinessException("New account must not have id. Could not save new account!");
            }

            if (this.AppUnitOfWork.AccountRepository.GetAccountByEmail(teacher.Account.Email) != null)
            {
                throw new BusinessException("Teacher with email already exists.", teacher.Account.Email);
            }
        }

        private void ValidateTeacherParent(Teacher teacher)
        {
            Institution parentInstitution = this.AppUnitOfWork.InstitutionRepository.GetById(teacher.ParentId);

            if (parentInstitution == null || (parentInstitution.Account.Role != Role.School && parentInstitution.Account.Role != Role.OtherBusiness))
            {
                throw new BusinessException("Parent institution is not valid.");
            }
        }

        private void ValidateParentLicense(Teacher teacher)
        {
            Institution parentInstitution = this.AppUnitOfWork.InstitutionRepository.GetById(teacher.ParentId);
            int numberOfSavedTeachers = this.AppUnitOfWork.TeacherRepository.GetNumberOfSavedTeachers(parentInstitution.Id.Value);

            if (parentInstitution.Account.License == null || parentInstitution.Account.License.NumberOfTeachers == numberOfSavedTeachers)
            {
                throw new BusinessException("Number of teachers exceeded for a license. Please contact Administrator to expand the license plan.");
            }
        }

        private void SetTeacherLicense(Teacher teacher)
        {
            if (this.context.CurrentAccount.Role != Role.SuperAdmin)
            {
                teacher.Account.License = this.context.CurrentAccount.License;
                teacher.Account.LicenseId = this.context.CurrentAccount.LicenseId;
            }
            else
            {
                Institution parentInstitution = this.AppUnitOfWork.InstitutionRepository.GetById(teacher.ParentId);
                teacher.Account.License = parentInstitution.Account.License;
                teacher.Account.LicenseId = parentInstitution.Account.LicenseId;
            }
        }

        private void SetTeacherParent(Teacher teacher)
        {
            if (this.context.CurrentAccount.Role == Role.OtherBusiness || this.context.CurrentAccount.Role == Role.School)
            {
                Institution currentInstitution = this.AppUnitOfWork.InstitutionRepository.GetByAccountId(this.context.CurrentAccount.Id.Value);

                if (currentInstitution == null)
                {
                    throw new BusinessException("Teacher parent institution for currently logged user can not be found.");
                }

                teacher.ParentId = currentInstitution.Id.Value;
                teacher.Parent = currentInstitution;
            }
            else if (this.context.CurrentAccount.Role == Role.Teacher)
            {
                Teacher currentTeacher = this.AppUnitOfWork.TeacherRepository.GetByAccountId(this.context.CurrentAccount.Id.Value);

                if (currentTeacher == null)
                {
                    throw new BusinessException("Teacher for currently logged user can not be found.");
                }

                if (teacher.Id != null && currentTeacher.Id != teacher.Id)
                {
                    throw new BusinessException("Teacher can not manipulate with other teachers.");
                }

                teacher.ParentId = currentTeacher.ParentId;
                teacher.Parent = currentTeacher.Parent;
            }
            else
            {
                Institution parentInstitution = this.AppUnitOfWork.InstitutionRepository.GetById(teacher.ParentId);
                teacher.ParentId = parentInstitution.Id.Value;
                teacher.Parent = parentInstitution;
            }
        }

        private void PopulateParentTree(Teacher t)
        {
            t.Parent = this.institutionService.GetById(t.ParentId);
        }
    }
}
