﻿using DroneLegends.DataAccess.Common;
using DroneLegends.DataAccess.Common.Interfaces;

namespace DroneLegends.BL.Services
{
    public class ServiceBase
    {
        public ServiceBase(IUnitOfWork unitOfWork)
        {
            this.UnitOfWork = unitOfWork;
        }

        protected IUnitOfWork UnitOfWork { get; private set; }

        public void Save()
        {
            this.Saving();
            this.SaveInternal();
            this.Saved();
        }

        protected virtual void SaveInternal()
        {
            this.UnitOfWork.SaveChanges();
        }

        protected virtual void Saving()
        {
        }

        protected virtual void Saved()
        {
        }

        protected void SetEntityBaseFields(EntityBase entityForUpdate, EntityBase savedEntity)
        {
            entityForUpdate.Id = savedEntity.Id;
            entityForUpdate.CreationDate = savedEntity.CreationDate;
            entityForUpdate.ModificationDate = savedEntity.ModificationDate;
            entityForUpdate.IsDeleted = savedEntity.IsDeleted;
        }
    }
}
