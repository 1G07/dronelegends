﻿using System;
using DroneLegends.BL.RequiredInterfaces;
using DroneLegends.DataModels;

namespace DroneLegends.BL.Services
{
    public class TicketService : ServiceBase
    {
        private static string AdminEmail = "scott@dronelegends.com";

        private readonly ContextService context;

        private readonly MailService mailService;

        public TicketService(IUnitOfWorkFactory unitOfWorkFactory, ContextService context, MailService mailService)
         : base(unitOfWorkFactory.CreateUnitOfWork())
        {
            this.context = context;
            this.mailService = mailService;
        }

        private IUnitOfWork AppUnitOfWork
        {
            get { return (IUnitOfWork)this.UnitOfWork; }
        }

        /// <summary>
        /// Saves the ticket and sends an email to admin.
        /// </summary>
        /// <param name="ticket">Ticket.</param>
        public void SaveTicket(Ticket ticket)
        {
            ticket.AccountId = this.context.CurrentAccount.Id.Value;
            ticket.Account = this.context.CurrentAccount;

            this.mailService.Send(AdminEmail, ticket.Type.ToString(), ticket.Text, ticket.Account.Email);
            this.AppUnitOfWork.TicketRepository.AddEntity(ticket);
            this.Save();
        }
    }
}
