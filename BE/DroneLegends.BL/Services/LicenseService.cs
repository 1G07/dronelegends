﻿using System;
using System.Collections.Generic;
using System.Linq;
using DroneLegends.BL.DomainModels;
using DroneLegends.BL.Exceptions;
using DroneLegends.BL.RequiredInterfaces;
using DroneLegends.DataModels;

namespace DroneLegends.BL.Services
{
    /// <summary>
    /// License manipulation service.
    /// </summary>
    public class LicenseService : ServiceBase
    {
        private readonly ModuleService moduleService;

        public LicenseService(IUnitOfWorkFactory unitOfWorkFactory, ModuleService moduleService)
         : base(unitOfWorkFactory.CreateUnitOfWork())
        {
            this.moduleService = moduleService;
        }

        private IUnitOfWork AppUnitOfWork
        {
            get { return (IUnitOfWork)this.UnitOfWork; }
        }

        /// <summary>
        /// Saves the new license.
        /// </summary>
        /// <param name="license">License for saving.</param>
        /// <returns>Saved license.</returns>
        public LicenseDO Save(Guid institutionId, License license, List<Guid> moduleIds)
        {
            if (license.Id != null)
            {
                throw new BusinessException("New license must not have id. Could not save new license!");
            }

            Institution institution = this.AppUnitOfWork.InstitutionRepository.GetById(institutionId);

            if (institution == null)
            {
                throw new BusinessException("Account for which the license should be saved does not exist.");
            }

            if (institution.Account.License != null)
            {
                throw new BusinessException($"Account already has a license.");
            }

            if (institution.ParentId != null)
            {
                throw new BusinessException($"Institution inherits its parent license. License must be changed on the parents level.");
            }

            license.OwnerId = institution.Id;
            license.Owner = institution;
            this.AppUnitOfWork.LicenseRepository.AddEntity(license);
            institution.Account.LicenseId = license.Id;
            institution.Account.License = license;
            this.AppUnitOfWork.AccountRepository.UpdateEntity(institution.Account);

            this.SetLicenseOnChildren(institution, license);
            List<Module> modules = this.SetModuleSubscriptions(license.Id.Value, moduleIds);

            this.AppUnitOfWork.SaveChanges();

            return new LicenseDO(license, modules);
        }

        /// <summary>
        /// Updates the existing license.
        /// </summary>
        /// <param name="license">License for saving.</param>
        /// <returns>Saved license.</returns>
        public LicenseDO Update(License license, List<Guid> moduleIds)
        {
            License savedLicense = this.AppUnitOfWork.LicenseRepository.GetById(license.Id.Value);

            if (savedLicense == null)
            {
                throw new BusinessException("Can not find the record to update.");
            }

            this.SetEntityBaseFields(license, savedLicense);
            license.OwnerId = savedLicense.OwnerId;
            license.Owner = savedLicense.Owner;
            license.StartDate = savedLicense.StartDate;

            if (savedLicense.NumberOfSchools != license.NumberOfSchools)
            {
                this.UnlockAllAccounts(license.Id.Value);
            }

            this.AppUnitOfWork.LicenseRepository.UpdateEntity(license);
            List<Module> modules = this.SetModuleSubscriptions(license.Id.Value, moduleIds);

            this.AppUnitOfWork.SaveChanges();

            return new LicenseDO(license, modules);
        }

        /// <summary>
        /// Assings the existing license to account.
        /// </summary>
        /// <param name="accountEmail">Account email.</param>
        /// <param name="licenseId">License id.</param>
        /// <returns>Saved license.</returns>
        public LicenseDO Assign(string accountEmail, Guid licenseId)
        {
            Account account = this.AppUnitOfWork.AccountRepository.GetAccountByEmail(accountEmail);

            if (account == null)
            {
                throw new BusinessException($"Can not find the account associated with email address: {accountEmail}.");
            }

            License license = this.AppUnitOfWork.LicenseRepository.GetById(licenseId);

            if (license == null)
            {
                throw new BusinessException($"Can not find the license with the provided id: {licenseId}.");
            }

            account.LicenseId = license.Id;
            account.License = license;
            this.AppUnitOfWork.AccountRepository.UpdateEntity(account);

            this.AppUnitOfWork.SaveChanges();

            return new LicenseDO(license, this.moduleService.GetAllModules(license.Id.Value));
        }

        /// <summary>
        /// Detaches the license frome an account.
        /// </summary>
        /// <param name="accountEmail">Account email.</param>
        public void Detach(string accountEmail)
        {
            Account account = this.AppUnitOfWork.AccountRepository.GetAccountByEmail(accountEmail);

            if (account == null)
            {
                throw new BusinessException($"Can not find the account associated with email address: {accountEmail}.");
            }

            if (account.LicenseId != null)
            {
                account.LicenseId = null;
            }

            this.AppUnitOfWork.AccountRepository.UpdateEntity(account);

            this.AppUnitOfWork.SaveChanges();
        }

        /// <summary>
        /// Calculates and returns the license expiry date.
        /// </summary>
        /// <param name="id">License id.</param>
        /// <param name="numberOfMonths">Number of months.</param>
        /// <returns>Expiration date.</returns>
        public long? CalcualteExpirationDate(Guid id, int numberOfMonths)
        {
            License license = this.AppUnitOfWork.LicenseRepository.GetById(id);

            if (license == null)
            {
                throw new BusinessException("Can not find the license with the provided id.");
            }

            license.DurationInMonths = numberOfMonths;

            if (license.ExpiryDate != null)
            {
                return ((DateTimeOffset)license.ExpiryDate).ToUnixTimeSeconds();
            }

            return null;
        }

        /// <summary>
        /// Deletes license by id.
        /// </summary>
        /// <param name="id">Id of license for delete.</param>
        public void Delete(Guid id)
        {
            License license = this.AppUnitOfWork.LicenseRepository.GetById(id);

            if (license == null)
            {
                throw new BusinessException("License does not exist. Could not delete license!");
            }

            foreach (Account account in this.AppUnitOfWork.AccountRepository.GetByLicenseId(id))
            {
                account.LicenseId = null;
                account.License = null;
                this.AppUnitOfWork.AccountRepository.UpdateEntity(account);
            }

            foreach (Subscription subscription in this.AppUnitOfWork.SubscriptionRepository.GetByLicenseId(id))
            {
                this.AppUnitOfWork.SubscriptionRepository.DeleteEntity(subscription);
            }

            this.AppUnitOfWork.LicenseRepository.DeleteEntity(license);
            this.Save();
        }

        public void SetLicenseOnChildren(Institution institution, License license)
        {
            if (institution.Id != null)
            {
                if (institution.Account.Role == DataModels.Enums.Role.School || institution.Account.Role == DataModels.Enums.Role.OtherBusiness)
                {
                    List<Teacher> children = this.AppUnitOfWork.TeacherRepository.Get(new TeacherFilter() { ParentId = institution.Id, Offset = 1, Limit = int.MaxValue });

                    foreach (Teacher child in children)
                    {
                        child.Account.License = license;
                        child.Account.LicenseId = license?.Id;
                        this.AppUnitOfWork.AccountRepository.UpdateEntity(child.Account);
                    }

                    this.Save();
                }
                else
                {
                    List<Institution> children = this.AppUnitOfWork.InstitutionRepository.Get(new InstitutionFilter() { ParentId = institution.Id, Offset = 1, Limit = int.MaxValue });

                    foreach (Institution child in children)
                    {
                        child.Account.License = license;
                        child.Account.LicenseId = license?.Id;
                        this.AppUnitOfWork.AccountRepository.UpdateEntity(child.Account);

                        this.SetLicenseOnChildren(child, license);
                    }
                }
            }
        }

        private List<Module> SetModuleSubscriptions(Guid licenseId, List<Guid> moduleIds)
        {
            List<Module> modules = new List<Module>();

            foreach (Guid moduleId in moduleIds)
            {
                Module module = this.AppUnitOfWork.ModuleRepository.GetById(moduleId);

                if (module == null)
                {
                    throw new BusinessException($"Module with id {moduleId} doesn't exist.");
                }

                if (this.AppUnitOfWork.SubscriptionRepository.GetByLicenseId(licenseId).Where(item => item.ModuleId == moduleId).Any())
                {
                    continue;
                }

                modules.Add(module);
                this.AppUnitOfWork.SubscriptionRepository.AddEntity(new Subscription() { LicenseId = licenseId, ModuleId = moduleId });
            }

            return modules;
        }

        private void UnlockAllAccounts(Guid licenseId)
        {
            List<Account> accountsForUnlock = this.AppUnitOfWork.AccountRepository.GetByLicenseId(licenseId);

            foreach (Account account in accountsForUnlock)
            {
                account.FirstLoginWithChangedLicense = null;
                account.IsLockedDueToLicenseChange = false;
                account.IsLockedEmailSent = false;
                this.AppUnitOfWork.AccountRepository.UpdateEntity(account);
            }
        }
    }
}
