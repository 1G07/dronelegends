﻿using System;
using System.Collections.Generic;
using DroneLegends.BL.Exceptions;
using DroneLegends.BL.RequiredInterfaces;
using DroneLegends.DataModels;
using DroneLegends.DataModels.Enums;

namespace DroneLegends.BL.Services
{
    /// <summary>
    /// Institution manipulation service.
    /// </summary>
    public class InstitutionService : ServiceBase
    {
        private readonly ContextService context;
        private readonly LicenseService licenseService;

        public InstitutionService(IUnitOfWorkFactory unitOfWorkFactory, ContextService context, LicenseService licenseService)
         : base(unitOfWorkFactory.CreateUnitOfWork())
        {
            this.context = context;
            this.licenseService = licenseService;
        }

        private IUnitOfWork AppUnitOfWork
        {
            get { return (IUnitOfWork)this.UnitOfWork; }
        }

        /// <summary>
        /// Gets the list by sent filters.
        /// </summary>
        /// <param name="filter">Filter</param>
        /// <returns>List of institutions.</returns>
        public List<Institution> Get(InstitutionFilter filter)
        {
            if (this.context.CurrentAccount.Role == Role.SuperAdmin)
            {
                if (filter.Role == null)
                {
                    throw new ArgumentNullException("Role can not be null.");
                }
            }
            else
            {
                Institution currentInstitution = this.AppUnitOfWork.InstitutionRepository.GetByAccountId(this.context.CurrentAccount.Id.Value);
                filter.ParentId = currentInstitution.Id;
                filter.Role = null;
            }

            if (filter.Role == Role.Teacher)
            {
                throw new BusinessException("Institution role can not be set to teacher.");
            }

            if (filter.Limit == -1)
            {
                filter.Limit = int.MaxValue;
            }

            List<Institution> institutions = this.AppUnitOfWork.InstitutionRepository.Get(filter);

            this.PopulateUsedAccountsLicences(institutions);
            foreach (Institution institution in institutions)
            {
                this.PopulateInstitutionParents(institution);
            }

            return institutions;
        }

        /// <summary>
        /// Get total count by defined filter.
        /// </summary>
        /// <param name="filter">Filter</param>
        /// <returns>Total count</returns>
        public int GetTotalCount(InstitutionFilter filter)
        {
            return this.AppUnitOfWork.InstitutionRepository.GetTotalCount(filter);
        }

        /// <summary>
        /// Gets the institution by id.
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Institution.</returns>
        public Institution GetById(Guid id)
        {
            Institution institution = this.GetInstitution(id);

            if (institution.Account.License != null)
            {
                institution.Account.License.UsedSchoolAccounts = this.AppUnitOfWork.AccountRepository.GetUsedLicenses(institution.Account.License.Id.Value, Role.School);

                if (institution.Account.Role == Role.School || institution.Account.Role == Role.OtherBusiness)
                {
                    institution.Account.License.UsedTeacherAccounts = this.AppUnitOfWork.TeacherRepository.GetTotalCount(new TeacherFilter() { ParentId = institution.Id });
                }
                else if (institution.Account.Role == Role.SchoolDistrict)
                {
                    institution.Account.License.UsedSchoolAccountsPerDistrict = this.AppUnitOfWork.InstitutionRepository.GetTotalCount(new InstitutionFilter() { ParentId = institution.Id });
                }
            }

            return this.PopulateInstitutionParents(institution);
        }

        /// <summary>
        /// Gets the institution by id.
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Institution.</returns>
        public Institution GetSelf()
        {
            Institution currentInstitution = this.AppUnitOfWork.InstitutionRepository.GetByAccountId(this.context.CurrentAccount.Id.Value);

            if (currentInstitution == null)
            {
                throw new BusinessException("Can not find the record by provided account id.");
            }

            return this.GetById(currentInstitution.Id.Value);
        }

        /// <summary>
        /// Saves the new institution.
        /// </summary>
        /// <param name="Institution">Institution for saving.</param>
        /// <returns>Saved institution.</returns>
        public Institution Save(Institution institution)
        {
            this.ValidateInstitutionForSave(institution);
            this.SetInstitutionParent(institution);
            this.SetInstitutionLicense(institution);
            institution.Account.RegistrationStatus = RegistrationStatus.NotInvited;

            this.AppUnitOfWork.AccountRepository.AddEntity(institution.Account);
            if (institution.Contact != null)
            {
                this.AppUnitOfWork.ContactRepository.AddEntity(institution.Contact);
            }
            this.AppUnitOfWork.AddressRepository.AddEntity(institution.Address);
            institution.AccountId = institution.Account.Id.Value;
            institution.AddressId = institution.Address.Id.Value;

            this.AppUnitOfWork.InstitutionRepository.AddEntity(institution);

            this.AppUnitOfWork.SaveChanges();

            return this.PopulateInstitutionParents(institution);
        }

        /// <summary>
        /// Updates the existing institution.
        /// </summary>
        /// <param name="institution">Institution for saving.</param>
        /// <returns>Saved institution.</returns>
        public Institution Update(Institution institution)
        {
            Institution savedInstitution = this.GetInstitution(institution.Id.Value);

            if (savedInstitution.Account.Role != institution.Account.Role)
            {
                throw new BusinessException("The role can not be changed!");
            }

            if (savedInstitution.Account.Email != institution.Account.Email)
            {
                this.ValidateIfInsitutionEmailIsUnique(institution.Account.Email);
            }

            this.ValidateInstitutionParent(institution);
            this.SetInstitutionParent(institution);

            savedInstitution.Account.Email = institution.Account.Email;
            savedInstitution.Account.AvatarImage = institution.Account.AvatarImage;
            savedInstitution.Account.FirstName = institution.Account.FirstName;
            savedInstitution.Account.LastName = institution.Account.LastName;
            institution.Account = savedInstitution.Account;

            this.SetInstitutionLicense(institution, savedInstitution.ParentId);
            institution.ParentInstitution = null;

            this.SetEntityBaseFields(institution.Address, savedInstitution.Address);
            this.SetEntityBaseFields(institution, savedInstitution);
            this.AppUnitOfWork.AccountRepository.UpdateEntity(savedInstitution.Account);

            if (savedInstitution.Contact != null)
            {
                this.SetEntityBaseFields(institution.Contact, savedInstitution.Contact);
                this.AppUnitOfWork.ContactRepository.UpdateEntity(institution.Contact);
            }
            else if (institution.Contact != null)
            {
                this.AppUnitOfWork.ContactRepository.AddEntity(institution.Contact);
                institution.ContactId = institution.Contact.Id.Value;
            }

            this.AppUnitOfWork.AddressRepository.UpdateEntity(institution.Address);
            this.AppUnitOfWork.InstitutionRepository.UpdateEntity(institution);

            this.AppUnitOfWork.SaveChanges();

            return this.PopulateInstitutionParents(institution);
        }

        /// <summary>
        /// Deletes institution by id.
        /// </summary>
        /// <param name="id">Id of institution for delete.</param>
        /// <param name="parentId">Id of parent institution.</param>
        public void Delete(Guid id, Guid? parentId = null)
        {
            Institution institution = this.GetInstitution(id, parentId);

            if (institution.Account.Role == Role.School || institution.Account.Role == Role.OtherBusiness)
            {
                List<Teacher> teachersForDelete = this.AppUnitOfWork.TeacherRepository.Get(new TeacherFilter() { ParentId = institution.Id, Limit = int.MaxValue, Offset = 1 });

                foreach (Teacher t in teachersForDelete)
                {
                    this.AppUnitOfWork.AccountRepository.DeleteEntity(t.Account);
                }

                this.Save();
            }
            else
            {
                List<Institution> childInstitutionsForDelete = this.AppUnitOfWork.InstitutionRepository.Get(new InstitutionFilter() { ParentId = institution.Id, Limit = int.MaxValue, Offset = 1 });

                foreach (Institution inst in childInstitutionsForDelete)
                {
                    this.Delete(inst.Id.Value, institution.Account.Id);
                }
            }

            this.AppUnitOfWork.AddressRepository.DeleteEntity(institution.Address);
            if (institution.Contact != null)
            {
                this.AppUnitOfWork.ContactRepository.DeleteEntity(institution.Contact);
            }
            this.AppUnitOfWork.AccountRepository.DeleteEntity(institution.Account);

            this.Save();
        }

        private void PopulateUsedAccountsLicences(List<Institution> institutions)
        {
            foreach (Institution institution in institutions)
            {
                if (institution.Account.License != null)
                {
                    institution.Account.License.UsedSchoolAccounts = this.AppUnitOfWork.AccountRepository.GetUsedLicenses(institution.Account.License.Id.Value, Role.School);
                    if (institution.Account.Role == Role.School || institution.Account.Role == Role.OtherBusiness)
                    {
                        institution.Account.License.UsedTeacherAccounts = this.AppUnitOfWork.TeacherRepository.GetTotalCount(new TeacherFilter() { ParentId = institution.Id });
                    }
                    else if (institution.Account.Role == Role.SchoolDistrict)
                    {
                        institution.Account.License.UsedSchoolAccountsPerDistrict = this.AppUnitOfWork.InstitutionRepository.GetTotalCount(new InstitutionFilter() { ParentId = institution.Id });
                    }
                }
            }
        }

        private Institution PopulateInstitutionParents(Institution institution)
        {
            if (institution == null || institution.ParentId == null)
            {
                return institution;
            }

            institution.ParentInstitution = PopulateInstitutionParents(this.AppUnitOfWork.InstitutionRepository.GetById(institution.ParentId.Value));

            return institution;
        }

        private Institution GetInstitution(Guid id, Guid? parentAccountId = null)
        {
            Institution currentInstitution = this.AppUnitOfWork.InstitutionRepository.GetByAccountId(this.context.CurrentAccount.Id.Value);
            Institution institution;

            if (this.context.CurrentAccount.Role == Role.SuperAdmin || this.context.CurrentAccount.Role == Role.Teacher || (currentInstitution != null && currentInstitution.Id == id))
            {
                institution = this.AppUnitOfWork.InstitutionRepository.GetById(id);
            }
            else
            {
                institution = this.AppUnitOfWork.InstitutionRepository.GetByIdAndParentAccountId(id, parentAccountId ?? this.context.CurrentAccount.Id.Value);
            }

            if (institution == null)
            {
                throw new BusinessException("Can not find the record by provided id.");
            }

            return institution;
        }

        private void ValidateInstitutionForSave(Institution institution)
        {
            if ((this.context.CurrentAccount.Role == Role.Esa && institution.Account.Role != Role.SchoolDistrict) ||
               (this.context.CurrentAccount.Role == Role.SchoolDistrict && institution.Account.Role != Role.School))
            {
                throw new UnauthorizedException($"{this.context.CurrentAccount.Role} can not create {institution.Account.Role} records.");
            }

            if (this.context.CurrentAccount.Role != Role.SuperAdmin && institution.Account.Role == Role.School)
            {
                Institution currentInstitution = this.AppUnitOfWork.InstitutionRepository.GetByAccountId(this.context.CurrentAccount.Id.Value);
                int numberOfSavedSchools = this.AppUnitOfWork.InstitutionRepository.GetNumberOfSavedInstitutions(currentInstitution.Id.Value, Role.School);

                if (this.context.CurrentAccount.License.NumberOfSchools <= numberOfSavedSchools)
                {
                    throw new BusinessException("Number of schools exceeded for a license. Please contact Administrator to expand the license plan.");
                }
            }

            this.ValidateInstitutionParent(institution);
            this.ValidateParentLicense(institution);

            if (institution.Id != null)
            {
                throw new BusinessException("New account must not have id. Could not save new account!");
            }

            this.ValidateIfInsitutionEmailIsUnique(institution.Account.Email);
        }

        private void SetInstitutionLicense(Institution institution, Guid? savedParentId = null)
        {
            if (institution.ParentId != null)
            {
                Institution parentInstitution = this.AppUnitOfWork.InstitutionRepository.GetById(institution.ParentId.Value);
                if (parentInstitution != null)
                {
                    institution.Account.License = parentInstitution.Account.License;
                    institution.Account.LicenseId = parentInstitution.Account.LicenseId;
                }
            }
            else if (savedParentId != null)
            {
                institution.Account.License = null;
                institution.Account.LicenseId = null;
            }

            this.licenseService.SetLicenseOnChildren(institution, institution.Account.License);
        }

        private void SetInstitutionParent(Institution institution)
        {
            if (institution.Account.Role > Role.Esa && this.context.CurrentAccount.Role != Role.SuperAdmin)
            {
                Institution currentInstitution = this.AppUnitOfWork.InstitutionRepository.GetByAccountId(this.context.CurrentAccount.Id.Value);

                if (currentInstitution == null)
                {
                    throw new BusinessException("Institution for currently logged user can not be found.");
                }

                if (institution.Id != null && currentInstitution.Id == institution.Id)
                {
                    institution.ParentId = currentInstitution.ParentId;
                    institution.ParentInstitution = currentInstitution.ParentInstitution;
                }
                else
                {
                    institution.ParentId = currentInstitution.Id;
                    institution.ParentInstitution = currentInstitution;
                }
            }
        }

        private void ValidateIfInsitutionEmailIsUnique(string email)
        {
            if (this.AppUnitOfWork.AccountRepository.GetAccountByEmail(email) != null)
            {
                throw new BusinessException("Institution with email already exists.", email);
            }
        }
        private void ValidateInstitutionParent(Institution institution)
        {
            if (this.context.CurrentAccount.Role == Role.SuperAdmin && institution.ParentId != null)
            {
                Institution parentInstitution = this.AppUnitOfWork.InstitutionRepository.GetById(institution.ParentId.Value);

                if (parentInstitution == null ||
                    institution.Account.Role == Role.Esa ||
                    (institution.Account.Role == Role.SchoolDistrict && parentInstitution.Account.Role != Role.Esa) ||
                    (institution.Account.Role == Role.School && parentInstitution.Account.Role != Role.SchoolDistrict) ||
                    (institution.Account.Role == Role.OtherBusiness && parentInstitution != null)
                )
                {
                    throw new BusinessException("Parent institution is not valid.");
                }

                int numberOfSavedSchools = this.AppUnitOfWork.InstitutionRepository.GetNumberOfSavedInstitutions(parentInstitution.Id.Value, Role.School);

                if (institution.Account.Role == Role.School && (parentInstitution.Account.License == null || parentInstitution.Account.License.NumberOfSchools <= numberOfSavedSchools))
                {
                    throw new BusinessException("Parent institution license is not valid. Child institution can not be assigned. Please contact Administrator to expand the license plan.");
                }
            }
            else if (institution.ParentId != null)
            {
                throw new BusinessException("Parent ID should not be set for this role.");
            }
        }

        private void ValidateParentLicense(Institution institution)
        {
            if (this.context.CurrentAccount.Role == Role.SuperAdmin && institution.ParentId != null && institution.Account.Role == Role.School)
            {
                Institution parentInstitution = this.AppUnitOfWork.InstitutionRepository.GetById(institution.ParentId.Value);
                int numberOfSavedSchool = this.AppUnitOfWork.InstitutionRepository.GetNumberOfSavedInstitutions(institution.ParentId.Value, Role.School);

                if (parentInstitution.Account.License == null || parentInstitution.Account.License.NumberOfSchools <= numberOfSavedSchool)
                {
                    throw new BusinessException("Number of schools exceeded for a license. Please contact Administrator to expand the license plan.");
                }
            }
        }
    }
}
