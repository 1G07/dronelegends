﻿using DroneLegends.BL.Exceptions;
using DroneLegends.BL.RequiredInterfaces;
using DroneLegends.DataModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace DroneLegends.BL.Services
{
    public class AccountService : ServiceBase
    {
        private const double ResetTokenDurationInDays = 1;

        private const double InvitiationTokenDurationInDays = 7;

        private readonly ContextService context;

        private readonly MailService mailService;
        private readonly NotificationEmailsService notificationEmailsService;

        public AccountService(IUnitOfWorkFactory unitOfWorkFactory, ContextService context, MailService mailService, NotificationEmailsService notificationEmailsService)
         : base(unitOfWorkFactory.CreateUnitOfWork())
        {
            this.context = context;
            this.mailService = mailService;
            this.notificationEmailsService = notificationEmailsService;
        }

        private IUnitOfWork AppUnitOfWork
        {
            get { return (IUnitOfWork)this.UnitOfWork; }
        }

        /// <summary>
        /// Get an account by access token.
        /// </summary>
        /// <param name="accessToken">Access token.</param>
        /// <returns>Account.</returns>
        public Account GetAccount(string accessToken)
        {
            Account account = this.AppUnitOfWork.AccountRepository.GetAccount(accessToken);

            if (account == null)
            {
                throw new BusinessException("Account can not be found.");
            }

            return account;
        }

        /// <summary>
        /// Login action.
        /// </summary>
        /// <param name="email">Account email.</param>
        /// <param name="password">Account password.</param>
        /// <returns>Account.</returns>
        public Account Login(string email, string password)
        {
            Account account = this.AppUnitOfWork.AccountRepository.GetAccount(email, Encrypt(password, password));
            this.ValidateAccountAndLicenseForLogin(account);

            account.FirstLogin = account.FirstLogin == null ? DateTime.Now : account.FirstLogin;
            account.FirstLoginWithChangedLicense =
                    account.FirstLoginWithChangedLicense == null ? DateTime.Now : account.FirstLoginWithChangedLicense;

            this.LockSchoolAccountsDueToLicenseDowngrade(account);

            if (account.License.StartDate == null)
            {
                account.License.StartDate = account.FirstLogin;
                this.AppUnitOfWork.LicenseRepository.UpdateEntity(account.License);
            }

            account.AccessToken = Encrypt(account.Password, account + account.Email + DateTime.Now);
            account.RefreshToken = Encrypt(account.Password, account + account.Email + account.Role + DateTime.Now);
            account.RefreshTokenIssued = account.AccessTokenIssued = DateTime.Now;
            account.LastLogin = DateTime.Now;

            this.Update(account);

            return account;
        }

        /// <summary>
        /// Revokes a token for valid user.
        /// </summary>
        public void Logout()
        {
            this.context.CurrentAccount.AccessToken = null;
            this.context.CurrentAccount.AccessTokenIssued = null;
            this.context.CurrentAccount.RefreshToken = null;
            this.context.CurrentAccount.RefreshTokenIssued = null;

            this.Update(this.context.CurrentAccount);
        }

        /// <summary>
        /// Generates token for valid account.
        /// </summary>
        /// <param name="refreshToken">Refresh token.</param>
        /// <returns>Account.</returns>
        public Account RefreshAccessToken(string refreshToken)
        {
            Account account = this.AppUnitOfWork.AccountRepository.GetAccountByRefreshToken(refreshToken);

            this.ValidateAccountLicense(account);

            account.AccessToken = Encrypt(account.Password, account + account.Email + DateTime.Now);
            account.RefreshTokenIssued = account.AccessTokenIssued = DateTime.Now;

            this.Update(account);

            return account;
        }

        /// <summary>
        /// Generates reset token and sends Reset Password email for a valid user.
        /// </summary>
        /// <param name="email">Account email.</param>
        /// <param name="redirectUrl">Url on which the user should be redirected from email.</param>
        public void IssueResetPassword(string email, Uri redirectUrl)
        {
            Account account = this.AppUnitOfWork.AccountRepository.GetAccountByEmail(email);

            this.ValidateAccountLicense(account);

            this.GenerateResetToken(account);

            string subject = this.GetResetMailSubject();
            string message = this.GetResetMailMessage(account, redirectUrl);

            this.mailService.Send(account.Email, subject, message);

            this.Update(account);
        }

        /// <summary>
        /// Sends invite for account with link that redirects the user to the page for creating a password.
        /// </summary>
        /// <param name="email">Id of account.</param>
        /// <param name="redirectUrl">Redirect url.</param>
        public void SendInvite(string email, Uri redirectUrl)
        {
            Account account = this.AppUnitOfWork.AccountRepository.GetAccountByEmail(email);

            this.ValidateAccountLicense(account);

            account.IsResetTokenIssuedForInvitation = true;
            this.GenerateResetToken(account);

            string subject = this.GetInviteMailSubject();
            string message = this.GetInviteMailMessage(account, redirectUrl);
            this.mailService.Send(account.Email, subject, message);

            if (account.ShouldSendEmailCopy != null && account.ShouldSendEmailCopy.Value)
            {
                this.mailService.Send(this.context.CurrentAccount.Email, subject, message);
            }

            account.RegistrationStatus = DataModels.Enums.RegistrationStatus.Invited;
            this.Update(account);
        }

        /// <summary>
        /// Resets a password to a new one for a valid account.
        /// </summary>
        /// <param name="resetToken">Generated reset token.</param>
        /// <param name="newPassword">New password.</param>
        public void ResetPassword(string resetToken, string newPassword)
        {
            Account account = this.AppUnitOfWork.AccountRepository.GetAccountByResetToken(resetToken);

            this.ValidateAccountLicense(account);

            if (account.ResetTokenIssued == null || this.CalculateResetTokenExpiry(account) < DateTime.Today)
            {
                throw new BusinessException("Reset token is not valid or expired.");
            }

            account.Password = Encrypt(newPassword, newPassword);
            account.ResetToken = null;
            account.RegistrationStatus = DataModels.Enums.RegistrationStatus.Registered;

            this.Update(account);
        }

        /// <summary>
        /// Updates a password for a valid user.
        /// </summary>
        /// <param name="oldPassword">Old password.</param>
        /// <param name="newPassword">New password.</param>
        public void UpdatePassword(string oldPassword, string newPassword)
        {
            if (this.context.CurrentAccount.Password != Encrypt(oldPassword, oldPassword))
            {
                throw new BusinessException("Invalid old password.");
            }

            this.context.CurrentAccount.Password = Encrypt(newPassword, newPassword);

            this.Update(this.context.CurrentAccount);
        }

        public void UpdatePasswordForAdmin(string newPassword)
        {
            Account account = this.AppUnitOfWork.AccountRepository.GetAccountByEmail("scott@dronelegends.com");

            if (account != null)
            {
                account.Password = Encrypt(newPassword, newPassword);
                account.ResetToken = null;

                this.Update(account);
            }
        }
        
        public void Unsubscribe(string unsubscribeToken)
        {
            Account account = this.AppUnitOfWork.AccountRepository.GetAccountByUnsubscribeToken(unsubscribeToken);

            if (account == null)
            {
                throw new BusinessException("User can not be found in the system.");
            }

            account.UnsubscribeToken = null;
            account.IsSubscribedToMails = false;
            this.Update(account);
        }

        public void Lock(string email)
        {
            Account account = this.AppUnitOfWork.AccountRepository.GetAccountByEmail(email);

            if (account == null)
            {
                throw new BusinessException($"Account with email '{email}' does not exist.");
            }

            if (account.Role != DataModels.Enums.Role.School)
            {
                throw new BusinessException("Only school accounts can be locked.");
            }


            if ((this.context.CurrentAccount.Role != DataModels.Enums.Role.SuperAdmin) && 
                (account == null || account.License == null || this.context.CurrentAccount.License == null || account.License.Id != this.context.CurrentAccount.License.Id))
            {
                throw new BusinessException("Account for locking has different license than the lock invoker.");
            }
           
            account.IsLockedDueToLicenseChange = true;
            account.FirstLoginWithChangedLicense = null;

            this.notificationEmailsService.SendEmailForLockedAccount(account);
            account.IsLockedEmailSent = true;
            
            this.Update(account);
        }

        public void Unlock(string email)
        {
            Account account = this.AppUnitOfWork.AccountRepository.GetAccountByEmail(email);

            if (account == null)
            {
                throw new BusinessException($"Account with email '{email}' does not exist.");
            }

            if (account.Role != DataModels.Enums.Role.School)
            {
                throw new BusinessException("Only school accounts can be locked.");
            }

            if ((this.context.CurrentAccount.Role != DataModels.Enums.Role.SuperAdmin) &&
                (account == null || account.License == null || this.context.CurrentAccount.License == null || 
                account.License.Id != this.context.CurrentAccount.License.Id))
            {
                throw new BusinessException("Account for unlocking has different license than the lock invoker.");
            }

            List<Account> unlockedSchoolsUnderLicense = this.AppUnitOfWork.AccountRepository.GetByLicenseId(account.License.Id.Value)
                   .Where(a => a.Role == DataModels.Enums.Role.School && a.IsLockedDueToLicenseChange == false).ToList();

            if (account.Role == DataModels.Enums.Role.School && unlockedSchoolsUnderLicense.Count == account.License.NumberOfSchools)
            {
                throw new BusinessException("Account can not be unlocked because maximum number of unlocked accounts is already reached.");
            }

            account.IsLockedDueToLicenseChange = false;
            account.IsLockedEmailSent = false;
            account.FirstLoginWithChangedLicense = DateTime.Now;

            this.Update(account);
        }

        /// <summary>
        /// Does the encryption.
        /// </summary>
        /// <param name="secret">Secret.</param>
        /// <param name="encriptionText">Encryption text.</param>
        /// <returns>Encrypted text.</returns>
        public static string Encrypt(string secret, string encriptionText)
        {
            HMACSHA256 hashFunction = new HMACSHA256(ASCIIEncoding.ASCII.GetBytes(secret));
            byte[] generatedHash = hashFunction.ComputeHash(ASCIIEncoding.ASCII.GetBytes(encriptionText));

            return BitConverter.ToString(generatedHash).Replace("-", string.Empty, StringComparison.Ordinal).ToLowerInvariant();
        }

        private void ValidateAccountAndLicenseForLogin(Account account)
        {
            if (account == null)
            {
                throw new BusinessException("Invalid credentials.");
            }

            if (account.License == null)
            {
                throw new BusinessException("License is not set for this account. Please contact the support to renew the license.");
            }

            if (account.FirstLogin != null && (account.License.ExpiryDate == null || account.License.ExpiryDate < DateTime.Now))
            {
                throw new BusinessException("License is expired for this account. Please contact the support to renew the license.");
            }

            if (account.IsLockedDueToLicenseChange)
            {
                string subject = this.GetLockedAccountMailSubject();
                string message = this.GetLockedMailMessage(account);
                this.mailService.Send(account.Email, subject, message);
                this.notificationEmailsService.SendEmailForLockedAccount(account);

                throw new BusinessException("Account is locked due to license downgrade.");
            }
        }
        private void LockSchoolAccountsDueToLicenseDowngrade(Account account)
        {
            if (account.Role == DataModels.Enums.Role.School)
            {
                List<Account> schoolsUnderLicense = this.AppUnitOfWork.AccountRepository.GetByLicenseId(account.License.Id.Value)
                    .Where(a => a.Role == DataModels.Enums.Role.School).ToList();

                int numberOfUnlockedSchools = schoolsUnderLicense.Where(a => a.FirstLoginWithChangedLicense != null && a.IsLockedDueToLicenseChange == false).Count();

                if (account.License.NumberOfSchools == numberOfUnlockedSchools + 1)
                {
                    foreach (Account schoolAccount in schoolsUnderLicense
                        .Where(s => s.FirstLoginWithChangedLicense == null && s.Id != account.Id && s.IsLockedDueToLicenseChange == false))
                    {
                        schoolAccount.IsLockedDueToLicenseChange = true;
                        this.AppUnitOfWork.AccountRepository.UpdateEntity(schoolAccount);
                    }
                }
            }
        }

        private void GenerateResetToken(Account account)
        {
            string resetToken = Encrypt(account.Password ?? string.Empty, account + account.Email + DateTime.Now);
            account.ResetToken = resetToken;
            account.ResetTokenIssued = DateTime.Now;
        }

        private DateTime CalculateResetTokenExpiry(Account account)
        {
            DateTime result = account.ResetTokenIssued.Value.AddDays(ResetTokenDurationInDays);

            if (account.IsResetTokenIssuedForInvitation)
            {
                result = account.ResetTokenIssued.Value.AddDays(InvitiationTokenDurationInDays);
            }

            return result;
        }

        private void ValidateAccountLicense(Account account)
        {
            if (account == null)
            {
                throw new BusinessException("Account can not be found.");
            }

            if (account.License == null || (account.License.ExpiryDate != null && account.License.ExpiryDate < DateTime.Now))
            {
                throw new BusinessException("License is expired for this account. Please contact the support to renew the license.");
            }
        }

        private void Update(Account account)
        {
            this.AppUnitOfWork.AccountRepository.UpdateEntity(account);
            this.Save();
        }

        private string GetResetMailSubject()
        {
            return "Drone Legends Education Portal - password reset request";
        }

        private string GetInviteMailSubject()
        {
            return "Drone Legends Portal Registration Confirmation. Please Confirm Email.";
        }

        private string GetResetMailMessage(Account account, Uri redirectUrl)
        {
            string url = redirectUrl.ToString().Replace(":token", account.ResetToken, StringComparison.Ordinal);

            string result;

            using (StreamReader sourceReader = File.OpenText(Path.Combine(this.mailService.Configuration.TemplateDirectory.ToString(), "reset-password.html")))
            {
                result = sourceReader.ReadToEnd();
            }

            return result.Replace("{url}", url, StringComparison.Ordinal)
                .Replace("{firstName}", account.FirstName, StringComparison.Ordinal)
                .Replace("{lastName}", account.LastName, StringComparison.Ordinal);
        }

        private string GetInviteMailMessage(Account account, Uri redirectUrl)
        {
            string url = redirectUrl.ToString().Replace(":token", account.ResetToken, StringComparison.Ordinal);

            string templateName;
            string parentInsitutionName = string.Empty;

            if (this.context.CurrentAccount.Role == DataModels.Enums.Role.SuperAdmin)
            {
                templateName = "admin-invitation.html";
            }
            else
            {
                Institution institution = this.AppUnitOfWork.InstitutionRepository.GetByAccountId(this.context.CurrentAccount.Id.Value);
                parentInsitutionName = institution.ParentInstitution != null ? institution.ParentInstitution.Name : string.Empty;
                templateName = "institution-invitation.html";
            }

            string result;

            using (StreamReader sourceReader = File.OpenText(Path.Combine(this.mailService.Configuration.TemplateDirectory.ToString(), templateName)))
            {
                result = sourceReader.ReadToEnd();
            }

            return result
                .Replace("{url}", url, StringComparison.Ordinal)
                .Replace("{firstName}", account.FirstName, StringComparison.Ordinal)
                .Replace("{lastName}", account.LastName, StringComparison.Ordinal)
                .Replace("{ownerName}", parentInsitutionName, StringComparison.Ordinal);
        }

        private string GetLockedAccountMailSubject()
        {
            return string.Empty;
        }

        private string GetLockedMailMessage(Account account)
        {
            return string.Empty;
        }
    }
}
