﻿using System;
using System.Collections.Generic;
using System.Linq;
using DroneLegends.BL.Exceptions;
using DroneLegends.BL.RequiredInterfaces;
using DroneLegends.DataModels;
using DroneLegends.DataModels.Enums;

namespace DroneLegends.BL.Services
{
    /// <summary>
    /// Super admin manipulation service.
    /// </summary>
    public class SuperAdminService : ServiceBase
    {
        private readonly ContextService context;

        private const string OwnerSuperAdminEmail = "scott@dronelegends.com";

        public SuperAdminService(IUnitOfWorkFactory unitOfWorkFactory, ContextService context)
         : base(unitOfWorkFactory.CreateUnitOfWork())
        {
            this.context = context;
        }

        private IUnitOfWork AppUnitOfWork
        {
            get { return (IUnitOfWork)this.UnitOfWork; }
        }

        /// <summary>
        /// Gets the list by sent filters.
        /// </summary>
        /// <param name="filter">Filter</param>
        /// <returns>List of super admins.</returns>
        public List<SuperAdmin> Get(SuperAdminFilter filter)
        {
            if (filter.Limit == -1)
            {
                filter.Limit = int.MaxValue;
            }
            
            return this.AppUnitOfWork.SuperAdminRepository.Get(filter).Where(sa => sa.AccountId != this.context.CurrentAccount.Id).ToList();
        }

        /// <summary>
        /// Get total count by defined filter.
        /// </summary>
        /// <param name="filter">Filter</param>
        /// <returns>Total count</returns>
        public int GetTotalCount(SuperAdminFilter filter)
        {
            return this.AppUnitOfWork.SuperAdminRepository.GetTotalCount(filter) - 1;
        }

        /// <summary>
        /// Gets the super admin by id.
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Super admin.</returns>
        public SuperAdmin GetById(Guid id)
        {
            return this.AppUnitOfWork.SuperAdminRepository.GetById(id);
        }

        /// <summary>
        /// Gets self.
        /// </summary>
        /// <returns>Super admin.</returns>
        public SuperAdmin GetSelf()
        {
            SuperAdmin currentSuperAdmin = this.AppUnitOfWork.SuperAdminRepository.GetByAccountId(this.context.CurrentAccount.Id.Value);

            if (currentSuperAdmin == null)
            {
                throw new BusinessException("Can not find the record by provided account id.");
            }

            return this.GetById(currentSuperAdmin.Id.Value);
        }

        /// <summary>
        /// Saves the new super admin.
        /// </summary>
        /// <param name="superAdmin">Super admin for saving.</param>
        /// <returns>Saved super admin.</returns>
        public SuperAdmin Save(SuperAdmin superAdmin)
        {
            this.ValidateSuperAdminForSave(superAdmin);
            superAdmin.Account.License = new License() {
                DurationInMonths = 9574,
                NumberOfSchools = Int32.MaxValue,
                NumberOfTeachers = Int32.MaxValue,
            };
            superAdmin.Account.Role = Role.SuperAdmin;
            this.AppUnitOfWork.LicenseRepository.AddEntity(superAdmin.Account.License);
            superAdmin.Account.LicenseId = superAdmin.Account.License.Id;
            superAdmin.Account.IsResetTokenIssuedForInvitation = false;
            this.AppUnitOfWork.AccountRepository.AddEntity(superAdmin.Account);
            superAdmin.AccountId = superAdmin.Account.Id.Value;

            this.AppUnitOfWork.SuperAdminRepository.AddEntity(superAdmin);

            this.AppUnitOfWork.SaveChanges();

            return superAdmin;
        }

        /// <summary>
        /// Updates the existing super admin.
        /// </summary>
        /// <param name="superAdmin">Super admin for saving.</param>
        /// <returns>Saved super admin.</returns>
        public SuperAdmin Update(SuperAdmin superAdmin)
        {
            SuperAdmin savedSuperAdmin = this.GetById(superAdmin.Id.Value);

            savedSuperAdmin.Phone = superAdmin.Phone;
            
            if (savedSuperAdmin.Account.Email != superAdmin.Account.Email)
            {
                throw new BusinessException("Account email can not be changed.");
            }

            savedSuperAdmin.Account.AvatarImage = superAdmin.Account.AvatarImage;
            savedSuperAdmin.Account.Email = superAdmin.Account.Email;
            savedSuperAdmin.Account.FirstName = superAdmin.Account.FirstName;
            savedSuperAdmin.Account.LastName = superAdmin.Account.LastName;
            superAdmin.Account = savedSuperAdmin.Account;
            this.SetEntityBaseFields(superAdmin, savedSuperAdmin);
            this.AppUnitOfWork.AccountRepository.UpdateEntity(savedSuperAdmin.Account);
            this.AppUnitOfWork.SuperAdminRepository.UpdateEntity(superAdmin);

            this.AppUnitOfWork.SaveChanges();

            return superAdmin;
        }

        /// <summary>
        /// Deletes super admin by id.
        /// </summary>
        /// <param name="id">Id of super admin for delete.</param>
        public void Delete(Guid id)
        {
            SuperAdmin sa = this.AppUnitOfWork.SuperAdminRepository.GetById(id);

            if (sa == null)
            {
                throw new BusinessException("User with the provided id can not be found.");
            }

            if (sa.AccountId == this.context.CurrentAccount.Id)
            {
                throw new BusinessException("Currenly logged in account can not delete itself.");
            }

            if (sa.Account.Email == OwnerSuperAdminEmail)
            {
                throw new BusinessException("Owner super admin can not be deleted!");
            }

            this.AppUnitOfWork.AccountRepository.DeleteEntity(sa.Account);

            this.Save();
        }

        private void ValidateSuperAdminForSave(SuperAdmin sa)
        {
            if (sa.Id != null)
            {
                throw new BusinessException("New account must not have id. Could not save new account!");
            }

            if (this.AppUnitOfWork.AccountRepository.GetAccountByEmail(sa.Account.Email) != null)
            {
                throw new BusinessException("Super admin with email already exists.", sa.Account.Email);
            }
        }
    }
}
