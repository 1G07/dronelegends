﻿using System;

namespace DroneLegends.BL.DomainModels
{
    /// <summary>
    /// Mail configuration.
    /// </summary>
    public class MailConfiguration
    {
        /// <summary>
        /// Gets or sets Sender.
        /// </summary>
        public string Sender { get; set; }

        /// <summary>
        /// Gets or sets Smtp server.
        /// </summary>
        public string SmtpServer { get; set; }

        /// <summary>
        /// Gets or sets Port.
        /// </summary>
        public int Port { get; set; }

        /// <summary>
        /// Gets or sets Username.
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// Gets or sets Password.
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is SSL enabled.
        /// </summary>
        public bool EnableSsl { get; set; }

        /// <summary>
        /// Gets or sets TemplateDirectory.
        /// </summary>
        public Uri TemplateDirectory { get; set; }
    }
}
