﻿using DroneLegends.DataModels;
using System.Collections.Generic;

namespace DroneLegends.BL.DomainModels
{
    public class LicenseDO
    {
        public LicenseDO(License license, List<Module> modules)
        {
            License = license;
            Modules = modules;
        }

        public License License { get; }

        public List<Module> Modules { get; }
    }
}
