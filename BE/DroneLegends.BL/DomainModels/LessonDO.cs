﻿using System.Collections.Generic;
using DroneLegends.DataModels;

namespace DroneLegends.BL.DomainModels
{
    public class LessonDO
    {
        public LessonDO(Lesson lesson, List<DownloadableResource> downloadableResources, List<EmbededResource> embededResources)
        {
            Lesson = lesson;
            DownloadableResources = downloadableResources;
            EmbededResources = embededResources;
        }

        public Lesson Lesson { get; }

        public List<DownloadableResource> DownloadableResources { get; }
        
        public List<EmbededResource> EmbededResources { get; }
    }
}
