﻿using System;

namespace DroneLegends.BL.Exceptions
{
    public abstract class ValidationException : Exception
    {
        protected ValidationException()
        {
        }

        protected ValidationException(string message)
            : base(message)
        {
        }

        protected ValidationException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected ValidationException(int errorCode, string message)
            : base(message)
        {
            this.ErrorCode = errorCode;
        }

        protected ValidationException(int errorCode, string message, Exception innerException)
            : base(message, innerException)
        {
            this.ErrorCode = errorCode;
        }

        public int ErrorCode { get; private set; }
    }
}
