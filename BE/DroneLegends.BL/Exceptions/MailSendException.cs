﻿using System;

namespace DroneLegends.BL.Exceptions
{
    public class MailSendException : BusinessException
    {
        public MailSendException(string message)
            : base(message)
        {
        }

        public MailSendException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        public MailSendException(string message, Exception innerException, string parameter)
            : base(message, innerException)
        {
            this.Parameter = parameter;
        }

        public MailSendException()
            : base()
        {
        }
    }
}
