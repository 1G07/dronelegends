﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DroneLegends.BL.Exceptions
{
    /// <summary>
    /// Unauthenticated Exception.
    /// </summary>
    public class UnauthenticatedException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UnauthenticatedException"/> class.
        /// </summary>
        /// <param name="message">Exception message.</param>
        public UnauthenticatedException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UnauthenticatedException"/> class.
        /// </summary>
        /// <param name="message">Exception message.</param>
        /// <param name="innerException">Inner exception.</param>
        public UnauthenticatedException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UnauthenticatedException"/> class.
        /// </summary>
        public UnauthenticatedException()
        {
        }
    }
}
