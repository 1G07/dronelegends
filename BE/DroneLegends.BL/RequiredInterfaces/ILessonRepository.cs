﻿using System;
using System.Collections.Generic;
using DroneLegends.DataAccess.Common.Interfaces.Generic;
using DroneLegends.DataModels;

namespace DroneLegends.BL.RequiredInterfaces
{
    /// <summary>
    /// Lesson repository.
    /// </summary>
    public interface ILessonRepository : IRepository<Lesson>
    {
        /// <summary>
        /// Gets Lessons for module id.
        /// </summary>
        /// <param name="moduleId">Module Id.</param>
        /// <returns>List of lessons.</returns>
        List<Lesson> GetForModule(Guid moduleId);

        /// <summary>
        /// Gets Lesson by lessonId.
        /// </summary>
        /// <param name="moduleId">Lesson Id.</param>
        /// <returns>Lesson.</returns>
        Lesson GetLessonDetails(Guid lessonId);
    }
}
