﻿using System;
using System.Collections.Generic;
using DroneLegends.DataAccess.Common.Interfaces.Generic;
using DroneLegends.DataModels;

namespace DroneLegends.BL.RequiredInterfaces
{
    /// <summary>
    /// Teacher repository.
    /// </summary>
    public interface ITeacherRepository : IRepository<Teacher>
    {
        /// <summary>
        /// Gets Teachers by filter.
        /// </summary>
        /// <param name="filter">Filter.</param>
        /// <returns>Teacher.</returns>
        List<Teacher> Get(TeacherFilter filter);

        /// <summary>
        /// Gets count of teachers by filter.
        /// </summary>
        /// <param name="filter">Filter.</param>
        /// <returns>Teacher count.</returns>
        int GetTotalCount(TeacherFilter filter);

        /// <summary>
        /// Gets Teacher by account id.
        /// </summary>
        /// <param name="accountId">Account id.</param>
        /// <returns>Teacher</returns>
        Teacher GetByAccountId(Guid accountId);

        /// <summary>
        /// Gets Teacher by account id.
        /// </summary>
        /// <param name="id">Id.</param>
        /// <param name="parentAccountId">Parent account id.</param>
        /// <returns>Teacher</returns>
        Teacher GetByIdAndParentAccountId(Guid id, Guid? parentAccountId);

        /// <summary>
        /// Gets number of saved teachers by parent id.
        /// </summary>
        /// <param name="parentId">Parent institution id.</param>
        /// <returns>Teacher</returns>
        int GetNumberOfSavedTeachers(Guid parentId);
    }
}
