﻿using System;
using System.Collections.Generic;
using DroneLegends.DataAccess.Common.Interfaces.Generic;
using DroneLegends.DataModels;

namespace DroneLegends.BL.RequiredInterfaces
{
    /// <summary>
    /// Account repository.
    /// </summary>
    public interface IEmbededResourceRepository : IRepository<EmbededResource>
    {
        /// <summary>
        /// Gets EmbededResource by lessonId.
        /// </summary>
        /// <param name="licenseId">Lesson Id.</param>
        /// <returns>List of embeded resources.</returns>
        List<EmbededResource> GetByLessonId(Guid lessonId);
    }
}
