﻿using System;
using System.Collections.Generic;
using DroneLegends.DataAccess.Common.Interfaces.Generic;
using DroneLegends.DataModels;
using DroneLegends.DataModels.Enums;

namespace DroneLegends.BL.RequiredInterfaces
{
    /// <summary>
    /// Account repository.
    /// </summary>
    public interface IAccountRepository : IRepository<Account>
    {
        /// <summary>
        /// Gets account by token.
        /// </summary>
        /// <param name="accessToken">Access token.</param>
        /// <returns>Account.</returns>
        Account GetAccount(string accessToken);

        /// <summary>
        /// Gets account by email and password.
        /// </summary>
        /// <param name="accessToken">Access token.</param>
        /// <returns>Account.</returns>
        Account GetAccount(string email, string password);

        /// <summary>
        /// Gets account by refresh token.
        /// </summary>
        /// <param name="refreshToken">Refresh token.</param>
        /// <returns>User.</returns>
        Account GetAccountByRefreshToken(string refreshToken);

        /// <summary>
        /// Gets account by refresh token.
        /// </summary>
        /// <param name="unsubscribeToken">Unsubscribe token.</param>
        /// <returns>User.</returns>
        Account GetAccountByUnsubscribeToken(string unsubscribeToken);
        
        /// <summary>
        /// Gets account by email.
        /// </summary>
        /// <param name="email">Email.</param>
        /// <returns>Account.</returns>
        Account GetAccountByEmail(string email);
        
        /// <summary>
        /// Gets account by role.
        /// </summary>
        /// <param name="role">Role.</param>
        /// <returns>List of accounts.</returns>
        List<Account> GetAccountsByRole(Role role);

        /// <summary>
        /// Gets account by reset token.
        /// </summary>
        /// <param name="resetToken">Reset token.</param>
        /// <returns>Account.</returns>
        Account GetAccountByResetToken(string resetToken);

        /// <summary>
        /// Gets used Licenses number for account.
        /// </summary>
        /// <param name="accountId">Account id.</param>
        /// <param name="role">Role.</param>
        /// <returns>Number of used licenses.</returns>
        int GetUsedLicenses(Guid accountId, Role role);

        /// <summary>
        /// Gets accounts associated with the license.
        /// </summary>
        /// <param name="licenseId">License id.</param>
        /// <returns>List of accounts.</returns>
        List<Account> GetByLicenseId(Guid licenseId);
    }
}
