﻿using System;
using DroneLegends.DataAccess.Common.Interfaces.Generic;
using DroneLegends.DataModels;
using IUnitOfWorkBase = DroneLegends.DataAccess.Common.Interfaces.IUnitOfWork;

namespace DroneLegends.BL.RequiredInterfaces
{
    public interface IUnitOfWork : IDisposable, IUnitOfWorkBase
    {
        IAccountRepository AccountRepository { get; }
        
        IInstitutionRepository InstitutionRepository { get; }
        
        IRepository<Contact> ContactRepository { get; }
        
        IRepository<Address> AddressRepository { get; }
        
        IRepository<License> LicenseRepository { get; }
        
        ITeacherRepository TeacherRepository { get; }

        IRepository<Module> ModuleRepository { get; }

        ILessonRepository LessonRepository { get; }
        
        ISubscriptionRepository SubscriptionRepository { get; }
        
        IDownloadableResourceRepository DownloadableResourceRepository { get; }

        IEmbededResourceRepository EmbededResourceRepository { get; }

        IRepository<Ticket> TicketRepository { get; }

        ISuperAdminRepository SuperAdminRepository { get; }
    }
}
