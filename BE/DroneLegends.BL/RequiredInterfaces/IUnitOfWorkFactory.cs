﻿namespace DroneLegends.BL.RequiredInterfaces
{
    public interface IUnitOfWorkFactory
    {
        IUnitOfWork CreateUnitOfWork();
    }
}
