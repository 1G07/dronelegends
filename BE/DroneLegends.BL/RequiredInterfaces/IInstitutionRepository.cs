﻿using System;
using System.Collections.Generic;
using DroneLegends.DataAccess.Common.Interfaces.Generic;
using DroneLegends.DataModels;
using DroneLegends.DataModels.Enums;

namespace DroneLegends.BL.RequiredInterfaces
{
    /// <summary>
    /// Institution repository.
    /// </summary>
    public interface IInstitutionRepository : IRepository<Institution>
    {
        /// <summary>
        /// Gets Institutions by filter.
        /// </summary>
        /// <param name="filter">Filter.</param>
        /// <returns>Institution.</returns>
        List<Institution> Get(InstitutionFilter filter);
        
        /// <summary>
        /// Gets count of institutions by filter.
        /// </summary>
        /// <param name="filter">Filter.</param>
        /// <returns>Institution count.</returns>
        int GetTotalCount(InstitutionFilter filter);

        /// <summary>
        /// Gets institution by account id.
        /// </summary>
        /// <param name="accountId">Account id.</param>
        /// <returns>Institution</returns>
        Institution GetByAccountId(Guid accountId);

        /// <summary>
        /// Gets institution by account id.
        /// </summary>
        /// <param name="accountId">Account id.</param>
        /// <param name="id">Parent account id.</param>
        /// <returns>Institution</returns>
        Institution GetByIdAndParentAccountId(Guid id, Guid? parentAccountId);

        /// <summary>
        /// Gets number of saved institution by parent id and role.
        /// </summary>
        /// <param name="parentId">Parent institution id.</param>
        /// <param name="role">Role.</param>
        /// <returns>Institution</returns>
        int GetNumberOfSavedInstitutions(Guid parentId, Role role);
    }
}
