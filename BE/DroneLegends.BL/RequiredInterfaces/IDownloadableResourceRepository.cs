﻿using System;
using System.Collections.Generic;
using DroneLegends.DataAccess.Common.Interfaces.Generic;
using DroneLegends.DataModels;

namespace DroneLegends.BL.RequiredInterfaces
{
    /// <summary>
    /// Account repository.
    /// </summary>
    public interface IDownloadableResourceRepository : IRepository<DownloadableResource>
    {
        /// <summary>
        /// Gets DownloadableResource by lessonId.
        /// </summary>
        /// <param name="licenseId">Lesson Id.</param>
        /// <returns>List of downloadable resource.</returns>
        List<DownloadableResource> GetByLessonId(Guid lessonId);
    }
}
