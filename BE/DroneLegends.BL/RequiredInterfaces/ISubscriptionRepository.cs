﻿using System;
using System.Collections.Generic;
using DroneLegends.DataAccess.Common.Interfaces.Generic;
using DroneLegends.DataModels;

namespace DroneLegends.BL.RequiredInterfaces
{
    /// <summary>
    /// Account repository.
    /// </summary>
    public interface ISubscriptionRepository : IRepository<Subscription>
    {
        /// <summary>
        /// Gets Subscription by licenseId.
        /// </summary>
        /// <param name="licenseId">License Id.</param>
        /// <returns>Subscription.</returns>
        List<Subscription> GetByLicenseId(Guid licenseId);
    }
}
