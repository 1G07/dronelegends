﻿using System;
using System.Collections.Generic;
using DroneLegends.DataAccess.Common.Interfaces.Generic;
using DroneLegends.DataModels;

namespace DroneLegends.BL.RequiredInterfaces
{
    /// <summary>
    /// Super admin repository.
    /// </summary>
    public interface ISuperAdminRepository : IRepository<SuperAdmin>
    {
        /// <summary>
        /// Gets super admins by filter.
        /// </summary>
        /// <param name="filter">Filter.</param>
        /// <returns>Super admins.</returns>
        List<SuperAdmin> Get(SuperAdminFilter filter);

        /// <summary>
        /// Gets count of super admins by filter.
        /// </summary>
        /// <param name="filter">Filter.</param>
        /// <returns>Super admin count.</returns>
        int GetTotalCount(SuperAdminFilter filter);

        /// <summary>
        /// Gets super admin by account id.
        /// </summary>
        /// <param name="accountId">Account id.</param>
        /// <returns>Super admin.</returns>
        SuperAdmin GetByAccountId(Guid accountId);
    }
}
