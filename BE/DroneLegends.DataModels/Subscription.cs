﻿using System;
using DroneLegends.DataAccess.Common;

namespace DroneLegends.DataModels
{
    public class Subscription : EntityBase
    {
        /// <summary>
        /// Gets or sets LicenseId.
        /// </summary>
        public Guid LicenseId { get; set; }

        /// <summary>
        /// Gets or sets License.
        /// </summary>
        public License License { get; set; }

        /// <summary>
        /// Gets or sets ModuleId.
        /// </summary>
        public Guid ModuleId { get; set; }

        /// <summary>
        /// Gets or sets Module.
        /// </summary>
        public Module Module { get; set; }
    }
}
