﻿using System;
using DroneLegends.DataAccess.Common;

namespace DroneLegends.DataModels
{
    public class Lesson : EntityBase
    {
        /// <summary>
        /// Gets or sets Name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets FolderName.
        /// </summary>
        public string FolderName { get; set; }

        /// <summary>
        /// Gets or sets Description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets Image.
        /// </summary>
        public string Image { get; set; }

        /// <summary>
        /// Gets or sets Introduction.
        /// </summary>
        public string Introduction { get; set; }

        /// <summary>
        ///  Gets or sets Position of a lesson in module.
        /// </summary>
        public int Position { get; set; }

        /// <summary>
        /// Gets or sets PresentationCoverImage.
        /// </summary>
        public string PresentationCoverImage { get; set; }

        /// <summary>
        ///  Gets or sets Presentation hash.
        /// </summary>
        public string PresentationHash { get; set; }

        /// <summary>
        ///  Gets or sets PresentationHashExpiry.
        /// </summary>
        public DateTime? PresentationHashExpiry { get; set; }

        /// <summary>
        /// Gets or sets ModuleId.
        /// </summary>
        public Guid ModuleId { get; set; }

        /// <summary>
        /// Gets or sets Module.
        /// </summary>
        public Module Module { get; set; }
    }
}
