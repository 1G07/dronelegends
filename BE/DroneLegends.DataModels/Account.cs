﻿using DroneLegends.DataAccess.Common;
using DroneLegends.DataModels.Enums;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace DroneLegends.DataModels
{
    /// <summary>
    /// Account data model.
    /// </summary>
    public class Account : EntityBase
    {
        public const double AccessTokenDurationInHours = 2;

        /// <summary>
        /// Gets or sets Email.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets Password.
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets AvatarImage.
        /// </summary>
        public string AvatarImage { get; set; }

        /// <summary>
        /// Gets or sets FirstName.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets LastName.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets AccessToken.
        /// </summary>
        public string AccessToken { get; set; }

        /// <summary>
        /// Gets or sets AccessTokenIssued.
        /// </summary>
        public DateTime? AccessTokenIssued { get; set; }

        /// <summary>
        /// Gets or sets ResetToken.
        /// </summary>
        public string ResetToken { get; set; }

        /// <summary>
        /// Gets or sets ResetTokenIssued.
        /// </summary>
        public DateTime? ResetTokenIssued { get; set; }

        /// <summary>
        /// Gets or sets ResetTokenIssued.
        /// </summary>
        public DateTime? LastLogin { get; set; }

        /// <summary>
        /// Gets or sets FirstLogin.
        /// </summary>
        public DateTime? FirstLogin { get; set; }

        /// <summary>
        /// Gets or sets RefreshToken.
        /// </summary>
        public string RefreshToken { get; set; }

        /// <summary>
        /// Gets or sets ResetToken.
        /// </summary>
        public DateTime? RefreshTokenIssued { get; set; }

        /// <summary>
        /// Is reset token issued for invitation.
        /// </summary>
        public bool IsResetTokenIssuedForInvitation { get; set; }

        /// <summary>
        /// Gets or sets Role.
        /// </summary>
        public Role Role { get; set; }

        /// <summary>
        /// Gets or sets RegistrationStatus.
        /// </summary>
        public RegistrationStatus RegistrationStatus { get; set; }

        /// <summary>
        /// Gets or sets IsSubscribedToMails.
        /// </summary>
        public bool IsSubscribedToMails { get; set; }

        /// <summary>
        /// Gets or sets UnsubscribeToken.
        /// </summary>
        public string UnsubscribeToken { get; set; }

        /// <summary>
        /// Gets or sets ShouldSendEmailCopy.
        /// </summary>
        public bool? ShouldSendEmailCopy { get; set; }

        /// <summary>
        /// Gets or sets IsLockedDueToLicenseChange.
        /// </summary>
        public bool IsLockedDueToLicenseChange { get; set; }

        /// <summary>
        /// Gets or sets FirstLoginWithDowngradedLicense.
        /// </summary>
        public DateTime? FirstLoginWithChangedLicense { get; set; }

        /// <summary>
        /// Get or sets value indicatin whether is locked email sent.
        /// </summary>
        public bool IsLockedEmailSent { get; set; }

        /// <summary>
        /// Gets or sets LicenseId.
        /// </summary>
        public Guid? LicenseId { get; set; }

        /// <summary>
        /// Gets or sets License.
        /// </summary>
        public License License  { get; set; }

        [NotMapped]
        public long AccessTokenExpiryTime
        {
            get
            {
                return this.AccessTokenIssued != null ?
                ((DateTimeOffset)this.AccessTokenIssued.Value.AddHours(AccessTokenDurationInHours)).ToUnixTimeSeconds() : DateTimeOffset.Now.ToUnixTimeSeconds();
            }
        }
    }
}
