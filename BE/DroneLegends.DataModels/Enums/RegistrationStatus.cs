﻿using System.Runtime.Serialization;

namespace DroneLegends.DataModels.Enums
{
    public enum RegistrationStatus
    {
        [EnumMember(Value = "notInvited")]
        NotInvited = 1,

        [EnumMember(Value = "invited")]
        Invited = 2,

        [EnumMember(Value = "registered")]
        Registered = 3,
    }
}
