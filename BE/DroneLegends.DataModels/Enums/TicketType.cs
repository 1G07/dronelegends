﻿using System.Runtime.Serialization;

namespace DroneLegends.DataModels.Enums
{
    public enum TicketType
    {
        [EnumMember(Value = "question")]
        Question = 1,

        [EnumMember(Value = "request")]
        Request = 2,

        [EnumMember(Value = "bug")]
        Bug = 3
    }
}
