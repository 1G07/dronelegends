﻿using System.Runtime.Serialization;

namespace DroneLegends.DataModels.Enums
{
    public enum Role
    {
        [EnumMember(Value = "superAdmin")]
        SuperAdmin = 1,

        [EnumMember(Value = "esa")]
        Esa = 2,

        [EnumMember(Value = "schoolDistrict")]
        SchoolDistrict = 3,

        [EnumMember(Value = "school")]
        School = 4,

        [EnumMember(Value = "otherBusiness")]
        OtherBusiness = 5,

        [EnumMember(Value = "teacher")]
        Teacher = 6
    }
}
