﻿using DroneLegends.DataAccess.Common;
using System;

namespace DroneLegends.DataModels
{
    /// <summary>
    /// SuperAdmin data model.
    /// </summary>
    public class SuperAdmin : EntityBase
    {
        /// <summary>
        /// Gets or sets Phone.
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Gets or sets AccountId.
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Gets or sets Account.
        /// </summary>
        public Account Account { get; set; }
    }
}
