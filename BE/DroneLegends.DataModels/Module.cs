﻿using DroneLegends.DataAccess.Common;

namespace DroneLegends.DataModels
{
    public class Module : EntityBase
    {
        /// <summary>
        /// Gets or sets Name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets FolderName.
        /// </summary>
        public string FolderName { get; set; }

        /// <summary>
        /// Gets or sets Description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Position of a module.
        /// </summary>
        public int Position { get; set; }

        /// <summary>
        /// Gets or sets Image.
        /// </summary>
        public string Image { get; set; }
    }
}
