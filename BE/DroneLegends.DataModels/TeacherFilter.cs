﻿using System;

namespace DroneLegends.DataModels
{
    /// <summary>
    /// Filter data model.
    /// </summary>
    public class TeacherFilter
    {
        /// <summary>
        /// Gets or sets ParentId.
        /// </summary>
        public Guid? ParentId { get; set; }

        /// <summary>
        /// Gets or sets Input.
        /// </summary>
        public string Input { get; set; }

        /// <summary>
        /// Gets or sets Limit.
        /// </summary>
        public int Limit { get; set; }

        /// <summary>
        /// Gets or sets Offset.
        /// </summary>
        public int Offset { get; set; }
    }
}
