﻿using System;
using DroneLegends.DataAccess.Common;

namespace DroneLegends.DataModels
{
    public class DownloadableResource : EntityBase
    {
        /// <summary>
        /// Gets or sets Name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets FileName.
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// Position of a resource in lesson.
        /// </summary>
        public int Position { get; set; }

        /// <summary>
        /// Gets or sets LessonId.
        /// </summary>
        public Guid? LessonId { get; set; }

        /// <summary>
        /// Gets or sets Lesson.
        /// </summary>
        public Lesson Lesson { get; set; }
        
        /// <summary>
        /// Gets or sets ModuleId.
        /// </summary>
        public Guid? ModuleId { get; set; }

        /// <summary>
        /// Gets or sets Module.
        /// </summary>
        public Module Module { get; set; }
    }
}
