﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using DroneLegends.DataAccess.Common;

namespace DroneLegends.DataModels
{
    /// <summary>
    /// License data model.
    /// </summary>
    public class License : EntityBase
    {
        /// <summary>
        /// Gets or sets NumberOfSchools.
        /// </summary>
        public int NumberOfSchools { get; set; }

        /// <summary>
        /// Gets or sets NumberOfTeachers.
        /// </summary>
        public int NumberOfTeachers { get; set; }

        /// <summary>
        /// Gets or sets DurationInMonths.
        /// </summary>
        public int DurationInMonths { get; set; }

        /// <summary>
        /// Gets or sets StartTime.
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Gets or sets OwnerId.
        /// </summary>
        public Guid? OwnerId { get; set; }

        /// <summary>
        /// Gets or sets Owner.
        /// </summary>
        public Institution Owner { get; set; }

        /// <summary>
        /// Gets or sets UsedSchoolAccounts.
        /// </summary>
        [NotMapped]
        public int UsedSchoolAccounts { get; set; }

        /// <summary>
        /// Gets or sets UsedTeacherAccounts.
        /// </summary>
        [NotMapped]
        public int? UsedTeacherAccounts { get; set; }

        /// <summary>
        /// Gets or sets UsedSchoolAccountsPerDistrict.
        /// </summary>
        [NotMapped]
        public int? UsedSchoolAccountsPerDistrict { get; set; }

        /// <summary>
        /// Gets or sets ExpiryDate.
        /// </summary>
        [NotMapped]
        public DateTime? ExpiryDate
        {
            get
            {
                return this.StartDate?.AddMonths(this.DurationInMonths);
            }
        }
    }
}
