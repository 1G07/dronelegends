﻿using System;
using DroneLegends.DataAccess.Common;
using DroneLegends.DataModels.Enums;

namespace DroneLegends.DataModels
{
    /// <summary>
    /// Teacher data model.
    /// </summary>
    public class Ticket: EntityBase
    {
        /// <summary>
        /// Gets or sets Type.
        /// </summary>
        public TicketType Type { get; set; }

        /// <summary>
        /// Gets or sets Text.
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Gets or sets AccountId.
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Gets or sets Account.
        /// </summary>
        public Account Account { get; set; }
    }
}
