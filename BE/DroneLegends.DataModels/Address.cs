﻿using DroneLegends.DataAccess.Common;

namespace DroneLegends.DataModels
{
    /// <summary>
    /// Address data model.
    /// </summary>
    public class Address: EntityBase
    {
        /// <summary>
        /// Gets or sets Street.
        /// </summary>
        public string Street { get; set; }

        /// <summary>
        /// Gets or sets City.
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Gets or sets State.
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// Gets or sets PostCode.
        /// </summary>
        public string PostCode { get; set; }
    }
}
