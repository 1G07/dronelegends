﻿using System;
using DroneLegends.DataAccess.Common;

namespace DroneLegends.DataModels
{
    public class EmbededResource : EntityBase
    {
        /// <summary>
        /// Gets or sets Url.
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Position of a resource in lesson.
        /// </summary>
        public int Position { get; set; }

        /// <summary>
        /// Gets or sets LessonId.
        /// </summary>
        public Guid? LessonId { get; set; }

        /// <summary>
        /// Gets or sets Lesson.
        /// </summary>
        public Lesson Lesson { get; set; }
    }
}
