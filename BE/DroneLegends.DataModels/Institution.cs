﻿using DroneLegends.DataAccess.Common;
using System;

namespace DroneLegends.DataModels
{
    /// <summary>
    /// Institution data model.
    /// </summary>
    public class Institution : EntityBase
    {
        /// <summary>
        /// Gets or sets Name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets AccountId.
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Gets or sets Account.
        /// </summary>
        public Account Account { get; set; }

        /// <summary>
        /// Gets or sets AddressId.
        /// </summary>
        public Guid AddressId { get; set; }

        /// <summary>
        /// Gets or sets Address.
        /// </summary>
        public Address Address { get; set; }

        /// <summary>
        /// Gets or sets ContactId.
        /// </summary>
        public Guid? ContactId { get; set; }

        /// <summary>
        /// Gets or sets Contact.
        /// </summary>
        public Contact Contact { get; set; }

        /// <summary>
        /// Gets or sets ParentId.
        /// </summary>
        public Guid? ParentId { get; set; }

        /// <summary>
        /// Gets or sets ParentInstitution.
        /// </summary>
        public Institution ParentInstitution { get; set; }
    }
}
