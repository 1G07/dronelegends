﻿using System;
using DroneLegends.DataAccess.Common;

namespace DroneLegends.DataModels
{
    /// <summary>
    /// Teacher data model.
    /// </summary>
    public class Teacher : EntityBase
    {
        /// <summary>
        /// Gets or sets FirstName.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets LastName.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets Title.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets Phone.
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Gets or sets ParentId.
        /// </summary>
        public Guid ParentId { get; set; }

        /// <summary>
        /// Gets or sets Parent.
        /// </summary>
        public Institution Parent { get; set; }

        /// <summary>
        /// Gets or sets AccountId.
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Gets or sets Account.
        /// </summary>
        public Account Account { get; set; }
    }
}
