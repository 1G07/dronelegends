
export default {
  getRoles() {
    const axios = window.$nuxt.$axios
    return axios.$get(
      process.env.API_URL + '/api/account/roles/'
    )
  },
  inviteAccount(email) {
    const axios = window.$nuxt.$axios
    return axios.$put(
      process.env.API_URL + `/api/account/invite`,
      {
        url: window.location.origin + '/setPassword/?inviteToken=:token',
        email: email
      }
    )
  },
  changePassword(data) {
    const axios = window.$nuxt.$axios
    return axios.$post(
      process.env.API_URL + `/api/account/changePassword`,
      {
        ...data
      }
    )
  },

  lockAccount(email) {
    const axios = window.$nuxt.$axios
    return axios.$put(
      process.env.API_URL + `/api/account/` + email + `/lock`
    )
  },

  unlockAccount(email) {
    const axios = window.$nuxt.$axios
    return axios.$put(
      process.env.API_URL + `/api/account/` + email + `/unlock`
    )
  },

  unsubscribe(token) {
    const axios = window.$nuxt.$axios
    return axios.$put(
      process.env.API_URL + `/api/account/unsubscribe`,
      {
        token: token,
      }
    )
  },
}
