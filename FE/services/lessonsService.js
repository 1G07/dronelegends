export default {
  getModules() {
    const axios = window.$nuxt.$axios
    return axios.$get(
      process.env.API_URL + '/api/module/'
    ).then((data) => data)
  },

  getModulesThenLessons() {
    const axios = window.$nuxt.$axios
    return axios.$get(
      process.env.API_URL + '/api/module/'
    ).then((data) => this.getLessons(data[0].id))
  },

  getAvailableModules() {
    const axios = window.$nuxt.$axios
    return axios.$get(
      process.env.API_URL + '/api/module/options'
    ).then((data) => data)
  },
  getLessons(id) {
    const axios = window.$nuxt.$axios
    return axios.$get(
      process.env.API_URL + '/api/module/' + id
    ).then((data) => data)
  },
  getLesson(id) {
    const axios = window.$nuxt.$axios
    return axios.$get(
      process.env.API_URL + '/api/module/lesson/' + id
    ).then((data) => data)
  },
  getPresentation(id) {
    const axios = window.$nuxt.$axios
    return axios.$get(
      process.env.API_URL + '/api/module/lesson/' + id + '/presentation/url'
    )
  },
  downloadResource(id) {
    const axios = window.$nuxt.$axios
    return axios.$get(
      process.env.API_URL + '/api/module/resource/download/' + id,
      {
        responseType: 'arraybuffer',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/pdf'
        }
      }
    ).then((data) => data)
  }
}
