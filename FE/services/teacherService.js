
export default {
  removeTeacher(id) {
    const axios = window.$nuxt.$axios
    return axios.$delete(
      process.env.API_URL + '/api/teacher/' + id,
    )
  },
  addNewTeacher(teacherData, parentId) {
    const axios = window.$nuxt.$axios
    return axios.$post(
      process.env.API_URL + '/api/teacher',
      {
        ...teacherData,
        ...({ parentId: parentId })
      }
    )
  },
  editTeacher(data) {
    const axios = window.$nuxt.$axios
    return axios.$put(
      process.env.API_URL + '/api/teacher/' + data.id,
      { ...data, parentId: data.parentInstitution.id }
    )
  },

  getTeachers(limit, input, offset, parentId) {
    const axios = window.$nuxt.$axios
    return axios.$get(
      process.env.API_URL + '/api/teacher',
      {
        params: {
          limit: limit,
          input: input,
          offset: offset,
          parentId: parentId
        },
      }
    )
  },

  getTeacher(id) {
    const axios = window.$nuxt.$axios
    return axios.$get(
      process.env.API_URL + '/api/teacher/' + id
    ).then((data) => data)
  },

  getTeacherByToken() {
    const axios = window.$nuxt.$axios
    return axios.$get(
      process.env.API_URL + '/api/teacher/self'
    ).then((data) => data)
  }
}
