
export default {
  removeInstitution(id) {
    const axios = window.$nuxt.$axios
    return axios.$delete(
      process.env.API_URL + '/api/institution/' + id,
    )
  },
  addNewInstitution(institutionType, institutionData, parentId) {
    const axios = window.$nuxt.$axios
    return axios.$post(
      process.env.API_URL + '/api/institution',
      {
        role: institutionType, ...institutionData,
        ...({ parentId: parentId })
      }
    )
  },
  editInstitution(data) {
    const axios = window.$nuxt.$axios
    const returndata = axios.$put(
      process.env.API_URL + '/api/institution/' + data.id,
      { ...data }
    )
    return returndata
  },

  changeLicense(id, data) {
    const axios = window.$nuxt.$axios
    return axios.$post(
      process.env.API_URL + '/api/license/' + id,
      { ...data },
    ).then((data) => data).catch((err) => err)
  },

  updateLicense(data) {
    const axios = window.$nuxt.$axios
    return axios.$put(
      process.env.API_URL + '/api/license/' + data.id,
      { ...data },
    )
  },

  getInstitutions(institutionType, input, limit, offset, parentId) {
    const axios = window.$nuxt.$axios
    return axios.$get(
      process.env.API_URL + '/api/institution',
      {
        params: {
          role: institutionType,
          limit: limit,
          offset: offset,
          input: input,
          parentId: parentId
        },
      }
    )
  },
  getInstitutionById(id) {
    const axios = window.$nuxt.$axios
    return axios.$get(
      process.env.API_URL + '/api/institution/' + id
    ).then((data) => data)
  },
  getInstitutionByToken() {
    const axios = window.$nuxt.$axios
    return axios.$get(
      process.env.API_URL + '/api/institution/self'
    ).then((data) => data)
  }
}
