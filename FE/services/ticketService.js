export default {
  sendTicket(type, text) {
    const axios = window.$nuxt.$axios
    return axios.$post(
      process.env.API_URL + '/api/ticket/', {
      type: type,
      text: text
    }
    ).then((data) => data)
  },

}
