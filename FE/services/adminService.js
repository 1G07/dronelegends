
export default {
  removeAdmin(id) {
    const axios = window.$nuxt.$axios
    return axios.$delete(
      process.env.API_URL + '/api/superadmin/' + id,
    )
  },
  addNewAdmin(data) {
    const axios = window.$nuxt.$axios
    return axios.$post(
      process.env.API_URL + '/api/superadmin',
      {
        ...data,
      }
    )
  },
  saveAdmin(data) {
    const axios = window.$nuxt.$axios
    return axios.$put(
      process.env.API_URL + '/api/superadmin/' + data.id,
      { ...data }
    )
  },

  getAdmins(input, limit, offset) {
    const axios = window.$nuxt.$axios
    return axios.$get(
      process.env.API_URL + '/api/superadmin',
      {
        params: {
          limit: limit,
          input: input,
          offset: offset
        },
      }
    )
  },

  getAdmin(id) {
    const axios = window.$nuxt.$axios
    return axios.$get(
      process.env.API_URL + '/api/superadmin/' + id
    ).then((data) => data)
  },

  getUserData() {
    const axios = window.$nuxt.$axios
    return axios.$get(
      process.env.API_URL + '/api/superadmin/self'
    ).then((data) => {
      return data
    })
  },

  saveUserData(data) {
    const axios = window.$nuxt.$axios
    return axios.$put(
      process.env.API_URL + '/api/superadmin/' + data.id,
      { ...data }
    )
  },
}
