[
  {
    "headline": "environment",
    "content": [
      "Fly in areas with bright light.",
      "Tello cannot fly over highly reflective surfaces like water, some tile floors, etc. If your floor is very reflective or monochromatic, try placing some sheets of paper or boxes on the ground to break up the reflectivity and color of the floor.",
      "Tello is lightweight. It can easily be affected by wind from air conditioning or fans. A slight wind can move the drone off course so always allow for ample space for flight.",
      "Be aware of ceiling fans, lighting, and exposed light bulbs in ceiling fixtures.",
      "Metal fixtures can cause interference and you may notice Tello behaves unexpectedly when flying near large metal objects."
    ]
  },
  {
    "headline": "Interference",
    "content": [
      "Tello uses sensors instead of GPS. Sometimes you may notice unexpected behavior by Tello which could be caused by a number of obstacles. People, especially if many students are standing in close proximity to Tello, can cause obstacle avoidance. Set boundaries in class for safety and to reduce interference.",
      "Metal or rebar in the ceiling, floor, metal light fixtures, tables, or chairs can cause Tello to behave oddly.  If you notice this issue, reconfigure your mission commands to prohibit Tello from flying low to the ground, near light fixtures, or other objects that may contain metal and cause interference.",
      "Stone, concrete, reflective, or marble flooring or walls may cause some interference issues. We notice that wooden gym floors work the best if flying missions close to the ground. If you notice your Tello seems to become stuck about 10-15 inches from the ground, reconfigure your code to fly at a higher altitude.",
      "Have a clear flight path available. If you choose to fly near or through obstacles as part of an activity, use plastic goalposts or rings, as plastic will provide the least interference."
    ]
  },
  {
    "headline": "WI-FI and Connectivity",
    "content": [
      "Update the DJI Tello firmware using the DJI app",
      "SSID (Service Set Identifier): the technical name for a Wi-Fi network. Tello creates its own Wi-Fi hotspot that connects to a device and allows for remote control.",
      {
        "headline": "Compatibility - Tello App or DroneBlocks App",
        "subs": [
          "iOS - Requires iOS 9.0 or later.",
          "Android - Android version 4.4.0 or later."
        ]
      },
      "Be sure you have enough bandwidth and space to support the number of tablets/computers and Tellos you are using simultaneously.",
      "Turn Tello off before connecting to a new device.",
      "If Tello fails to connect, shut down the app, turn off Tello, connect to your regular Wifi to re-launch, and repeat the connectivity sequence from \"Connect to Tello\". This sequence must be followed exactly.",
      "In some cases, the WiFi connection between your mobile device and Tello may drop. If this happens, DroneBlocks can no longer issue commands to Tello. Do not panic! Tello will execute the last issued command and then hover in place. At this point, you can toggle into your WiFi settings and reconnect to Tello, then open the DroneBlocks app again and resume the regular Tello connection process.",
      "In rare cases, Tello may stop responding to DroneBlocks commands and will hover in the air. Try inserting a land block, then tap launch mission. You can also close DroneBlocks and switch to the Tello app to regain manual control, going through the connection process again.",
      "If you still are unable to connect to DroneBlocks, the great news is that Tello has an internal battery threshold. Once this gets to a critical state Tello will land automatically.",
      {
        "headline": "Skipped DroneBlocks commands",
        "subs": [
          "If you notice DroneBlocks commands are being skipped and cannot determine the cause, first check that your software and firmware are updated, then check your battery level.",
          "If this problem continues, try adding a \"hover\" block between commands. Occasionally, commands may be sent too quickly for Tello to respond. Adding a block such as \"hover\" may give Tello time to better process the command."
        ]
      }
    ]
  },
  {
    "headline": "Anatomy extras",
    "content": [
      {
        "headline": "Vision Positioning System (VPS)",
        "subs": [
          "The two black spheres on the bottom of Tello that control Tello’s altitude. Tello uses these sensors to hover about 4 feet above the ground upon taking off.  The VPS should be wiped clean periodically, like a camera lens. If it's dirty, it can malfunction. The VPS consists of a camera and an infrared sensor. Both serve to stabilize the drone in order to hold a certain position when moved to it."
        ]
      },
      {
        "headline": "Propellers",
        "subs": [
          "Tello is a quadcopter. It has four propellers that spin to generate an upward force. Two of the propellers spin clockwise while the other two spin counterclockwise. The propellers that spin clockwise are shaped differently than the propellers that spin counterclockwise. The propellers need to be placed properly for Tello to fly. See page x of the Operations Manual for more information."
        ]
      },
      {
        "headline": "The Tello's internal IMU",
        "subs": [
          "The “inertial measurement unit” is an assistance system built into the Tello drone that measures the speed, angular velocity, and position of the drone. It ensures that the drone maintains its orientation toward the ground by itself and does not drift.",
          "Shaking Tello can disrupt the IMU. If you suspect this happened, you can perform an IMU calibration by going to the settings>more>...>calibrate IMU in the Tello App"
        ]
      }
    ]
  }
]
