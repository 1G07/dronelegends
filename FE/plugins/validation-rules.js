
import { extend, setInteractionMode } from 'vee-validate'
import { required, numeric, confirmed } from 'vee-validate/dist/rules'

setInteractionMode('eager')

extend('required', {
  ...required,
  message: 'Required field',
})

extend('numeric', {
  ...numeric,
  message: 'Numbers only',
})

extend('emailCustom', value => {
  // if the field is not a valid email
  if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(value)) {
    return 'Email address is not valid'
  }
  return true;
})

extend('noSpaces', value => {
  // if the field is not a valid email
  console.log('NO SPACES IN')
  if (!/^\S*$/.test(value)) {
    return 'Email address cannot contain space character'
  }
  return true;
})

extend('confirmed', {
  ...confirmed,
  message: 'Passwords are not matched',
})

extend('passwordSecure', value => {
  // if the field is not a valid Password
  if (!/^(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[0-9]).{8,}$/.test(value)) {
    return 'minimum 8 characters, 1 BIG letter, 1 small letter and 1 number';
  }

  // All is good
  return true;
});
