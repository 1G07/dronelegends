import Vue from 'vue'
import VueFbCustomerChat from 'vue-fb-customer-chat'

Vue.use(VueFbCustomerChat, {
  page_id: 101600371430437, //  change 'null' to your Facebook Page ID,
  theme_color: '#0074BC', // theme color in HEX
  locale: 'en_US', // default 'en_US'
  greeting_dialog_display: 'hide'
})
