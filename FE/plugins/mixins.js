
import Vue from 'vue'
import _ from 'lodash'
import accountService from '../services/accountService'
Vue.mixin({
  methods: {

    parseDate(date) {
      return Date.parse(date) / 1000
    },

    getFormatedDate(unix) {

      const date = new Date(unix * 1000)
      const MM = date.getMonth() + 1
      const DD = date.getDate()
      const YY = date.getFullYear()

      return MM + '/' + DD + '/' + YY
    },

    getType(role, roles) {

      return _.find(roles, { 'key': role })
    },

    async getChildType(role, roles) {
      if (role === 'school') {
        return await roles[_.findIndex(roles, { 'key': role }) + 2]
      }
      return await roles[_.findIndex(roles, { 'key': role }) + 1]
    },

    isTeacher(type) {
      return type === 'teacher'
    },

    getStatusClass(license, isLocked) {
      if (!license || license.startDate === null || isLocked) {
        return 'status-orange'
      } else if (this.currentDate < license.expiryDate) {
        return 'status-green'
      } else {
        return 'status-red'
      }
    },

    getStatus(license, isLocked) {
      if (!license) {
        return 'NO LICENSE'
      } else if (license.startDate === null) {
        return 'INACTIVE'
      } else if (isLocked) {
        return 'LOCKED'
      } else if (this.currentDate < license.expiryDate) {
        return 'ACTIVE'
      } else {
        return 'OVERDUE'
      }
    },

    getRegistraionStatus(status, isLocked) {
      if (status === 'notInvited') {
        return 'Not Invited'
      } else if (status === 'invited') {
        return 'Invited'
      } else if (isLocked) {
        return 'LOCKED'
      } else if (this.currentDate < license.expiryDate) {
        return 'ACTIVE'
      } else {
        return 'OVERDUE'
      }
    },
  }
})

