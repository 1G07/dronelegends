import Vue from 'vue'
import InputField from '~/components/Design/inputField'
import Avatar from '~/components/Design/avatar'

const components = { InputField, Avatar }

Object.entries(components).forEach(([name, component]) => {
  Vue.component(name, component)
})
