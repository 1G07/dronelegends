export default async function ({ $auth, store, redirect, route }) {

  const user = $auth.$storage.getUniversal('user')
  if (user && ((user.role !== 'teacher' && user.role !== 'superAdmin' && user.role !== 'school') && (route.name === 'index' || route.name === 'module' || route.name === 'module-slug'))) {
    await store.dispatch('institutionStore/getInstitutionByToken')
    return redirect('/administration/' + store.state.institutionStore.userInstitution.id)
  }

  if (user && (user.role !== 'superAdmin' && route.name === 'administration')) {
    await store.dispatch('institutionStore/getInstitutionByToken')
    return redirect('/administration/' + store.state.institutionStore.userInstitution.id)
  }

  store.app.router.beforeEach(

    async (to, from, next) => {
      if (user && (user.role !== 'superAdmin' && to.path === '/administration')) {
        await store.dispatch('institutionStore/getInstitutionByToken')
        return redirect('/administration/' + store.state.institutionStore.userInstitution.id)
      }
      next()
    }
  )
}
