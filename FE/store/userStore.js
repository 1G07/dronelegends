import InstitutionService from '@/services/institutionService'
import AdminService from '../services/adminService'
import TeacherService from '../services/teacherService'

export const state = () => ({
  user: null,
  isSnackbarActive: false,
  snackbarMsg: []
})

export const getters = {
  getUser: state => state.user,

  isSnackbarActive: state => state.isSnackbarActive,
  snackbarMsg: state => state.snackbarMsg
}

export const actions = {

  async getUserData({ commit }) {

    const loginData = window.$nuxt.$auth.$storage.getUniversal('user')
    let response;

    if (loginData.role === 'superAdmin') {

      try {
        response = await AdminService.getUserData()
      } catch {
        return
      }

    } else if (loginData.role === 'teacher') {

      try {
        response = await TeacherService.getTeacherByToken()
      } catch (error) {
        return error
      }

    } else {

      try {
        response = await InstitutionService.getInstitutionByToken()
      } catch (error) {
        return error
      }
    }

    commit('setUserData', response)
  },

  saveUserData({ commit }, userData) {
    const loginData = window.$nuxt.$auth.$storage.getUniversal('user')

    if (loginData.role === 'superAdmin') {
      AdminService.saveUserData(userData).then(
        (data) => {
          commit('setUserData', data)
        }
      )
    } else if (loginData.role === 'teacher') {

      TeacherService.editTeacher(userData).then(
        (data) => {
          commit('setUserData', data)
        }
      )
    } else {

      InstitutionService.editInstitution(userData).then(
        (data) => {
          commit('setUserData', data)
          commit('setUserLicense', userData.license)
        }
      )
    }

  },

  resetSnackbarMsg({ commit }) {
    commit('resetSnackbarMsg')
  },

  setSnackbarMsg({ commit }, snackbarMsg) {
    commit('setSnackbarMsg', snackbarMsg)
  },

  removeSnackbarMsgByIndex({ commit }, index) {
    commit('removeSnackbarMsgByIndex', index)
  },
}

export const mutations = {
  setUserData: (state, data) => state.user = data,
  setUserLicense: (state, data) => state.user.license = data,
  setSnackbarMsg: (state, snackbarMsg) => {
    state.snackbarMsg.push(snackbarMsg)
  },
  resetSnackbarMsg: (state) => state.snackbarMsg.unshift(),
  removeSnackbarMsgByIndex: (state, index) => state.snackbarMsg.splice(index, 1)
}
