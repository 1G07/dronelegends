import AdminService from '@/services/adminService'

export const state = () => ({
  admins: [],
  admin: {},
  userAdmin: {},
  editAdminData: {},
  breadcrumbs: [],
  page: 1,
  totalCount: null,
  countPerPage: 10,
  error: '',
})

export const getters = {
  allAdmins: state => state.admins,
  adminById: state => state.admin,
  editAdminData: state => state.editAdminData,
  getUserData: state => state.userAdmin,
  totalCount: state => state.totalCount,
  countPerPage: state => state.countPerPage,
  page: state => state.page,
  breadcrumbs: state => state.breadcrumbs,
  roles: state => state.roles,
  error: state => state.error
}

export const actions = {

  async getAdmins({ commit }, args) {
    await AdminService.getAdmins(args.input, args.limit, args.offset).then(
      (admins) => {
        commit('setAdmins', admins.data)
        commit('setPage', admins.page)
        commit('setPageTotalCount', admins.total)
        commit('setCountPerPage', admins.pageSize)
      }
    )
  },

  async getAdminById({ commit }, id) {

    await AdminService.getAdminById(id).then(
      (data) => {
        commit('setAdmin', data)
      }

    )
  },

  async getEditAdminData({ commit }, id) {

    await AdminService.getAdminById(id).then(
      (data) => {
        commit('setEditAdminData', data)
      }

    )
  },

  async getUserData({ commit }) {

    await AdminService.getAdminByToken().then(
      (data) => {
        commit('setUserAdmin', data)
      }
    )
  },

  async addAdmin({ commit, dispatch }, data) {

    await AdminService.addNewAdmin(data)
      .then((admin) => {
        commit('newAdmin', data)
        dispatch('userStore/setSnackbarMsg', 'Admin account created sucessfully', { root: true })

      }).catch((error) => {

        if (error.response) {
          // Request made and server responded
          console.log('Error', error.response.data.message);
          commit('setResponseError', error.response.data.message)
        } else if (error.request) {
          // The request was made but no response was received
          console.log('Error', error.request.message);
          commit('setResponseError', error.request.message)
        } else {
          // Something happened in setting up the request that triggered an Error
          console.log('Error', error.message);
        }
      })
  },

  async editUserAdmin({ commit, dispatch }, args) {
    await AdminService.editAdmin(args.data)
    dispatch('userStore/setSnackbarMsg', 'Profile data changed sucessfully', { root: true })
    commit('setUserAdmin', args.data)
  },

  async saveAdmin({ commit, dispatch }, data) {

    await AdminService.saveAdmin(data).then(
      (admin) => {
        commit('editAdmin', admin)
        dispatch('userStore/setSnackbarMsg', 'Admin data changed sucessfully', { root: true })
      }
    ).catch((error) => {
      if (error.response) {
        // Request made and server responded
        console.log('Error', error.response.data.message);
        commit('setResponseError', error.response.data.message)
      } else if (error.request) {
        // The request was made but no response was received
        console.log('Error', error.request.message);
        commit('setResponseError', error.request.message)
      } else {
        // Something happened in setting up the request that triggered an Error
        console.log('Error', error.message);
      }
    })
  },

  removeAdmin({ commit, dispatch }, id) {
    AdminService.removeAdmin(id).then(() => {
      commit('removeAdmin', id)
      dispatch('userStore/setSnackbarMsg', 'Admin account removed succesfully', { root: true })
    })
  },

  async setErrorEmpty({ commit }) {
    commit('setErrorEmpty')
  },

}

export const mutations = {
  setErrorEmpty: (state) => state.error = '',
  setResponseError: (state, error) => state.error = error,
  setRequestError: (state, error) => state.error.request = error,
  setBreadcrumbs: (state, breadcrumbs) => state.breadcrumbs = breadcrumbs,
  setRoles: (state, roles) => state.roles = roles,
  setPage: (state, page) => state.page = page,
  setPageTotalCount: (state, total) => state.totalCount = total,
  setCountPerPage: (state, pageSize) => state.countPerPage = pageSize,


  ///-------- Admins --------///
  setAdmins: (state, admins) => state.admins = admins,
  setAdmin: (state, admin) => state.admin = { ...admin },
  setEditAdminData: (state, admin) => state.editAdminData = { ...admin },
  setUserAdmin: (state, admin) => state.userAdmin = { ...admin },
  newAdmin: (state, newAdmin) => state.admins.unshift(newAdmin),
  removeAdmin: (state, id) => state.admins = state.admins.filter((admin) => {
    return admin.id !== id
  }),
  editAdmin: (state, newAdminData) => {
    const i = _.findIndex(state.admins, function (o) { return o.id == newAdminData.id })
    let newList = _.cloneDeep(state.admins.filter((admin) => {
      return admin.id !== newAdminData.id
    }))
    newList.splice(i, 0, newAdminData)
    state.admins = newList
  }
}
