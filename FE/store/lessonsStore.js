
import LessonsService from '@/services/lessonsService'
export const state = () => ({
  lessons: [],
  lesson: {},
  lessonModules: [],
  lessonModule: {},
  availableModules: [],
  breadcrumbs: [],
  error: {}
})

export const getters = {
  lessons: state => state.lessons,
  lesson: state => state.lesson,
  lessonModules: state => state.lessonModules,
  lessonModule: state => state.lessonModule,
  breadcrumbs: state => state.breadcrumbs,
  availdableModules: state => state.availableModules,
  error: state => state.error
}

export const actions = {
  async getAvailableModules({ commit }) {
    try {
      const availableModules = await LessonsService.getAvailableModules()
      commit('setAvailableModules', availableModules)
    } catch (error) {
      console.log(error)
    }


  },
  async getModules({ commit }) {
    await LessonsService.getModules().then(
      (lessonModules) => {
        commit('setModules', lessonModules)
      }

    )
  },

  async getLessons({ commit }, id) {
    await LessonsService.getLessons(id).then(
      (lessons) => {
        commit('setLessons', lessons)
      }

    )
  },

  async getLesson({ commit }, id) {
    await LessonsService.getLesson(id).then(
      (lesson) => {
        commit('setLesson', lesson)
      }

    )
  },
}

export const mutations = {
  setBreadcrumbs: (state, breadcrumbs) => state.breadcrumbs = breadcrumbs,
  setLessons: (state, lessons) => state.lessons = lessons,
  setLesson: (state, lesson) => state.lesson = lesson,
  setAvailableModules: (state, availableModules) => state.availableModules = availableModules,
  setModules: (state, lessonModules) => state.lessonModules = lessonModules,
  setModule: (state, lessonModule) => state.lessonModule = lessonModule,

}
