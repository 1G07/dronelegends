import InstitutionService from '@/services/institutionService'
import TeacherService from '@/services/teacherService'
import AccountService from '@/services/accountService'
import { _ } from 'core-js'
export const state = () => ({
  roles: [],
  institutions: [],
  institution: {},
  userInstitution: {},
  editInstitutionData: {},
  teacher: {},
  breadcrumbs: [],
  page: 1,
  totalCount: null,
  countPerPage: 10,
  error: '',

})

export const getters = {
  allInstitutions: state => state.institutions,
  institutionById: state => state.institution,
  editInstitutionData: state => state.editInstitutionData,
  getInstitutionByToken: state => state.userInstitution,
  teacherById: state => state.teacher,
  totalCount: state => state.totalCount,
  countPerPage: state => state.countPerPage,
  page: state => state.page,
  breadcrumbs: state => state.breadcrumbs,
  roles: state => state.roles,
  error: state => state.error,
}

export const actions = {

  async getRoles({ commit }) {
    await AccountService.getRoles().then(
      (roles) => {
        commit('setRoles', roles)
      }
    )
  },

  async getInstitutions({ commit }, args) {
    if (args.type === 'teacher') {
      await TeacherService.getTeachers(args.limit, args.input, args.offset, args.parentId).then(
        (institutions) => {
          commit('setInstitutions', institutions.data)
          commit('setPage', institutions.page)
          commit('setPageTotalCount', institutions.total)
          commit('setCountPerPage', institutions.pageSize)
        }
      )
    } else {
      await InstitutionService.getInstitutions(args.type, args.input, args.limit, args.offset, args.parentId).then(
        (institutions) => {
          commit('setInstitutions', institutions.data)
          commit('setPage', institutions.page)
          commit('setPageTotalCount', institutions.total)
          commit('setCountPerPage', institutions.pageSize)
        }
      )
    }
  },

  async getInstitutionById({ commit }, id) {
    try {
      const data = await InstitutionService.getInstitutionById(id)
      commit('setInstitution', data)
    } catch (err) {
      return err
    }
  },

  async getEditInstitutionData({ commit }, id) {
    await InstitutionService.getInstitutionById(id).then(
      (data) => {
        commit('setEditInstitutionData', data)
      }

    )
  },

  async getInstitutionByToken({ commit }) {
    const user = window.$nuxt.$auth.$storage.getUniversal('user')
    if (user.role !== 'teacher') {
      await InstitutionService.getInstitutionByToken().then(
        (data) => {
          commit('setUserInstitution', data)
        }
      )
    } else {
      await TeacherService.getTeacherByToken().then(
        (data) => {
          commit('setUserInstitution', data)
        }
      )
    }
  },

  async addInstitution({ commit, dispatch }, args) {
    const user = window.$nuxt.$auth.$storage.getUniversal('user')

    await InstitutionService.addNewInstitution(
      args.type,
      args.data,
      user.role === 'superAdmin' && args.parent ? args.parent.id : null)
      .then((institution) => {


        if (args.parent) {

          institution.license = args.parent.license
          commit('newInstitution', institution)

          dispatch('userStore/setSnackbarMsg', 'Institution account created sucessfully', { root: true })
          dispatch('inviteAccount', args.data.email)
        }

        else {

          InstitutionService.changeLicense(institution.id, args.license).then((response) => {
            dispatch('userStore/setSnackbarMsg', 'License added sucessfully', { root: true })
            // dispatch('inviteAccount', args.data.email)
            institution.license = { ...response }
            commit('newInstitution', institution)

            dispatch('userStore/setSnackbarMsg', 'Institution account created sucessfully', { root: true })
            dispatch('inviteAccount', args.data.email)
          })
        }
      }).catch((error) => {

        const errorMsg = getError(error)
        commit('setResponseError', errorMsg)
      })
  },
  async editUserInstitution({ commit, dispatch }, args) {
    const user = window.$nuxt.$auth.$storage.getUniversal('user')
    if (user.role !== 'teacher') {
      await InstitutionService.editInstitution(args.data)
      dispatch('userStore/setSnackbarMsg', 'Profile data changed sucessfully', { root: true })
      commit('setUserInstitution', args.data)
    } else {
      await TeacherService.saveTeacherData(args.data)
      dispatch('userStore/setSnackbarMsg', 'Profile data changed sucessfully', { root: true })
      commit('setUserInstitution', args.data)
    }
  },

  async editInstitution({ commit, dispatch }, args) {
    const user = window.$nuxt.$auth.$storage.getUniversal('user')

    // if (user.role !== 'superAdmin') {
    // delete args.data.parentId
    await InstitutionService.editInstitution(args.data).then(
      () => {
        dispatch('userStore/setSnackbarMsg', 'Institution data changed sucessfully', { root: true })
        commit('editInstitution', args.data)
      }).catch((error) => {
        const errorMsg = getError(error)
        commit('setResponseError', errorMsg)
      })
  },

  addNewLicense({ commit, dispatch }, args) {
    InstitutionService.changeLicense(args.id, args.data).then((response) => {
      commit('editLicense', args)
      dispatch('userStore/setSnackbarMsg', 'License added sucessfully', { root: true })
      AccountService.inviteAccount(args.data.email)
      dispatch('userStore/setSnackbarMsg', 'User account invited to join', { root: true })
    })
  },

  updateLicense({ commit, dispatch }, args) {
    InstitutionService.updateLicense(args.data).then(() => {
      commit('editLicense', args)
      dispatch('userStore/setSnackbarMsg', 'License added sucessfully', { root: true })
    })
  },

  removeInstitution({ commit, dispatch }, id) {
    InstitutionService.removeInstitution(id).then(() => {
      commit('removeInstitution', id)
      dispatch('userStore/setSnackbarMsg', 'Institution account removed succesfully', { root: true })
    })
  },

  /// ------ Teachers ------- ////
  async getTeachers({ commit }, args) {
    await TeacherService.getTeachers(args.limit, args.input, args.offset, args.parentId).then(
      (teachers) => {
        commit('setTeachers', teachers.data)
        commit('setPage', teachers.page)
        commit('setPageTotalCount', teachers.total)
        commit('setCountPerPage', teachers.pageSize)
      }

    )
  },
  async getTeacher({ commit }, id) {
    await TeacherService.getTeacher(id).then(
      (data) => commit('setTeacher', data)

    )
  },

  async addTeacher({ commit, dispatch }, args) {
    const user = window.$nuxt.$auth.$storage.getUniversal('user')

    await TeacherService.addNewTeacher(args.data,
      user.role === 'superAdmin' ? args.parentId : null).then((teacher) => {
        dispatch('userStore/setSnackbarMsg', 'Teacher account created sucessfully', { root: true })
        commit('newTeacher', teacher)
        dispatch('inviteAccount', args.data.email)
      }).catch((error) => {
        const errorMsg = getError(error)
        commit('setResponseError', errorMsg)
      })
  },

  async saveTeacherData({ commit }, teacher) {
    await TeacherService.saveTeacherData(teacher).then(() => {
      commit('saveTeacherData', teacher)
    }).catch((error) => {
      const errorMsg = getError(error)
      commit('setResponseError', errorMsg)
    })
  },

  removeTeacher({ commit }, id) {
    TeacherService.removeTeacher(id).then(() => {

      commit('removeTeacher', id)
    })
  },


  //-------Account -------//

  lockAccount({ commit }, data) {
    AccountService.lockAccount(data.email).then(() => {
      if (data.role !== 'teacher') {
        commit('editInstitution', data)
      } else {
        commit('saveTeacherData', data)
      }
    }).catch((error) => {
      const errorMsg = getError(error)
      commit('setResponseError', errorMsg)
    })
  },

  unlockAccount({ commit }, data) {
    AccountService.unlockAccount(data.email).then(() => {
      if (data.role !== 'teacher') {
        commit('editInstitution', data)
      } else {
        commit('saveTeacherData', data)
      }
    }).catch((error) => {
      const errorMsg = getError(error)
      commit('setResponseError', errorMsg)
    })
  },

  async inviteAccount({ commit, dispatch }, accountEmail) {
    AccountService.inviteAccount(accountEmail).then(() => {
      commit('changeInvitedAccountStatus')
      dispatch('userStore/setSnackbarMsg', 'User account invited to join', { root: true })
    })
  },

  //Snackbar and error messages
  async setErrorEmpty({ commit }) {
    commit('setErrorEmpty')
  },

  setPage({ commit }, pageNo) {
    commit('setPage', pageNo)
  }
}

export const mutations = {
  setErrorEmpty: (state) => state.error = '',
  setResponseError: (state, error) => {
    state.error = error
  },

  setRequestError: (state, error) => state.error.request = error,
  setBreadcrumbs: (state, breadcrumbs) => state.breadcrumbs = breadcrumbs,
  setRoles: (state, roles) => state.roles = roles,
  setPage: (state, page) => state.page = page,
  setPageTotalCount: (state, total) => state.totalCount = total,
  setCountPerPage: (state, pageSize) => state.countPerPage = pageSize,


  ///-------- Institutions --------///
  setInstitutions: (state, institutions) => state.institutions = institutions,
  setInstitution: (state, institution) => state.institution = { ...institution },
  setEditInstitutionData: (state, institution) => state.editInstitutionData = { ...institution },
  setUserInstitution: (state, institution) => state.userInstitution = { ...institution },
  newInstitution: (state, newInstitution) => state.institutions.unshift(newInstitution),
  removeInstitution: (state, id) => state.institutions = state.institutions.filter((institution) => {
    return institution.id !== id
  }),

  editInstitution: (state, newInstitutionData) => {

    const i = _.findIndex(state.institutions, function (o) { return o.id == newInstitutionData.id })

    let newList = _.cloneDeep(state.institutions.filter((institution) => {
      return institution.id !== newInstitutionData.id
    }))

    newList.splice(i, 0, newInstitutionData)
    state.institutions = newList
  },

  editLicense: (state, args) => {

    let newList = _.cloneDeep(state.institutions)

    if (args.data.ownerId === args.id) {

      const i = _.findIndex(state.institutions, function (o) { return o.id == args.id })
      _.set(newList[i], 'license', args.data)
    } else {

      _.forEach(newList, (listItem) => {
        listItem.license = args.data
      })
    }
    state.institutions = newList

  },

  changeInvitedAccountStatus: (state) => {
    state.institutions[0].registrationStatus = 'invited'
  },

  ///---------- Teachers --------///

  setTeachers: (state, teachers) => state.institutions = teachers,
  setTeacher: (state, teacher) => state.teacher = teacher,
  newTeacher: (state, newTeacher) => {
    state.institutions.unshift(newTeacher)
    state.institution.license.usedTeacherAccounts = state.institution.license.usedTeacherAccounts + 1
  },
  removeTeacher: (state, id) => {
    state.institutions = state.institutions.filter((teacher) => {
      return teacher.id !== id
    })
    state.institution.license.usedTeacherAccounts = state.institution.license.usedTeacherAccounts - 1
  },
  saveTeacherData: (state, newTeacherData) => {
    const i = _.findIndex(state.institutions, function (o) { return o.id == newTeacherData.id })
    let newList = _.cloneDeep(state.institutions.filter((teacher) => {
      return teacher.id !== newTeacherData.id
    }))
    newList.splice(i, 0, newTeacherData)
    state.institutions = newList
  },
}

function getError(error) {
  if (error.response) {
    // Request made and server responded
    console.log('Response error', error.response.data.message);
    return error.response.data.message
  } else if (error.request) {
    // The request was made but no response was received
    console.log('Request error', error);
    return error + ' please check your connection and try again'
  } else {
    console.log('Error', error.message);
    return error.message
  }
}
